<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriProduk extends Model
{
    protected $table = 'kategoriproduk';

    protected $fillable = [
        'namakategori',
        'foto'
    ];

    //Relasi One to Many dari
    public function subkategoriproduk(){
        return $this->hasMany('App\SubKategoriProduk', 'id_kategoriproduk');
    }
    public function produktoko(){
        return $this->hasMany('App\ProdukToko', 'id_kategoriproduk');
    }
    public function produkbumdes(){
        return $this->hasMany('App\ProdukBumdes', 'id_kategoriproduk');
    }
}
