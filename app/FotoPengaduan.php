<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoPengaduan extends Model
{
    protected $table = 'fotopengaduan';

    protected $fillable = [
        'id_pengaduan',
        'foto',  	        
    	'created_at',
        'updated_at'
    ];

 	public function pengaduan(){
        return $this->belongsTo('App\Pengaduan', 'id_pengaduan');
    }
}
