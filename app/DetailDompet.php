<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailDompet extends Model
{
    protected $table = 'detaildompet';
    
    protected $fillable = [
        'id_dompet',
        'tanggal',
        'keterangan',
        'nominal',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many ke
    public function dompet(){
        return $this->belongsTo('App\Dompet', 'id_dompet');
    }
}
