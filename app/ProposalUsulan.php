<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposalUsulan extends Model
{
    protected $table = 'proposalusulan';

    protected $fillable = [
        'id_usulan',
        'file',  	        
    	'created_at',
        'updated_at'
    ];

 	public function usulan(){
        return $this->belongsTo('App\Usulan', 'id_usulan');
    }
}
