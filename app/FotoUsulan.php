<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoUsulan extends Model
{
    protected $table = 'fotousulan';

    protected $fillable = [
        'id_usulan',
        'foto',  	        
    	'created_at',
        'updated_at'
    ];

 	public function usulan(){
        return $this->belongsTo('App\Usulan', 'id_usulan');
    }
}
