<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifikasiToko extends Model
{
    protected $table = 'notifikasitoko';
    
    protected $fillable = [
        'id_toko',
        'isi_notifikasi',
        'created_at',
        'updated_at'
    ];
    
    //Relasi One to Many ke
    public function toko(){
        return $this->belongsTo('App\Toko', 'id_toko');
    }
}
