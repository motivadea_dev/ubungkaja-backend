<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    protected $table = 'pengiriman';
    
    protected $fillable = [
        'id_kurir',
        'id_transaksipembelian',
        'status',
        'komisi',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many ke
    public function kurir(){
        return $this->belongsTo('App\Kurir', 'id_kurir');
    }
    public function transaksipembelian(){
        return $this->belongsTo('App\TransaksiPembelian', 'id_transaksipembelian');
    }
}
