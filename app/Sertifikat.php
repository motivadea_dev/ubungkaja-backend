<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sertifikat extends Model
{
    protected $table = 'sertifikat';

    protected $fillable = [
    	'id_keluarga',
    	'lokasi',
        'longitude',  
        'latitude',   	 
        'luaswilayah',         
    	'created_at',
        'updated_at'
    ];

 	public function keluarga(){
        return $this->belongsTo('App\Keluarga', 'id_keluarga');
    }
}
