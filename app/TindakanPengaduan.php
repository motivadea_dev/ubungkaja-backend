<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TindakanPengaduan extends Model
{
    protected $table = 'tindakanpengaduan';

    protected $fillable = [
        'id_pengaduan',
        'judul',
        'foto',  	        
    	'created_at',
        'updated_at'
    ];

 	public function pengaduan(){
        return $this->belongsTo('App\Pengaduan', 'id_pengaduan');
    }
}
