<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengaduan extends Model
{
    protected $table = 'pengaduan';

    protected $fillable = [
    	'tanggal',
    	'judul',
    	'id_kategoripengaduan',
        'id_warga',
        'id_dusun',
		'longitude', 
		'latitude',
        'deskripsi',
        'privasipengaduan',
        'privasiidentitas',
		'status',    	        
    	'created_at',
        'updated_at'
    ];

    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
    public function kategoripengaduan(){
    	return $this->belongsTo('App\KategoriPengaduan', 'id_kategoripengaduan');
    }
    public function dusun(){
    	return $this->belongsTo('App\Dusun', 'id_dusun');
    }

    public function fotopengaduan(){
        return $this->hasMany('App\FotoPengaduan', 'id_pengaduan');
    }
    public function komentarpengaduan(){
        return $this->hasMany('App\KomentarPengaduan', 'id_pengaduan');
    }
    public function tindakanpengaduan(){
        return $this->hasMany('App\TindakanPengaduan', 'id_pengaduan');
    }
}
