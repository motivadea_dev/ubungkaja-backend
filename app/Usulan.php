<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usulan extends Model
{
    protected $table = 'usulan';

    protected $fillable = [
    	'tanggal',
    	'judul',
    	'id_kategori',
		'id_warga',
		'lokasi',
		'longitude', 
		'latitude',
		'volume', 
		'satuan', 
		'pria',
		'wanita',
		'rtm',
		'deskripsi',
		'status',
		'prioritas',
		'rangking',    	        
    	'created_at',
        'updated_at'
    ];

 	public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
    public function kategori(){
    	return $this->belongsTo('App\Kategori', 'id_kategori');
	}
	public function fotousulan(){
        return $this->hasMany('App\FotoUsulan', 'id_usulan');
	}
	public function proposalusulan(){
        return $this->hasMany('App\ProposalUsulan', 'id_usulan');
    }
}
