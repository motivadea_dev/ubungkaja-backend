<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
    protected $table = 'toko';

    protected $fillable = [
    	'id_warga',
    	'kodetoko',
    	'namatoko',
        'alamat',
        'foto',
        'status',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many dari
    public function transaksipenjualan(){
        return $this->hasMany('App\TransaksiPenjualan', 'id_toko');
    }
    public function transaksipembelian(){
        return $this->hasMany('App\TransaksiPembelian', 'id_toko');
    }
    public function nofitikasitoko(){
        return $this->hasMany('App\NotifikasiToko', 'id_toko');
    }
    public function produktoko(){
        return $this->hasMany('App\ProdukToko', 'id_toko');
    }
    public function pesan(){
        return $this->hasMany('App\Pesan', 'id_toko');
    }
    public function perangkattoko(){
        return $this->hasMany('App\PerangkatToko', 'id_toko');
    }
    //Relasi One to Many ke
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
    
}
