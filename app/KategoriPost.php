<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriPost extends Model
{
    protected $table = 'kategoripost';

    protected $fillable = [
        'namakategori',
        'foto'
    ];

    public function posting(){
        return $this->hasMany('App\Posting', 'id_kategoripost');
    }
}
