<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnggotaKeluarga extends Model
{
    protected $table = 'anggotakeluarga';

    protected $fillable = [
    	'id_keluarga',
    	'id_warga',
    	'hubungan',   	        
    	'created_at',
        'updated_at'
    ];

 	public function keluarga(){
        return $this->belongsTo('App\Keluarga', 'id_keluarga');
    }
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
}
