<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antrian extends Model
{
    protected $table = 'antrian';

    protected $fillable = [
        'tanggal',
        'id_warga',
        'id_keperluan',
        'status_syarat',
        'status_jadi',      
    	'created_at',
        'updated_at'
    ];

    public function fotoantrian(){
        return $this->hasMany('App\FotoAntrian', 'id_antrian');
    }
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
    public function keperluan(){
        return $this->belongsTo('App\Keperluan', 'id_keperluan');
    }
}
