<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukToko extends Model
{
    protected $table = 'produktoko';

    protected $fillable = [
        'id_kategoriproduk',
        'id_subkategoriproduk',
    	'id_toko',
        'kodeproduk',
    	'namaproduk',
        'stok',
        'harga',
        'diskon',
        'foto',
        'created_at',
        'updated_at'
    ];

    //Relasi Many to Many ke
    public function warga(){
        return $this->belongsToMany('App\Warga', 'favorit', 'id_produktoko', 'id_warga');
    }
    /*public function transaksipenjualan(){
        return $this->belongsToMany('App\TransaksiPenjualan', 'detailpenjualan', 'id_produktoko', 'id_transaksipenjualan');
    }*/
    //Relasi One to Many ke
    public function kategoriproduk(){
        return $this->belongsTo('App\KategoriProduk', 'id_kategoriproduk');
    }
    public function subkategoriproduk(){
        return $this->belongsTo('App\SubKategoriProduk', 'id_subkategoriproduk');
    }
    public function toko(){
        return $this->belongsTo('App\Toko', 'id_toko');
    }
    public function keranjang(){
        return $this->hasMany('App\Keranjang', 'id_produktoko');
    }
    public function detailpenjualan(){
        return $this->hasMany('App\DetailPenjualan', 'id_produktoko');
    }
    public function fotoproduk(){
        return $this->hasMany('App\FotoProduk', 'id_produktoko');
    }
}
