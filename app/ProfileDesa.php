<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileDesa extends Model
{
    protected $table = 'profiledesa';

    protected $fillable = [
    	'nama',
    	'kecamatan',
    	'kabupaten',
        'provinsi',
        'nip',
        'nama_kades',
        'created_at',
        'updated_at'
    ];

    public function dusun(){
        return $this->hasMany('App\Dusun', 'id_profiledesa');
    }
    public function berita(){
        return $this->hasMany('App\Berita', 'id_profiledesa');
    }
    public function kategori(){
        return $this->hasMany('App\Kategori', 'id_profiledesa');
    }
    public function warga(){
        return $this->hasMany('App\Warga', 'id_profiledesa');
    }
    public function distributor(){
        return $this->hasMany('App\Distributor', 'id_profiledesa');
    }
    public function keperluan(){
        return $this->hasMany('App\Keperluan', 'id_profiledesa');
    }
    public function keluarga(){
        return $this->hasMany('App\Keluarga', 'id_profiledesa');
    }
    public function informasi(){
        return $this->hasMany('App\Informasi', 'id_profiledesa');
    }
}
