<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPenjualan extends Model
{
    protected $table = 'detailpenjualan';

    protected $fillable = [
        'id_transaksipenjualan',
        'id_produktoko',
        'jumlah',
        'created_at',
        'updated_at'
    ];
    //Relasi One to Many ke
    public function transaksipenjualan(){
        return $this->belongsTo('App\TransaksiPenjualan', 'id_transaksipenjualan');
    }
    public function produktoko(){
        return $this->belongsTo('App\ProdukToko', 'id_produktoko');
    }
}
