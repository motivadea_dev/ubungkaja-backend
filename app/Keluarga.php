<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
    protected $table = 'keluarga';

    protected $fillable = [
    	'nokk',
    	'tanggal',
    	'id_profiledesa',   	        
    	'created_at',
        'updated_at'
    ];

 	public function profiledesa(){
        return $this->belongsTo('App\ProfileDesa', 'id_profiledesa');
    }
	public function anggotakeluarga(){
        return $this->hasMany('App\AnggotaKeluarga', 'id_keluarga');
    }
    public function sertifikat(){
        return $this->hasMany('App\Sertifikat', 'id_keluarga');
    }
}
