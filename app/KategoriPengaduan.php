<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriPengaduan extends Model
{
    protected $table = 'kategoripengaduan';

    protected $fillable = [
        'id_profiledesa',
    	'namakategori',        
    	'created_at',
        'updated_at'
    ];

    public function pengaduan(){
        return $this->hasMany('App\Pengaduan', 'id_kategoripengaduan');
    }
    public function profiledesa(){
        return $this->belongsTo('App\ProfileDesa', 'id_profiledesa');
    }
}
