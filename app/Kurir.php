<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurir extends Model
{
    protected $table = 'kurir';
    
    protected $fillable = [
        'id_warga',
        'nomor_kurir',
        'plat_nomor',
        'jenis_kendaraan',
        'status',
        'created_at',
        'updated_at'
    ];
    
    //Relasi One to Many dari
    public function pengiriman(){
        return $this->hasMany('App\Pengiriman', 'id_kurir');
    }

    //Relasi One to Many ke
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
}
