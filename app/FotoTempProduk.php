<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoTempProduk extends Model
{
    protected $table = 'fototempproduk';

    protected $fillable = [
        'id_toko',
        'foto',  	        
    	'created_at',
        'updated_at'
    ];
}
