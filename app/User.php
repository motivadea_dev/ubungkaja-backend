<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'username', 
        'password',
        'level',
        'status',
        'fotoktp',
        'fotowajah',
        'nohp',
        'email',
        'id_dusun'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function dusun(){
        return $this->belongsTo('App\Dusun', 'id_dusun');
    }
}
