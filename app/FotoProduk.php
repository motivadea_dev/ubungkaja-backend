<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoProduk extends Model
{
    protected $table = 'fotoproduk';

    protected $fillable = [
        'id_produktoko',
        'foto',  	        
    	'created_at',
        'updated_at'
    ];

 	public function produktoko(){
        return $this->belongsTo('App\ProdukToko', 'id_produktoko');
    }
}
