<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penghasilan extends Model
{
    protected $table = 'penghasilan';

    protected $fillable = [
    	'namapenghasilan', 	        
    	'created_at',
        'updated_at'
    ];
}

