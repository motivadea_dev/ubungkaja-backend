<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarPengaduan extends Model
{
    protected $table = 'komentarpengaduan';

    protected $fillable = [
        'id_pengaduan',
        'id_warga',
        'tanggal',
        'komentar',  	        
    	'created_at',
        'updated_at'
    ];

 	public function pengaduan(){
        return $this->belongsTo('App\Pengaduan', 'id_pengaduan');
    }
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
}
