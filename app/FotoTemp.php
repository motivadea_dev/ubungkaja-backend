<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoTemp extends Model
{
    protected $table = 'fototemp';

    protected $fillable = [
        'id_warga',
        'foto',  	        
    	'created_at',
        'updated_at'
    ];
}
