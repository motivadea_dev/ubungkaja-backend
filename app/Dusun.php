<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dusun extends Model
{
    protected $table = 'dusun';

    protected $fillable = [
        'id_profiledesa',
    	'nama',        
    	'created_at',
        'updated_at'
    ];

    public function warga(){
    	return $this->hasMany('App\Warga', 'id_dusun');
    }
    public function user(){
    	return $this->hasMany('App\User', 'id_dusun');
    }
    public function pengaduan(){
        return $this->hasMany('App\Pengaduan', 'id_dusun');
    }

    public function profiledesa(){
        return $this->belongsTo('App\ProfileDesa', 'id_profiledesa');
    }
}
