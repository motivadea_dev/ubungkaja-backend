<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polling extends Model
{
    protected $table = 'polling';

    protected $fillable = [
        'id_posting',
        'tanggal',
        'nama',
        'email',  	        
    	'created_at',
        'updated_at'
    ];

 	public function posting(){
        return $this->belongsTo('App\Posting', 'id_posting');
    }
}
