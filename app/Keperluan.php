<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keperluan extends Model
{
    protected $table = 'keperluan';

    protected $fillable = [
        'id_profiledesa',
        'nama_keperluan',  
        'syarat',      
    	'created_at',
        'updated_at'
    ];

    public function antrian(){
        return $this->hasMany('App\Antrian', 'id_keperluan');
    }
    public function profiledesa(){
        return $this->belongsTo('App\ProfileDesa', 'id_profiledesa');
    }
}
