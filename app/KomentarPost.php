<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarPost extends Model
{
    protected $table = 'komentarpost';

    protected $fillable = [
        'id_posting',
        'tanggal',
        'nama',
        'email',
        'komentar',  	        
    	'created_at',
        'updated_at'
    ];

 	public function posting(){
        return $this->belongsTo('App\Posting', 'id_posting');
    }
    public function balaskomentar(){
        return $this->hasMany('App\BalasKomentar', 'id_komentarpost');
    }
}
