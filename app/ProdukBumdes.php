<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukBumdes extends Model
{
    protected $table = 'produkbumdes';

    protected $fillable = [
        'id_kategoriproduk',
        'id_subkategoriproduk',
        'id_distributor',
        'kodeproduk',
    	'namaproduk',
        'stok',
        'harga',
        'diskon',
        'foto',
        'created_at',
        'updated_at'
    ];

    //Relasi Many to Many ke
    /*public function transaksipembelian(){
        return $this->belongsToMany('App\TransaksiPembelian', 'detailpembelian', 'id_produkbumdes', 'id_transaksipembelian');
    }*/
    //Relasi One to Many ke
    public function kategoriproduk(){
        return $this->belongsTo('App\KategoriProduk', 'id_kategoriproduk');
    }
    public function subkategoriproduk(){
        return $this->belongsTo('App\SubKategoriProduk', 'id_subkategoriproduk');
    }
    public function distributor(){
        return $this->belongsTo('App\Distributor', 'id_distributor');
    }
    public function keranjangtoko(){
        return $this->hasMany('App\KeranjangToko', 'id_produkbumdes');
    }
    public function detailpembelian(){
        return $this->hasMany('App\DetailPembelian', 'id_produkbumdes');
    }
}
