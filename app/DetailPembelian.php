<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPembelian extends Model
{
    protected $table = 'detailpembelian';

    protected $fillable = [
        'id_transaksipembelian',
        'id_produkbumdes',
        'jumlah',
        'created_at',
        'updated_at'
    ];
    //Relasi One to Many ke
    public function transaksipembelian(){
        return $this->belongsTo('App\TransaksiPembelian', 'id_transaksipembelian');
    }
    public function produkbumdes(){
        return $this->belongsTo('App\ProdukBumdes', 'id_produkbumdes');
    }
}
