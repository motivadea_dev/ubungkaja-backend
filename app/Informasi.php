<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informasi extends Model
{
    protected $table = 'informasi';

    protected $fillable = [
    	'judul',
        'deskripsi',
        'foto',
    	'id_profiledesa',   	        
    	'created_at',
        'updated_at'
    ];

 	public function profiledesa(){
        return $this->belongsTo('App\ProfileDesa', 'id_profiledesa');
    }
}
