<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $table = 'keranjang';

    protected $fillable = [
        'id_warga',
        'id_produktoko',  
        'jumlah',      
    	'created_at',
        'updated_at'
    ];

    public function produktoko(){
        return $this->belongsTo('App\ProdukToko', 'id_produktoko');
    }
}
