<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posting extends Model
{
    protected $table = 'posting';

    protected $fillable = [
        'id_kategoripost',
    	'id_warga',
        'tanggal',
    	'judul',
        'isipost',
        'foto',
        'tag',
        'status',
        'headline',
        'view',
        'created_at',
        'updated_at'
    ];

    public function kategoripost(){
        return $this->belongsTo('App\KategoriPost', 'id_kategoripost');
    }
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
    public function komentarpost(){
        return $this->hasMany('App\KomentarPost', 'id_posting');
    }
    public function polling(){
        return $this->hasMany('App\Polling', 'id_posting');
    }
}
