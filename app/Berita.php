<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';

    protected $fillable = [
        'id_profiledesa',
    	'tahap',
    	'tanggalmulai',
    	'tanggalselesai',
        'created_at',
        'updated_at'
    ];
    public function profiledesa(){
        return $this->belongsTo('App\ProfileDesa', 'id_profiledesa');
    }
}
