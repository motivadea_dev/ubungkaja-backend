<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Warga;
use App\Keperluan;

class FormAntrianServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('antrian.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarwarga', Warga::lists('nama', 'id'));
            $view->with('daftarkeperluan', Keperluan::lists('nama_keperluan', 'id'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
