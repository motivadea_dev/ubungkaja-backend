<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Warga;
use App\KategoriPengaduan;
use App\ProfileDesa;
use App\Dusun;

class FormPengaduanServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('pengaduan.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarwarga', Warga::lists('nama', 'id'));
            $view->with('daftarkategoripengaduan', KategoriPengaduan::lists('namakategori', 'id'));
            $view->with('daftardusun', Dusun::lists('nama', 'id'));
        });
        view()->composer('kategoripengaduan.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftardesa', ProfileDesa::lists('nama', 'id'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
