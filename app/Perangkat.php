<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perangkat extends Model
{
    protected $table = 'perangkat';

    protected $fillable = [
		'id_warga',
		'app_id',  	        
    	'created_at',
        'updated_at'
    ];

 	public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
}
