<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataUser extends Model
{
    protected $table = 'datauser';
    protected $primaryKey = 'id_warga';
    //Hanya jika semua data disimpan tanpa seleksi
    protected $fillable = [
    	'id_warga',
    	'nomor_telepon',
    	'email'
    ];
    //Relasi One to One ke warga
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
}
