<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerangkatToko extends Model
{
    protected $table = 'perangkattoko';

    protected $fillable = [
		'id_toko',
		'app_id',  	        
    	'created_at',
        'updated_at'
    ];

 	public function toko(){
        return $this->belongsTo('App\Toko', 'id_toko');
    }
}
