<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiPembelian extends Model
{
    protected $table = 'transaksipembelian';
    
    protected $fillable = [
        'id_toko',
        'id_distributor',
        'tanggal',
        'totaldiskon',
        'totalbelanja',
        'subtotal',
        'status',
        'created_at',
        'updated_at'
    ];
    
    //Relasi Many to Many ke
    /*public function produkbumdes(){
        return $this->belongsToMany('App\ProdukBumdes', 'detailpembelian', 'id_transaksipembelian', 'id_produkbumdes');
    }*/
    public function detailpembelian(){
        return $this->hasMany('App\DetailPembelian', 'id_transaksipembelian');
    } 
    //Relasi One to Many dari
    public function pengiriman(){
        return $this->hasMany('App\Pengiriman', 'id_transaksipembelian');
    }

    //Relasi One to Many ke
    public function toko(){
        return $this->belongsTo('App\Toko', 'id_toko');
    }
    public function distributor(){
        return $this->belongsTo('App\Distributor', 'id_distributor');
    }
}
