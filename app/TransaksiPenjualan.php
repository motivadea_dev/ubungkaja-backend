<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiPenjualan extends Model
{
    protected $table = 'transaksipenjualan';

    protected $fillable = [
        'id_toko',
        'id_warga',
    	'tanggal',
        'totaldiskon',
        'totalbelanja',
        'subtotal',
        'status',
        'created_at',
        'updated_at'
    ];

    //Relasi Many to Many ke
    /*public function produktoko(){
        return $this->belongsToMany('App\ProdukToko', 'detailpenjualan', 'id_transaksipenjualan', 'id_produktoko');
    }*/
    public function detailpenjualan(){
        return $this->hasMany('App\DetailPenjualan', 'id_transaksipenjualan');
    }
    //Relasi One to Many ke
    public function toko(){
        return $this->belongsTo('App\Toko', 'id_toko');
    }
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
}
