<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    protected $table = 'pendidikan';

    protected $fillable = [
    	'namapendidikan', 	        
    	'created_at',
        'updated_at'
    ];
}
