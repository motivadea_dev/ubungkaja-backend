<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    protected $table = 'distributor';

    protected $fillable = [
    	'id_profiledesa',
    	'kodedistributor',
    	'namadistributor',
        'alamat',
        'foto',
        'status',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many dari
    public function transaksipembelian(){
        return $this->hasMany('App\TransaksiPembelian', 'id_distributor');
    }
    public function produkbumdes(){
        return $this->hasMany('App\ProdukBumdes', 'id_distributor');
    }
    //Relasi One to Many ke
    public function profiledesa(){
        return $this->belongsTo('App\ProfileDesa', 'id_profiledesa');
    }
}
