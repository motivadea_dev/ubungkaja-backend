<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesan extends Model
{
    protected $table = 'pesan';
    
    protected $fillable = [
        'id_warga',
        'id_toko',
        'tanggal',
        'isi_pesan',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many ke
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
    public function toko(){
        return $this->belongsTo('App\Toko', 'id_toko');
    }
}
