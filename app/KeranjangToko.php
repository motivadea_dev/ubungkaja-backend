<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeranjangToko extends Model
{
    protected $table = 'keranjangtoko';

    protected $fillable = [
        'id_toko',
        'id_produkbumdes',  
        'jumlah',      
    	'created_at',
        'updated_at'
    ];

    public function produkbumdes(){
        return $this->belongsTo('App\ProdukBumdes', 'id_produkbumdes');
    }
}
