<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//ROUTE WEBSITE
//=====================================================================
//Home CMS
Route::get('/home', 'HomePageController@index');
//View Posting
Route::get('/informasi/{kategori}', 'HomePageController@informasi');
//View Detail Posting + Komentar
Route::get('/detailinformasi/{posting}', 'HomePageController@detailinformasi');
//Simpan Komentar
Route::post('komentar','HomePageController@storekomentar');
//Simpan Balasan
Route::post('balasan','HomePageController@storebalasan');
//Informasi Usulan
Route::get('/usulanwarga', 'HomePageController@usulanwarga');
//Informasi Usulan
Route::get('/usulanwarga/{kategori}', 'HomePageController@usulanwarga2');
//View Detail Usulan
Route::get('/detailusulan/{usulan}', 'HomePageController@detailusulan');
//Informasi Pengaduan
Route::get('/pengaduanwarga', 'HomePageController@pengaduanwarga');
//Informasi Usulan
Route::get('/pengaduanwarga/{kategori}', 'HomePageController@pengaduanwarga2');
//View Detail Usulan
Route::get('/detailpengaduan/{pengaduan}', 'HomePageController@detailpengaduan');
//Simpan Komentar pengaduan
Route::post('komentarpengaduan','HomePageController@komentarpengaduan');
//Simpan Balasan pengaduan
Route::post('balaspengaduan','HomePageController@balaspengaduan');
//daftarperangkat
Route::get('/daftarperangkat', 'HomePageController@daftarperangkat');
//Simpan Registrasi Warga
Route::post('registrasi','HomePageController@registrasi');
//Simpan Registrasi Desa
Route::post('registrasidesa','HomePageController@registrasidesa');

//HALAMAN ADMIN
//=====================================================================
Route::group(['middleware' => ['web']], function(){
    //Controller Untuk Cek Login
    Route::auth();
    Route::get('/', 'HomeController@index');
    Route::resource('user','UserController');
    Route::patch('user/{user}','UserController@verifikasi');
    //WARGA
    Route::get('warga/cari','WargawebController@cari');
    Route::resource('warga','WargawebController');
    Route::get('warga/print/{warga}',[
    'uses' => 'WargawebController@getPdf',
    'as' => 'warga.print',]);
    Route::post('importExcel', 'WargawebController@importExcel');
    //USULAN
    Route::get('usulan/cari','UsulanwebController@cari');
    Route::get('usulan/printsemua',[
    'uses' => 'UsulanwebController@getPdfSemua',
    'as' => 'usulan.printsemua',]);
    Route::get('hasil/printsemua/{dusun}',[
        'uses' => 'UsulanwebController@getPdfHasil',
        'as' => 'usulan.printhasil',]);
    Route::get('hasil','UsulanwebController@hasil');
    Route::get('hasil/{dusun}','UsulanwebController@detailhasil');
    Route::resource('usulan','UsulanwebController');
    Route::patch('usulan/{usulan}','UsulanwebController@verifikasi');
    Route::get('usulan/print/{usulan}',[
    'uses' => 'UsulanwebController@getPdf',
    'as' => 'usulan.print',]);
    Route::get('usulannaik/{usulan}','UsulanwebController@rangkingnaik');
    Route::get('usulanturun/{usulan}','UsulanwebController@rangkingturun');
    //DUSUN
    Route::resource('dusun','DusunwebController');
    //KATEGORI
    Route::resource('kategori','KategoriwebController');
    //BERITA
    Route::resource('berita','BeritawebController');
    //PROFILE DESA
    Route::resource('profiledesa','ProfiledesawebController');
    //PENDIDIKAN
    Route::resource('pendidikan','PendidikanwebController');
    //PEKERJAAN
    Route::resource('pekerjaan','PekerjaanwebController');
    //PENGHASILAN
    Route::resource('penghasilan','PenghasilanwebController');

    //E-COMERCE
    //distributor
    Route::resource('distributor','DistributorwebController');
    //toko
    Route::resource('toko','TokowebController');
    //kategori produk
    Route::resource('kategoriproduk','KategoriprodukwebController');
    //Sub kategori produk
    Route::resource('subkategoriproduk','SubKategoriprodukwebController');
    //produk toko
    Route::resource('produktoko','ProduktokowebController');
    //produk bumdes
    Route::resource('produkbumdes','ProdukbumdeswebController');
    //penjualan
    Route::resource('transaksipenjualan','TransaksiPenjualanwebController');
    //pembeilan
    Route::resource('transaksipenjualanbumdes','TransaksiPenjualanBumdeswebController');
    //Pengiriman
    Route::resource('pengiriman','PengirimanwebController');
    //pesan
    Route::resource('pesan','PesanwebController');
    //Dompet
    Route::resource('dompet','DompetwebController');
    //kurir
    Route::resource('kurir','KurirwebController');

    //PENGADUAN
    Route::resource('pengaduan','PengaduanwebController');
    Route::get('pengaduan/komentar/{pengaduan}','PengaduanwebController@indexkomentar');
    Route::delete('pengaduan/komentar/{komentar}', 'PengaduanwebController@destroykomentar');
    Route::get('pengaduan/tindakan/{pengaduan}','PengaduanwebController@indextindakan');
    Route::delete('pengaduan/tindakan/{tindakan}', 'PengaduanwebController@destroytindakan');
    //KATEGORI PENGADUAN
    Route::resource('kategoripengaduan','KategoriPengaduanwebController');

    //ANTRIAN
    Route::resource('antrian','AntrianwebController');
    //KATEGORI LAYANAN
    Route::resource('keperluan','KeperluanwebController');
    
    //KELUARGA
    Route::resource('keluarga','KeluargawebController');
    Route::get('keluarga/anggota/{keluarga}','KeluargawebController@indexanggota');
    Route::get('keluarga/createanggota/{keluarga}','KeluargawebController@createanggota');
    Route::post('keluarga/anggota','KeluargawebController@storeanggota');
    Route::delete('keluarga/anggota/{anggota}', 'KeluargawebController@destroyanggota');

    //CMS Kategoripost
    Route::resource('kategoripost','KategoripostwebController');

    //CMS posting
    Route::resource('posting','PostingwebController');
    Route::patch('posting/{posting}','PostingwebController@verifikasi');
    Route::get('posting/komentar/{posting}','PostingwebController@indexkomentar');
    Route::delete('posting/komentar/{komentar}', 'PostingwebController@destroykomentar');
    Route::delete('posting/balasan/{balasan}', 'PostingwebController@destroybalasan');
    Route::post('posting/balasan','PostingwebController@balasan');
    //CMS posting
    Route::resource('komentarpost','KomentarpostwebController');

});

// API MOBILE APPS 
//=====================================================================
//HALAMAN AWAL
Route::get('api/halamanawal', 'WargaController@indexawal');
//HALAMAN POSTING INFORMASI
Route::get('api/informasi/{item}', 'WargaController@informasi'); 
Route::get('api/informasi/detail/{item}', 'WargaController@detailinformasi'); 
Route::get('api/informasi/komentar/{item}', 'WargaController@komentarinformasi'); 
//SIMPAN BALASAN
Route::post('api/informasi/komentar','WargaController@storekomentar');
//SIMPAN KOMENTAR
Route::post('api/informasi/balasan','WargaController@storebalasan');

//TOKO
//=====================================================================
//INDEX
Route::get('api/toko/{item}', 'WargaController@cektoko');       
//DAFTAR
Route::post('api/toko','WargaController@daftartoko');           
//UPDATE
Route::put('api/toko','WargaController@updatetoko');           
//PERANGKAT TOKO
Route::post('api/perangkattoko','WargaController@perangkattoko'); 
//USULAN
//=====================================================================
//1.WARGA
//INDEX
Route::get('api/warga/{item}', 'WargaController@indexapp');
//LOGIN
Route::get('api/loginuser','WargaController@cekkosong');
Route::get('api/loginuser/{noktp}/password/{password}', [
'as' => 'login', 'uses' => 'WargaController@loginuser']);
//UPDATE
Route::put('api/warga','WargaController@updateapp');
//SIMPAN PERANGKAT
Route::post('api/perangkat','WargaController@storeapp');
//DAFTAR USER BARU
Route::get('api/daftaruser/{item}','WargaController@cekdaftar');
Route::post('api/daftaruser','WargaController@daftaruser');
//UPLOAD
Route::post('api/uploadktp','WargaController@uploadKtp');
Route::post('api/uploadwajah','WargaController@uploadWajah');
Route::post('api/uploadtoko','WargaController@uploadToko');     
//2.USULAN
//INDEX DUSUN
Route::get('api/usulandusun/{item}', 'UsulanController@indexdusun'); 
//INDEX
Route::get('api/usulan/{item}', 'UsulanController@indexapp');
//USULAN TERBARU
Route::get('api/usulanbaru', 'UsulanController@usulanbaru');  
//SIMPAN
Route::post('api/usulan','UsulanController@storeapp');
//UPLOAD
Route::post('api/upload','UsulanController@uploadFoto');
//UPLOAD PROPOSAL
Route::post('api/uploadproposal','UsulanController@uploadProposal');
//UPDATE DUSUN
Route::put('api/usulandusun/{item}','UsulanController@updatedusun');
//UPDATE
Route::put('api/usulan/{item}','UsulanController@updateapp');
//HAPUS
Route::delete('api/usulan/{item}', 'UsulanController@destroyapp');
//HAPUS FOTO
Route::delete('api/hapusfoto/{item}', 'UsulanController@destroyfoto');
//HAPUS PROPOSAL
Route::delete('api/hapusproposal/{item}', 'UsulanController@destroyprop');
//3.DUSUN
//INDEX
Route::get('api/dusun', 'DusunController@indexapp');
//SIMPAN
Route::post('api/dusun','DusunController@storeapp');
//UPDATE
Route::put('api/dusun/{item}','DusunController@updateapp');
//HAPUS
Route::delete('api/dusun/{item}', 'DusunController@destroyapp');
//4.KATEGORI
//INDEX
Route::get('api/kategori', 'KategoriController@indexapp');
//SIMPAN
Route::post('api/kategori','KategoriController@storeapp');
//UPDATE
Route::put('api/kategori/{item}','KategoriController@updateapp');
//HAPUS
Route::delete('api/kategori/{item}', 'KategorController@destroyapp');
//5.BERITA
//INDEX
Route::get('api/berita/{item}', 'BeritaController@indexapp');
//HASIL
Route::get('api/beritahasil/{item}','BeritaController@detailhasil');
//SIMPAN
Route::post('api/berita','BeritaController@storeapp');
//UPDATE
Route::put('api/berita/{item}','BeritaController@updateapp');
//HAPUS
Route::delete('api/berita/{item}', 'BeritaController@destroyapp');
//6.PROFILE DESA
//INDEX
Route::get('api/profiledesa', 'ProfiledesaController@indexapp');
//SIMPAN
Route::post('api/profiledesa','ProfiledesaController@storeapp');
//UPDATE
Route::put('api/profiledesa/{item}','ProfiledesaController@updateapp');
//HAPUS
Route::delete('api/profiledesa/{item}', 'ProfiledesaController@destroyapp');

//ECOMMERCE
//=====================================================================
//1. KATEGORI PRODUK
//INDEX
Route::get('api/kategoriproduk','KategoriProdukController@indexapp');
//SUB KATEGORI
Route::get('api/kategoriproduk/{item}','KategoriProdukController@subkategoriapp');
//SUB KATEGORI 2
Route::get('api/kategoriproduk/idkat/{idkat}/idsub/{idsub}', [
'as' => 'subkategori', 'uses' => 'KategoriProdukController@subkategoriapp2']);
//2. KATEGORI PRODUK BUMDES
//INDEX PRODUK BUMDES
Route::get('api/kategoriprodukbumdes','KategoriProdukController@indexappBumdes');
//SUB KATEGORI BUMDES
Route::get('api/kategoriprodukbumdes/{item}','KategoriProdukController@subkategoriappBumdes');
//3. PRODUK TOKO
//INDEX
Route::get('api/produktoko/{item}','ProdukTokoController@indexapp');
//CARI
Route::get('api/produktoko/cari/{item}','ProdukTokoController@cariapp');
//CARI BUMDES
Route::get('api/produkbumdes/cari/{item}','ProdukTokoController@cariappBumdes');
//UPLOAD
Route::post('api/uploadproduk','ProdukTokoController@uploadFoto');
//SIMPAN
Route::post('api/produktoko','ProdukTokoController@storeapp');
//UPDATE
Route::put('api/produktoko/{item}','ProdukTokoController@updateapp');
//HAPUS
Route::delete('api/produktoko/{item}', 'ProdukTokoController@destroyapp');
//4. KERANJANG
//INDEX
Route::get('api/keranjang/{item}','KeranjangController@indexapp');
//SIMPAN
Route::post('api/keranjang','KeranjangController@storeapp');
//UPDATE
Route::put('api/keranjang/{item}','KeranjangController@updateapp');
//HAPUS
Route::delete('api/keranjang/{item}', 'KeranjangController@destroyapp');
//5. KERANJANG TOKO
//INDEX
Route::get('api/keranjangtoko','KeranjangController@indexappToko');
//SIMPAN
Route::post('api/keranjangtoko','KeranjangController@storeappToko');
//UPDATE
Route::put('api/keranjangtoko/{item}','KeranjangController@updateappToko');
//HAPUS
Route::delete('api/keranjangtoko/{item}', 'KeranjangController@destroyappToko');
//6. PEMBELIAN (WARGA)
//INDEX
Route::get('api/penjualan/{item}','TransaksiPenjualanController@indexapp');
//DETAIL
Route::get('api/penjualan/detail/{item}','TransaksiPenjualanController@detail');
//SIMPAN
Route::post('api/penjualan','TransaksiPenjualanController@storeapp');
//UPDATE
Route::put('api/penjualan','TransaksiPenjualanController@updateapp');
//HAPUS
Route::delete('api/penjualan/{item}', 'TransaksiPenjualanController@destroyapp');
//7. PENJUALAN (TOKO)
//INDEX
Route::get('api/penjualantoko/{item}','TransaksiPenjualanController@indexappToko');
//DETAIL
Route::get('api/penjualantoko/detail/{item}','TransaksiPenjualanController@detailToko');
//UPDATE
Route::put('api/penjualantoko','TransaksiPenjualanController@updateappToko');
//8. PEMESANAN (TOKO)
//INDEX
Route::get('api/pemesanan','TransaksiPenjualanBumdesController@indexapp');
//DETAIL
Route::get('api/pemesanan/{item}','TransaksiPenjualanBumdesController@detail');
//SIMPAN
Route::post('api/pemesanan','TransaksiPenjualanBumdesController@storeapp');
//UPDATE
Route::put('api/pemesanan/{item}','TransaksiPenjualanBumdesController@updateapp');
//HAPUS
Route::delete('api/pemesanan/{item}', 'TransaksiPenjualanBumdesController@destroyapp');
//9. PENJUALAN (BUMDES)
//INDEX
Route::get('api/penjualanbumdes','TransaksiPenjualanBumdesController@indexappBumdes');
//DETAIL
Route::get('api/penjualanbumdes/{item}','TransaksiPenjualanBumdesController@detailBumdes');
//UPDATE
Route::put('api/penjualanbumdes/{item}','TransaksiPenjualanBumdesController@updateappBumdes');

//PENGADUAN
//=====================================================================
//INDEX
Route::get('api/pengaduan', 'PengaduanController@indexapp');
//SIMPAN
Route::post('api/pengaduan','PengaduanController@storeapp');
//UPLOAD
Route::post('api/uploadpengaduan','PengaduanController@uploadFoto');
//UPDATE
Route::put('api/pengaduan/{item}','PengaduanController@updateapp');
//HAPUS
Route::delete('api/pengaduan/{item}', 'PengaduanController@destroyapp');

//ANTRIAN
//=====================================================================
//INDEX
Route::get('api/antrian', 'AntrianController@indexapp');
//SIMPAN
Route::post('api/antrian','AntrianController@storeapp');
//UPLOAD
Route::post('api/uploadantrian','AntrianController@uploadFoto');
//UPDATE
Route::put('api/antrian/{item}','AntrianController@updateapp');
//HAPUS
Route::delete('api/antrian/{item}', 'AntrianController@destroyapp');

//POSTING
//INDEX
Route::get('api/posting/{item}', 'PostingController@indexapp');
//USULAN TERBARU
Route::get('api/postingbaru', 'PostingController@postingbaru');
//SIMPAN
Route::post('api/posting','PostingController@storeapp');
//UPLOAD
Route::post('api/uploadpost','PostingController@uploadFoto');
//UPDATE
Route::put('api/posting/{item}','PostingController@updateapp');
//HAPUS
Route::delete('api/posting/{item}', 'PostingController@destroyapp');

