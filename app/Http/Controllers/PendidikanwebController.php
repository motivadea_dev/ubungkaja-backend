<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PendidikanRequest;
use App\Http\Controllers\Controller;

use App\Pendidikan;
use Session;


class PendidikanwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarpendidikan = Pendidikan::orderBy('id','asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahpendidikan = Pendidikan::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('pendidikan.index', compact('daftarpendidikan','jumlahpendidikan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pendidikan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PendidikanRequest $request, Pendidikan $pendidikan)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $pendidikan = Pendidikan::create($input);
        Session::flash('flash_message', 'Pendidikan Berhasil Disimpan');
        return redirect('pendidikan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pendidikan $pendidikan)
    {
        return view('pendidikan.edit', compact('pendidikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PendidikanRequest $request,Pendidikan $pendidikan)
    {
        $input = $request->all();
        
                $pendidikan->update($input);
        
                Session::flash('flash_message', 'Pendidikan berhasil diupdate');
                return redirect('pendidikan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pendidikan $pendidikan)
    {
        $pendidikan->delete();
        Session::flash('flash_message', 'Pendidikan berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('pendidikan');
    }
}
