<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PengaduanRequest;
use App\Http\Controllers\Controller;
use Storage;
use App\Pengaduan;
use App\Warga;
use App\KategoriPengaduan;
use App\Dusun;
use App\KomentarPengaduan;
use App\TindakanPengaduan;
use Session;
use PDF;
use Mapper;
class PengaduanwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
           $daftarpengaduan = Pengaduan::with('fotopengaduan')->orderBy('tanggal', 'desc')->paginate(20);;
           //2. Menghitung total keseluruhan jumlah warga
           $jumlahpengaduan = Pengaduan::count();
           //3. MEMBUAT DATA API / JSON UNTUK 
           return view('pengaduan.index', compact('daftarpengaduan','jumlahpengaduan'));
    }

    public function indexkomentar(Pengaduan $pengaduan)
    {
           $data = KomentarPengaduan::all();
           $daftarkomentar = $data->where('id_pengaduan',$pengaduan->id);
           $jumlahkomentar = $daftarkomentar->count();
           return view('pengaduan.komentar', compact('daftarkomentar','jumlahkomentar'));
    }


    public function indextindakan(Pengaduan $pengaduan)
    {
           $data = TindakanPengaduan::all();
           $daftartindakan = $data->where('id_pengaduan',$pengaduan->id);
           $jumlahtindakan = $daftartindakan->count();
           return view('pengaduan.tindakan', compact('daftartindakan','jumlahtindakan'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pengaduan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PengaduanRequest $request)
    {
        $input = $request->all();
        $pengaduan = Pengaduan::create($input);
        Session::flash('flash_message', 'Pengaduan Berhasil Disimpan');
        return redirect('pengaduan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pengaduan $pengaduan)
    {
        $map = Mapper::map(-8.6726769, 115.1542327, ['zoom' => 15, 'markers' => ['title' => 'Lokasi Pengaduan', 'animation' => 'DROP']]);
        return view('pengaduan.show',compact('pengaduan','map'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengaduan $pengaduan)
    {
        return view('pengaduan.edit', compact('pengaduan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PengaduanRequest $request,Pengaduan $pengaduan)
    {
        $input = $request->all();
        $pengaduan->update($input);
        
                Session::flash('flash_message', 'Pengaduan berhasil diupdate');
                return redirect('pengaduan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengaduan $pengaduan)
    {
        $pengaduan->delete();
        Session::flash('flash_message', 'Pengaduan berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('pengaduan');
    }

    public function destroykomentar($komentar)
    {
        $daftarkomentar = KomentarPengaduan::findOrFail($komentar);
        $daftarkomentar->delete();
        Session::flash('flash_message', 'Komentar berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('pengaduan');
    }

    public function destroytindakan($tindakan)
    {
        $daftartindakan = TindakanPengaduan::findOrFail($tindakan);
        $daftartindakan->delete();
        Session::flash('flash_message', 'Tindakan berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('pengaduan');
    }
}
