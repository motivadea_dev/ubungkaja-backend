<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\TransaksiPembelian;
use App\DetailPembelian;
use App\KeranjangToko;
use App\ProdukBumdes;

class TransaksiPenjualanBumdesController extends Controller
{
    public function indexapp()
    {
        $daftar = TransaksiPembelian::all();
        $daftartransaksi = $daftar->where('id_toko',1);
        $koleksi = collect($daftartransaksi);
        $koleksi->toJson();
        return $koleksi;
    }
    public function indexappBumdes()
    {
        $daftar = TransaksiPembelian::all();
        $daftartransaksi = $daftar->where('id_distributor',1);
        $koleksi = collect($daftartransaksi);
        $koleksi->toJson();
        return $koleksi;
    }

    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data Transaksi 
        $transaksi = TransaksiPembelian::create($input);
        //Ambil ID Warga
        $idtoko = $request->input('id_toko');
        settype($idtoko, "integer");
        //Ambil ID Transaksi
        $id_awal = TransaksiPembelian::orderBy('id', 'desc')->first();
        $idtransaksi = $id_awal->id;
        settype($idtransaksi, "integer");
        //Tampilkan Keranjang
        $daftar = KeranjangToko::all();
        $daftarkeranjang = $daftar->where('id_toko',$idtoko);
       
        //Simpan DetailPenjualan
        foreach($daftarkeranjang as $keranjang){
            $detailpembelian = New DetailPembelian;
            $detailpembelian->id_transaksipembelian = $idtransaksi;
            $detailpembelian->id_produkbumdes = $keranjang->id_produkbumdes;
            $detailpembelian->jumlah = $keranjang->jumlah;
            $detailpembelian->save();
            //Update Stok
            $produk = ProdukBumdes::findOrFail($keranjang->id_produkbumdes);
            $stokawal = $produk->stok;
            $stokakhir = $stokawal - $keranjang->jumlah;
            $produk->stok = $stokakhir;
            $produk->update();
            //Hapus Keranjang
            $keranjang->delete();
        }
        return $transaksi;
    }
    public function detail($item){
        settype($item, "integer"); 
        //Detail Penjualan
        $daftar = DetailPembelian::with('produkbumdes')->where('id_transaksipembelian',$item)->get();
        //$daftarpenjualan = $daftar->where('id_transaksipenjualan',$item);
        $daftar2 = TransaksiPembelian::where('id',$item)->get();
        //$daftarpenjualan2 = $daftar2->where('id',$item);
        
        $jumlahpenjualan = $daftar2->count();

        //Seleksi
        if($jumlahpenjualan == 0){
            $data = [
            ['id_transaksipembelian' => null, 'id_produkbumdes' => null, 'produkbumdes' => null],
            ];  
            $data2 = [
            ['id' => null, 'id_toko' => null, 'id_distributor' => null, 'tanggal' => null, 'subtotal' => null, 'status'=> null],
            ]; 
            $koleksi = collect($data);
            $koleksi->toJson();
            $koleksi2 = collect($data2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
    public function detailBumdes($item){
        settype($item, "integer"); 
        //Detail Penjualan
        $daftar = DetailPembelian::with('produkbumdes')->where('id_transaksipembelian',$item)->get();
        //$daftarpenjualan = $daftar->where('id_transaksipenjualan',$item);
        $daftar2 = TransaksiPembelian::where('id',$item)->get();
        //$daftarpenjualan2 = $daftar2->where('id',$item);
        
        $jumlahpenjualan = $daftar2->count();

        //Seleksi
        if($jumlahpenjualan == 0){
            $data = [
            ['id_transaksipembelian' => null, 'id_produkbumdes' => null, 'produkbumdes' => null],
            ];  
            $data2 = [
            ['id' => null, 'id_toko' => null, 'id_distributor' => null, 'tanggal' => null, 'subtotal' => null, 'status'=> null],
            ]; 
            $koleksi = collect($data);
            $koleksi->toJson();
            $koleksi2 = collect($data2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
    /*
    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id pengaduan
        $keranjang = Keranjang::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data pengaduan
        $keranjang->update($input);
        return $keranjang;
    }

    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id keranjang
        $keranjang = Keranjang::findOrFail($item);
        //2. Hapus data
        $keranjang->delete();
        return $keranjang;
    }*/
}
