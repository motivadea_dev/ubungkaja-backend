<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warga;
use App\Keranjang;
use App\ProdukToko;
use App\Toko;
use App\KeranjangToko;
use App\ProdukBumdes;

class KeranjangController extends Controller
{
    public function indexapp($item)
    {
        //WARGA
        settype($item, "integer");
        $daftar = Keranjang::with('produktoko')->get();
        $daftarkeranjang = $daftar->where('id_warga',$item);
        $jumlahkeranjang = $daftarkeranjang->count();
        $total = 0;
        $subtotal = 0;
        if($jumlahkeranjang == 0){
            $koleksi2 = [
                ['warga' => null,'toko' => null,'subtotal' => 0,'status' => null],
            ];
        }
        else{
            //Subtotal
            foreach($daftarkeranjang as $keranjang){
                $warga = $keranjang->id_warga;
                $toko = $keranjang->produktoko->id_toko;
                $status = "order";
                $total = $keranjang->jumlah * $keranjang->produktoko->harga;
                $subtotal = $subtotal + $total;
            }
            
            $koleksi2 = [
                ['warga' => $warga,'toko' => $toko,'subtotal' => $subtotal,'status' => $status],
            ];    
        }
        
        $koleksi = collect($daftarkeranjang);
        $koleksi->toJson();
        return compact('koleksi','koleksi2');
    }

    public function storeapp(Request $request)
    {
        $daftar = Keranjang::all();
        $keranjang = $daftar->where('id_warga',$request->id_warga)
        ->where('id_produktoko',$request->id_produktoko);
        $jumlah = $keranjang->count();
        if($jumlah == 1){
        return $keranjang;
        }
        else{
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data keranjang 
        $keranjang = Keranjang::create($input);
        return $keranjang;
        }
    }

    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id pengaduan
        $keranjang = Keranjang::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data pengaduan
        $keranjang->update($input);
        return $keranjang;
    }

    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id keranjang
        $keranjang = Keranjang::findOrFail($item);
        //2. Hapus data
        $keranjang->delete();
        return $keranjang;
    }

    //TOKO
    public function indexappToko()
    {
        $daftar = KeranjangToko::with('produkbumdes')->get();
        $daftarkeranjang = $daftar->where('id_toko',1);
        $jumlahkeranjang = $daftarkeranjang->count();
        $total = 0;
        $subtotal = 0;
        if($jumlahkeranjang == 0){
            $koleksi2 = [
                ['toko' => null,'distributor' => null,'subtotal' => 0,'status' => null],
            ];
        }
        else{
            //Subtotal
            foreach($daftarkeranjang as $keranjang){
                $toko = $keranjang->id_toko;
                $distributor = $keranjang->produkbumdes->id_distributor;
                $status = "order";
                $total = $keranjang->jumlah * $keranjang->produkbumdes->harga;
                $subtotal = $subtotal + $total;
            }
            
            $koleksi2 = [
                ['toko' => $toko,'distributor' => $distributor,'subtotal' => $subtotal,'status' => $status],
            ];    
        }
        
        $koleksi = collect($daftarkeranjang);
        $koleksi->toJson();
        return compact('koleksi','koleksi2');
    }

    public function storeappToko(Request $request)
    {
        $daftar = KeranjangToko::all();
        $keranjang = $daftar->where('id_produkbumdes',$request->id_produkbumdes);
        $jumlah = $keranjang->count();
        if($jumlah == 1){
        return $keranjang;
        }
        else{
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data keranjang 
        $keranjang = KeranjangToko::create($input);
        return $keranjang;
        }
    }

    public function updateappToko(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id pengaduan
        $keranjang = KeranjangToko::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data pengaduan
        $keranjang->update($input);
        return $keranjang;
    }

    public function destroyappToko($item)
    {
        //1. Pencarian berdasarkan Id keranjang
        $keranjang = KeranjangToko::findOrFail($item);
        //2. Hapus data
        $keranjang->delete();
        return $keranjang;
    }
}
