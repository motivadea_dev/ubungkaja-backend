<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\DistributorRequest;

use Storage;
use App\ProfileDesa;
use App\Distributor;
use Session;

class DistributorwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftardistributor = Distributor::orderBy('id', 'asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahdistributor = Distributor::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('distributor.index', compact('daftardistributor','jumlahdistributor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('distributor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistributorRequest $request)
    {
        $input = $request->all();
        
                if($request->hasFile('foto')){
                    $input['foto'] = $this->uploadFoto($request);
                }
                else{
                    $input['foto'] = "noimage.png";
                }
                //Simpan Data Distributor
                $distributor = Distributor::create($input);
                Session::flash('flash_message', 'Distributor Berhasil Disimpan');
                return redirect('distributor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Distributor $distributor)
    {
        return view('distributor.show',compact('distributor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Distributor $distributor)
    {
        return view('distributor.edit', compact('distributor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DistributorRequest $request,Distributor $distributor)
    {
        $input = $request->all();
        
                if($request->hasFile('foto')){
                    //Hapus foto lama
                    $this->hapusFoto($toko);
                    //Upload foto baru
                    $input['foto'] = $this->uploadFoto($request);
                }
        
                $distributor->update($input);
        
                Session::flash('flash_message', 'Distributor berhasil diupdate');
                return redirect('distributor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Distributor $distributor)
    {
        $this->hapusFoto($distributor);
        $distributor->delete();
        Session::flash('flash_message', 'Distributor berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('distributor');
    }

    //Upload foto ke direktori lokal
    public function uploadFoto(DistributorRequest $request){
        $foto = $request->file('foto');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('foto')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('foto')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
    }

    //Hapus foto di direktori lokal
    public function hapusFoto(Distributor $distributor){
        $exist = Storage::disk('foto')->exists($distributor->foto);
        if(isset($distributor->foto) && $exist){
           $delete = Storage::disk('foto')->delete($distributor->foto);
           if($delete){
            return true;
           }
           return false;
        }
    }
}
