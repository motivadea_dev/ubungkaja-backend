<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\KategoriPengaduanRequest;

use App\Http\Controllers\Controller;

use App\ProfileDesa;
use App\KategoriPengaduan;
use App\Pengaduan;
use Session;

class KategoriPengaduanwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarkategoripengaduan = KategoriPengaduan::orderBy('namakategori','asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahkategoripengaduan = KategoriPengaduan::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('kategoripengaduan.index', compact('daftarkategoripengaduan','jumlahkategoripengaduan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategoripengaduan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KategoriPengaduanRequest $request, KategoriPengaduan $kategoripengaduan)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $kategoripengaduan = KategoriPengaduan::create($input);
        Session::flash('flash_message', 'Kategori Berhasil Disimpan');
        return redirect('kategoripengaduan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(KategoriPengaduan $kategoripengaduan)
    {
        return view('kategoripengaduan.edit', compact('kategoripengaduan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KategoriPengaduanRequest $request,KategoriPengaduan $kategoripengaduan)
    {
        $input = $request->all();
                $kategoripengaduan->update($input);
        
                Session::flash('flash_message', 'Kategori berhasil diupdate');
                return redirect('kategoripengaduan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(KategoriPengaduan $kategoripengaduan)
    {
        $kategoripengaduan->delete();
        Session::flash('flash_message', 'Kategori berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('kategoripengaduan');
    }
}
