<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\KategoriRequest;

use App\Warga;
use App\Dusun;
use App\Kategori;
use App\Usulan;
use App\DataUser;
use Session;

class KategoriwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarkategori = Kategori::orderBy('namakategori','asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahkategori = Kategori::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('kategori.index', compact('daftarkategori','jumlahkategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KategoriRequest $request, Kategori $kategori)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $kategori = Kategori::create($input);
        Session::flash('flash_message', 'Kategori Berhasil Disimpan');
        return redirect('kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
         return view('kategori.show',compact('kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        return view('kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KategoriRequest $request, Kategori $kategori)
    {
         $input = $request->all();

        /*if($request->hasFile('foto')){
            //Hapus foto lama
            $this->hapusFoto($produk);
            //Upload foto baru
            $input['foto'] = $this->uploadFoto($request);
        }*/

        $kategori->update($input);

        Session::flash('flash_message', 'Kategori berhasil diupdate');
        return redirect('kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategori $kategori)
    {
        $kategori->delete();
        Session::flash('flash_message', 'Kategori berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('kategori');
    }
    
}
