<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProdukTokoRequest;

use App\Http\Controllers\Controller;


use App\ProdukToko;
use App\Toko;
use App\KategoriProduk;
use App\SubKategoriProduk;
use Session;
use Storage;


class ProduktokowebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarproduktoko = ProdukToko::orderBy('id', 'asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahproduktoko = ProdukToko::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('produktoko.index', compact('daftarproduktoko','jumlahproduktoko'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produktoko.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdukTokoRequest $request)
    {
        $input = $request->all();
        
                if($request->hasFile('foto')){
                    $input['foto'] = $this->uploadFoto($request);
                }
                else{
                    $input['foto'] = "noimage.png";
                }
                //Simpan Data Usulan
                $produktoko = ProdukToko::create($input);
                Session::flash('flash_message', 'Produk Toko Berhasil Disimpan');
                return redirect('produktoko');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ProdukToko $produktoko)
    {
        return view('produktoko.show',compact('produktoko'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProdukToko $produktoko)
    {
        return view('produktoko.edit', compact('produktoko'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdukTokoRequest $request,ProdukToko $produktoko)
    {
        $input = $request->all();
        
                if($request->hasFile('foto')){
                    //Hapus foto lama
                    $this->hapusFoto($produktoko);
                    //Upload foto baru
                    $input['foto'] = $this->uploadFoto($request);
                }
        
                $produktoko->update($input);
        
                Session::flash('flash_message', 'Produk Toko berhasil diupdate');
                return redirect('produktoko');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProdukToko $produktoko)
    {
        $this->hapusFoto($produktoko);
        $produktoko->delete();
        Session::flash('flash_message', 'Produk Toko berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('produktoko');
    }

    //Upload foto ke direktori lokal
    public function uploadFoto(ProdukTokoRequest $request){
        $foto = $request->file('foto');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('foto')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('foto')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
    }

    //Hapus foto di direktori lokal
    public function hapusFoto(ProdukToko $produktoko){
        $exist = Storage::disk('foto')->exists($produktoko->foto);
        if(isset($produktoko->foto) && $exist){
           $delete = Storage::disk('foto')->delete($produktoko->foto);
           if($delete){
            return true;
           }
           return false;
        }
    }
}
