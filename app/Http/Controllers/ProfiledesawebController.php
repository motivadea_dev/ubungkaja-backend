<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProfiledesaRequest;
use App\ProfileDesa;
use Session;

class ProfiledesawebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarprofiledesa = Profiledesa::orderBy('nama', 'asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahprofiledesa = Profiledesa::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('profiledesa.index', compact('daftarprofiledesa','jumlahprofiledesa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profiledesa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfiledesaRequest $request, Profiledesa $profiledesa)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $profiledesa = Profiledesa::create($input);
        Session::flash('flash_message', 'Profile Desa Berhasil Disimpan');
        return redirect('profiledesa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profiledesa $profiledesa)
    {
         return view('profiledesa.show',compact('profiledesa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Profiledesa $profiledesa)
    {
        return view('profiledesa.edit', compact('profiledesa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfiledesaRequest $request, Profiledesa $profiledesa)
    {
         $input = $request->all();

        /*if($request->hasFile('foto')){
            //Hapus foto lama
            $this->hapusFoto($produk);
            //Upload foto baru
            $input['foto'] = $this->uploadFoto($request);
        }*/

        $profiledesa->update($input);

        Session::flash('flash_message', 'Profile Desa berhasil diupdate');
        return redirect('profiledesa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profiledesa $profiledesa)
    {
        $profiledesa->delete();
        Session::flash('flash_message', 'Profile Desa berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('profiledesa');
    }
    
}
