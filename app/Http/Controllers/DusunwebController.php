<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\DusunRequest;

use App\Warga;
use App\Dusun;
use App\Usulan;
use App\DataUser;
use Session;

class DusunwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftardusun = Dusun::orderBy('nama', 'asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahdusun = Dusun::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('dusun.index', compact('daftardusun','jumlahdusun'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dusun.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DusunRequest $request)
    {
        $input = $request->all();
        //Simpan Data Warga
        $dusun = Dusun::create($input);
        Session::flash('flash_message', 'Data Dusun Berhasil Disimpan');
        return redirect('dusun');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Dusun $dusun)
    {
        return view('dusun.show',compact('dusun'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Dusun $dusun)
    {
        return view('dusun.edit',compact('dusun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DusunRequest $request, Dusun $dusun)
    {
        $input = $request->all();

        /*if($request->hasFile('foto')){
            //Hapus foto lama
            $this->hapusFoto($produk);
            //Upload foto baru
            $input['foto'] = $this->uploadFoto($request);
        }*/

        $dusun->update($input);

        Session::flash('flash_message', 'Data Dusun berhasil diupdate');
        return redirect('dusun');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dusun $dusun)
    {
        $dusun->delete();
        Session::flash('flash_message', 'Dusun berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('dusun');
    }
}
