<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UsulanRequest;
use Storage;
use App\Warga;
use App\Kategori;
use App\Usulan;
use App\FotoUsulan;
use App\Dusun;
use App\ProfileDesa;
use App\Perangkat;
use Session;
use PDF;
use Mapper;

class UsulanwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarusulan = Usulan::orderBy('tanggal', 'desc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahusulan = Usulan::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('usulan.index', compact('daftarusulan','jumlahusulan'));
    }

    public function hasil()
    {
       //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftardusun = Dusun::orderBy('nama', 'asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahdusun = Dusun::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('usulan.hasil', compact('daftardusun','jumlahdusun'));
    }
    public function detailhasil(Dusun $dusun)
    {
       $map = Mapper::map(-8.6726769, 115.1542327, ['zoom' => 15, 'markers' => ['title' => 'Lokasi Usulan', 'animation' => 'DROP']]); 
       $iddusun = $dusun->id;
       //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
       //$daftarwarga = Warga::where('id_dusun',$iddusun)->orderBy('nama', 'asc')->paginate(20);
       $daftarusulan = Usulan::where('status',2)->orderBy('rangking', 'asc')->paginate(20);
       //2. Menghitung total keseluruhan jumlah warga
       $jumlahwarga = Warga::count();
       //3. MEMBUAT DATA API / JSON UNTUK 
       return view('usulan.detail', compact('dusun','jumlahwarga','daftarusulan'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usulan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsulanRequest $request)
    {
        $input = $request->all();

        /*if($request->hasFile('foto')){
            $input['foto'] = $this->uploadFoto($request);
        }
        else{
            $input['foto'] = "noimage.png";
        }*/
        //Simpan Data Usulan
        $usulan = Usulan::create($input);
        Session::flash('flash_message', 'Usulan Berhasil Disimpan');
        return redirect('usulan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Usulan $usulan)
    {   
        $idusulan = $usulan->id;
        settype($idusulan, "integer");
        $fotousulan = FotoUsulan::where('id_usulan',$idusulan)->get();
        $map = Mapper::map($usulan->latitude, $usulan->longitude, ['zoom' => 15, 'markers' => ['title' => 'Lokasi Usulan', 'animation' => 'DROP']]);
        return view('usulan.show',compact('usulan','map','fotousulan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Usulan $usulan)
    {
        return view('usulan.edit', compact('usulan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsulanRequest $request, Usulan $usulan)
    {
        $input = $request->all();
        $usulan->update($input);
       
        /*if($request->hasFile('foto')){
            //Hapus foto lama
            $this->hapusFoto($usulan);
            //Upload foto baru
            $input['foto'] = $this->uploadFoto($request);
        }*/
    
    	//Seleksi Data Warga
        $idwarga = $request->input('id_warga');
	settype($idwarga, "integer");
	$daftarperangkat = Perangkat::where('id_warga',$idwarga)->get();
	
	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        
        //Jika Usulan diterima
        if($request->input('status') == 2){
            $content = array("en" => 'Selamat,usulan Anda diterima');
        }
        else if($request->input('status') == 3){
            $content = array("en" => 'Maaf,usulan Anda tidak diterima');
        }
        
	    $fields = array(
			'app_id' => "0cd199c8-a64b-4b4d-be1f-913897925183",
			'include_player_ids' => $device_id,
      		'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic ODM1NmExN2QtNjIwYy00OWVmLTljYzctOTM2MjZiNDgzNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
        
        Session::flash('flash_message', 'Usulan berhasil diupdate');
        return redirect('usulan');
       	
    }
    public function verifikasi(Request $request, Usulan $usulan)
    {
        $input = $request->all();
        $usulan->update($input);
       
    	//Seleksi Data Warga
        $idwarga = $request->input('id_warga');
        settype($idwarga, "integer");
        $daftarperangkat = Perangkat::where('id_warga',$idwarga)->get();
	
	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        
        //Jika Usulan diterima
        if($request->input('status') == 2){
            $content = array("en" => 'Selamat,usulan Anda diterima');
        }
        else if($request->input('status') == 2){
            $content = array("en" => 'Maaf,usulan Anda tidak diterima');
        }
        
        $fields = array(
        'app_id' => "0cd199c8-a64b-4b4d-be1f-913897925183",
        'include_player_ids' => $device_id,
          'data' => array("foo" => "bar"),
        'contents' => $content
        );

		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic ODM1NmExN2QtNjIwYy00OWVmLTljYzctOTM2MjZiNDgzNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
        
        Session::flash('flash_message', 'Verifikasi Sukses');
        return redirect('usulan');
       	
    }
    public function rangkingnaik(Usulan $usulan)
    {   
        $idusulan = $usulan->id;
        settype($idusulan, "integer");
        //SELEKSI DUSUN
        $daftardusun = Usulan::with('warga')->where('id',$idusulan)->get();
        foreach($daftardusun as $dusun){
            $iddusun = $dusun->warga->id_dusun;
        }
        settype($iddusun, "integer");
        //SELEKSI USULAN PER DUSUNG
        $daftarwarga = Warga::where('id_dusun',$iddusun)->get();
        $daftarusulan = Usulan::where('status',2)->orderBy('rangking', 'asc')->get();
        
        foreach($daftarwarga as $warga){
            //VARIABLE AWAL
            $previous_value = 0;
            $i = 0;
            $len = count($daftarusulan);
            $prevRank = 0;
            $nowRank = 0;
            $nextRank = 0;
            $prevID = 0;
            $nowID = 0;
            $nextID = 0;
            $rangking = array();
            foreach($daftarusulan as $key => $usulan){
                if($usulan->warga->id_dusun == $warga->id_dusun){
                    //SET NEXT VALUE
                    if ($i == $len - 1) {
                        $next_value = 0;
                    }
                    else{
                        $next_value = $daftarusulan[$key+1];
                    }
                    $i++;

                    //TAMPIL VALUE
                    //1.Previous
                    if($previous_value != '0'){
                        $prevID = $previous_value->id;
                        $prevRank = $previous_value->rangking;
                    }
                    else{
                        $prevID = 0;
                        $prevRank = 0;
                    }
                    //2.Now
                    $nowID = $usulan->id;
                    $nowRank = $usulan->rangking;
                    //3.Next
                    if($next_value != '0'){
                        $nextID = $next_value->id;
                        $nextRank = $next_value->rangking;
                    }
                    else{
                        $nextID = 0;
                        $nextRank = 0;
                    }
                    //SET PREV VALUE
                    $previous_value = $usulan;
                }
                //EDIT VALUE RANGKING
                if($usulan->id == $idusulan){
                    //$rangking[] = array('prevID' => $prevID, 'prevRank' => $prevRank, 'nowID' => $nowID, 'nowRank' => $nowRank, 'nextID' => $nextID, 'nextRank' => $nextRank);
                    $usulan->rangking = $prevRank;
                    $usulan->update();
                    
                    $usulan = New Usulan;
                    $usulan = Usulan::findOrFail($prevID);
                    $usulan->rangking = $nowRank;
                    $usulan->update();
                    
                    return redirect('hasil/' . $iddusun);
                }
            }
        }
    }
    
    public function rangkingturun(Usulan $usulan)
    {   
        $idusulan = $usulan->id;
        settype($idusulan, "integer");
        //SELEKSI DUSUN
        $daftardusun = Usulan::with('warga')->where('id',$idusulan)->get();
        foreach($daftardusun as $dusun){
            $iddusun = $dusun->warga->id_dusun;
        }
        settype($iddusun, "integer");
        //SELEKSI USULAN PER DUSUNG
        $daftarwarga = Warga::where('id_dusun',$iddusun)->get();
        $daftarusulan = Usulan::where('status',2)->orderBy('rangking', 'asc')->get();
        
        foreach($daftarwarga as $warga){
            //VARIABLE AWAL
            $previous_value = 0;
            $i = 0;
            $len = count($daftarusulan);
            $prevRank = 0;
            $nowRank = 0;
            $nextRank = 0;
            $prevID = 0;
            $nowID = 0;
            $nextID = 0;
            $rangking = array();
            foreach($daftarusulan as $key => $usulan){
                if($usulan->warga->id_dusun == $warga->id_dusun){
                    //SET NEXT VALUE
                    if ($i == $len - 1) {
                        $next_value = 0;
                    }
                    else{
                        $next_value = $daftarusulan[$key+1];
                    }
                    $i++;

                    //TAMPIL VALUE
                    //1.Previous
                    if($previous_value != '0'){
                        $prevID = $previous_value->id;
                        $prevRank = $previous_value->rangking;
                    }
                    else{
                        $prevID = 0;
                        $prevRank = 0;
                    }
                    //2.Now
                    $nowID = $usulan->id;
                    $nowRank = $usulan->rangking;
                    //3.Next
                    if($next_value != '0'){
                        $nextID = $next_value->id;
                        $nextRank = $next_value->rangking;
                    }
                    else{
                        $nextID = 0;
                        $nextRank = 0;
                    }
                    //SET PREV VALUE
                    $previous_value = $usulan;
                }
                //EDIT VALUE RANGKING
                if($usulan->id == $idusulan){
                    //$rangking[] = array('prevID' => $prevID, 'prevRank' => $prevRank, 'nowID' => $nowID, 'nowRank' => $nowRank, 'nextID' => $nextID, 'nextRank' => $nextRank);
                    $usulan->rangking = $nextRank;
                    $usulan->update();
                    
                    $usulan = New Usulan;
                    $usulan = Usulan::findOrFail($nextID);
                    $usulan->rangking = $nowRank;
                    $usulan->update();
                    
                    return redirect('hasil/' . $iddusun);
                }
            }
        }  
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usulan $usulan)
    {
        //$this->hapusFoto($usulan);
        $usulan->delete();
        Session::flash('flash_message', 'Usulan berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('usulan');
    }
    //Upload foto ke direktori lokal
    public function uploadFoto(UsulanRequest $request){
        $foto = $request->file('foto');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('foto')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('foto')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
    }

    //Hapus foto di direktori lokal
    public function hapusFoto(Usulan $usulan){
        $exist = Storage::disk('foto')->exists($usulan->foto);
        if(isset($usulan->foto) && $exist){
           $delete = Storage::disk('foto')->delete($usulan->foto);
           if($delete){
            return true;
           }
           return false;
        }
    }

    //pencarian
    public function cari(Request $request){
        //$kata_kunci = $request->input('kata_kunci');    //Ambil value dari inputan pencarian
        $id_kategori = $request->input('id_kategori'); 
        /*if(!empty($kata_kunci)){                        //Jika kata kunci tidak kosong, maka... 
            //Query
            $query = Usulan::where('tanggal', 'LIKE', '%' . $kata_kunci . '%');
            $daftarusulan = $query->paginate(10);
            //Url Pagination
            $pagination = $daftarusulan->appends($request->except('page'));
            $jumlahusulan = $daftarusulan->total();
            return view('usulan.index', compact('daftarusulan','kata_kunci','pagination','jumlahusulan'));
        }
        else*/ 
        if(!empty($id_kategori)){                        //Jika kata kunci tidak kosong, maka... 
            //Query
            $query = Usulan::where('id_kategori', 'LIKE', '%' . $id_kategori . '%');
            $daftarusulan = $query->paginate(10);
            //Url Pagination
            $pagination = $daftarusulan->appends($request->except('page'));
            $jumlahusulan = $daftarusulan->total();
            return view('usulan.index', compact('daftarusulan','id_kategori','pagination','jumlahusulan'));
        }
        return redirect('usulan');
    }


    //cetak pdf
    public function getPdf(Usulan $usulan) 
    {
        $pdf = PDF::loadView('usulan.print',compact('usulan'))->setPaper('a4','portrait');
        //$pdf = PDF::loadView('customer.print',compact('customer','profiletoko'))->setPaper(array(0,0,332.59,650),'portrait');
        return $pdf->stream();
    }
    public function getPdfSemua() 
    {
        $seleksidaftarusulan = Usulan::all();
        //$seleksidaftarusulan = $daftarusulan->where('status', '2');
        $pdf = PDF::loadView('usulan.printsemua',compact('seleksidaftarusulan'))->setPaper('a4','portrait');
        //$pdf = PDF::loadView('customer.print',compact('customer','profiletoko'))->setPaper(array(0,0,332.59,650),'portrait');
        return $pdf->stream();
    }
    public function getPdfHasil(Dusun $dusun) 
    {
        //$desa = ProfileDesa::where('id',1)->get();
        $desa = ProfileDesa::findOrFail($dusun->id_profiledesa);
        //return $desa;
        $daftarusulan = Usulan::where('status',2)->orderBy('rangking', 'asc')->get();
        $pdf = PDF::loadView('usulan.printhasil',compact('desa','dusun','daftarusulan'))->setPaper('a4','portrait');
        return $pdf->stream();
    }
}
