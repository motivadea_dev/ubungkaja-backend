<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warga;
use App\KategoriPengaduan;
use App\Pengaduan;
use App\FotoTemp;
use App\FotoPengaduan;
use app\KomentarPengaduan;
use app\TindakanPengadun;
use Storage;

class PengaduanController extends Controller
{
    public function indexapp(){
        //1. Menampilkan semua data pengaduan dan dibagi per halaman 20 baris
        $daftarpengaduan = Pengaduan::with('kategoripengaduan','fotopengaduan','komentarpengaduan','tindakanpengaduan')->get();;
        //2. Menghitung total keseluruhan jumlah pengaduan
        $jumlahpengaduan = Pengaduan::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftarpengaduan);
        $koleksi->toJson();

        //1. Menampilkan semua data pengaduan dan dibagi per halaman 20 baris
        $daftarkategori = KategoriPengaduan::all();
        //2. Menghitung total keseluruhan jumlah pengaduan
        $jumlahkategori = KategoriPengaduan::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi2 = collect($daftarkategori);
        $koleksi2->toJson();

        return compact('koleksi','koleksi2');
    }
    
    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data pengaduan 
        $pengaduan = Pengaduan::create($input);
        //Ambil ID Transaksi
        $id_awal = Pengaduan::orderBy('id', 'desc')->first();
        $idpengaduan = $id_awal->id;
        settype($idpengaduan, "integer");
        //Ambil ID Warga
        $idwarga = $request->input('id_warga');
        settype($idwarga, "integer");
        //Ambil Data Foto Sementara
        $daftar = FotoTemp::all();
        $daftarfoto = $daftar->where('id_warga',$idwarga);
        //Simpan Foto pengaduan
        foreach($daftarfoto as $foto){
            $fotopengaduan = New FotoPengaduan;
            $fotopengaduan->id_pengaduan = $idpengaduan;
            $fotopengaduan->foto = $foto->foto;
            $fotopengaduan->save();
            //Hapus Foto Sementara
            $foto->delete();
        }

        return $pengaduan;
    }
    //Upload foto ke direktori lokal
    public function uploadFoto(Request $request){
        $foto = $request->file('file');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('file')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('file')->move($upload_path, $foto_name); //Copy paste ke server
            //Simpan Link Foto
            $fototemp = New FotoTemp;
            $fototemp->id_warga = $request->id_warga;
            $fototemp->foto = $foto_name;
            $fototemp->save();
            
            return $foto_name;
        }
        return false;
    }

    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id pengaduan
        $pengaduan = Pengaduan::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data pengaduan
        $pengaduan->update($input);
        return $pengaduan;
    }
    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id pengaduan
        $pengaduan = Pengaduan::findOrFail($item);
        //2. Hapus data
        $pengaduan->delete();
        return $pengaduan;
    }
}
