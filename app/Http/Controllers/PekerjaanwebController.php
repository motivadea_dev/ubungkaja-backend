<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PekerjaanRequest;
use App\Http\Controllers\Controller;

use App\Pekerjaan;
use Session;

class PekerjaanwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarpekerjaan = Pekerjaan::orderBy('id','asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahpekerjaan = Pekerjaan::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('pekerjaan.index', compact('daftarpekerjaan','jumlahpekerjaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pekerjaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PekerjaanRequest $request, Pekerjaan $pekerjaan)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $pekerjaan = Pekerjaan::create($input);
        Session::flash('flash_message', 'Pekerjaan Berhasil Disimpan');
        return redirect('pekerjaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pekerjaan $pekerjaan)
    {
        return view('pekerjaan.edit', compact('pekerjaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PekerjaanRequest $request,Pekerjaan $pekerjaan)
    {
        $input = $request->all();
        
                $pekerjaan->update($input);
        
                Session::flash('flash_message', 'Pekerjaan berhasil diupdate');
                return redirect('pekerjaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pekerjaan $pekerjaan)
    {
        $pekerjaan->delete();
        Session::flash('flash_message', 'Pekerjaan berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('pekerjaan');
    }
}
