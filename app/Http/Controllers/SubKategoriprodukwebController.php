<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubKategoriprodukRequest;

use App\KategoriProduk;
use App\SubKategoriProduk;
use Session;


class SubKategoriprodukwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarsubkategoriproduk = SubKategoriProduk::orderBy('id', 'asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahsubkategoriproduk = SubKategoriProduk::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('subkategoriproduk.index', compact('daftarsubkategoriproduk','jumlahsubkategoriproduk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subkategoriproduk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubKategoriprodukRequest $request)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $subkategoriproduk = SubKategoriproduk::create($input);
        Session::flash('flash_message', 'Sub Kategori Berhasil Disimpan');
        return redirect('subkategoriproduk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SubKategoriproduk $subkategoriproduk)
    {
        return view('subkategoriproduk.edit', compact('subkategoriproduk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubKategoriprodukRequest $request,SubKategoriproduk $subkategoriproduk)
    {
        $input = $request->all();
        $subkategoriproduk->update($input);
        
        Session::flash('flash_message', 'Sub Kategori berhasil diupdate');
        return redirect('subkategoriproduk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubKategoriproduk $subkategoriproduk)
    {
        $subkategoriproduk->delete();
        Session::flash('flash_message', 'Sub Kategori berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('subkategoriproduk');
    }
}
