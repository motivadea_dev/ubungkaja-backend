<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\TransaksiPenjualanBumdesRequest;
use App\Http\Controllers\Controller;

use App\TransaksiPembelian;
use App\Toko;
use Storage;
use Session;



class TransaksiPenjualanBumdeswebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
       $daftartransaksipenjualan = TransaksiPembelian::orderBy('tanggal', 'desc')->paginate(20);;
       //2. Menghitung total keseluruhan jumlah warga
       $jumlahtransaksipenjualan = TransaksiPembelian::count();
       //3. MEMBUAT DATA API / JSON UNTUK 
       return view('transaksipenjualanbumdes.index', compact('daftartransaksipenjualan','jumlahtransaksipenjualan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaksipenjualanbumdes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $transaksipenjualanbumdes = TransaksiPembelian::create($input);
        Session::flash('flash_message', 'Transaksi Penjualan Berhasil Disimpan');
        return redirect('transaksipenjualanbumdes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(TransaksiPembelian $transaksipenjualanbumdes)
    {
        return view('transaksipenjualanbumdes.show',compact('transaksipenjualanbumdes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TransaksiPembelian $transaksipenjualanbumdes)
    {
        return view('transaksipenjualanbumdes.edit', compact('transaksipenjualanbumdes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransaksiPenjualanBumdesRequest $request,TransaksiPembelian $transaksipenjualanbumdes)
    {
        $input = $request->all();
        
                $transaksipenjualanbumdes->update($input);
                
                        Session::flash('flash_message', 'Transaksi berhasil diupdate');
                        return redirect('transaksipenjualanbumdes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransaksiPembelian $transaksipenjualanbumdes)
    {
        $transaksipenjualanbumdes->delete();
        Session::flash('flash_message', 'Transaksi berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('transaksipenjualanbumdes');
    }
}
