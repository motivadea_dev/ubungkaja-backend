<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BeritaRequest;
use App\Berita;
use Session;

class BeritawebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarberita = Berita::orderBy('id', 'asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahberita = Berita::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('berita.index', compact('daftarberita','jumlahberita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('berita.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BeritaRequest $request)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $berita = Berita::create($input);
        Session::flash('flash_message', 'Berita Berhasil Disimpan');
        return redirect('berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Berita $berita)
    {
         return view('berita.show',compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Berita $berita)
    {
        return view('berita.edit', compact('berita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BeritaRequest $request, Berita $berita)
    {
         $input = $request->all();

        /*if($request->hasFile('foto')){
            //Hapus foto lama
            $this->hapusFoto($produk);
            //Upload foto baru
            $input['foto'] = $this->uploadFoto($request);
        }*/

        $berita->update($input);

        Session::flash('flash_message', 'Berita berhasil diupdate');
        return redirect('berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Berita $berita)
    {
        $berita->delete();
        Session::flash('flash_message', 'Berita berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('berita');
    }
    
}
