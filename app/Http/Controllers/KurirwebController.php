<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\KurirRequest;
use App\Http\Controllers\Controller;

use App\Warga;
use App\Kurir;
use Storage;
use Session;
class KurirwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
       $daftarkurir = Kurir::orderBy('id', 'desc')->paginate(20);;
       //2. Menghitung total keseluruhan jumlah warga
       $jumlahkurir = Kurir::count();
       //3. MEMBUAT DATA API / JSON UNTUK 
       return view('kurir.index', compact('daftarkurir','jumlahkurir'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kurir.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $kurir = Kurir::create($input);
        Session::flash('flash_message', 'Kurir Berhasil Disimpan');
        return redirect('kurir');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kurir $kurir)
    {
        return view('kurir.edit', compact('kurir'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KurirRequest $request,Kurir $kurir)
    {
        $input = $request->all();
        
            $kurir->update($input);
                
            Session::flash('flash_message', 'Kurir berhasil diupdate');
            return redirect('kurir');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kurir $kurir)
    {
        $kurir->delete();
        Session::flash('flash_message', 'Transaksi berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('kurir');
    }
}
