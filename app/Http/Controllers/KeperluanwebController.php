<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\KeperluanRequest;
use App\Http\Controllers\Controller;
use App\ProfileDesa;
use App\Keperluan;
use Session;
use Storage;

class KeperluanwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarkeperluan = Keperluan::orderBy('id', 'asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahkeperluan = Keperluan::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('keperluan.index', compact('daftarkeperluan','jumlahkeperluan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('keperluan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KeperluanRequest $request, Keperluan $keperluan)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $keperluan = Keperluan::create($input);
        Session::flash('flash_message', 'Kategori Layanan Berhasil Disimpan');
        return redirect('keperluan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( Keperluan $keperluan)
    {
        return view('keperluan.edit', compact('keperluan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KeperluanRequest $request, Keperluan $keperluan)
    {
        $input = $request->all();
        $keperluan->update($input);

        Session::flash('flash_message', 'Kategori Layanan berhasil diupdate');
        return redirect('keperluan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keperluan $keperluan)
    {
        $keperluan->delete();
        Session::flash('flash_message', 'Kategori Layanan berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('keperluan');
    }
}
