<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warga;
use App\Kategori;
use App\Usulan;
use App\Dusun;

class DusunController extends Controller
{
    public function indexapp(){
        //1. Menampilkan semua data usulan dan dibagi per halaman 20 baris
        $daftardusun = Dusun::all();
        //2. Menghitung total keseluruhan jumlah dusun
        $jumlahdusun = Dusun::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftardusun);
        $koleksi->toJson();
        return $koleksi;
    }
    
    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data usulan 
        $usulan = Dusun::create($input);
        return $dusun;
    }
    
    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id usulan
        $usulan = Dusun::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data usulan
        $dusun->update($input);
        return $dusun;
    }
    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id usulan
        $dusun = Dusun::findOrFail($item);
        //2. Hapus data
        $dusun->delete();
        return $dusun;
    }
}
