<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warga;
use App\Toko;
use App\ProdukToko;
use App\ProdukBumdes;
use App\FotoTempProduk;
use App\FotoProduk;

class ProdukTokoController extends Controller
{

    public function indexapp($item)
    {
        settype($item, "integer");
        $daftar = ProdukToko::where('id_toko',$item)->get();
        $koleksi = collect($daftar);
        $koleksi->toJson();
        return $koleksi;
    }
    public function cariapp($item)
    {
        $daftar = ProdukToko::where('namaproduk', 'LIKE', '%' . $item . '%')->get();
        $koleksi = collect($daftar);
        $koleksi->toJson();
        return $koleksi;
    }
    public function cariappBumdes($item)
    {
        $daftar = ProdukBumdes::where('namaproduk', 'LIKE', '%' . $item . '%')->get();
        $koleksi = collect($daftar);
        $koleksi->toJson();
        return $koleksi;
    }
    //Upload foto ke direktori lokal
    public function uploadFoto(Request $request){
        $foto = $request->file('file');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('file')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('file')->move($upload_path, $foto_name); //Copy paste ke server
            //Simpan Link Foto
            $fototemp = New FotoTempProduk;
            $fototemp->id_toko = $request->id_toko;
            $fototemp->foto = $foto_name;
            $fototemp->save();
            
            return $foto_name;
        }
        return false;
    }
    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data usulan 
        $produktoko = ProdukToko::create($input);
        //Ambil ID Produk
        $id_awal = ProdukToko::orderBy('id', 'desc')->first();
        $idproduk = $id_awal->id;
        settype($idproduk, "integer");
        //Ambil ID Toko
        $idtoko = $request->input('id_toko');
        settype($idtoko, "integer");
        //Ambil Data Foto Sementara
        $daftar = FotoTempProduk::all();
        $daftarfoto = $daftar->where('id_toko',$idtoko);
        //Simpan Foto Usulan
        foreach($daftarfoto as $foto){
            $fotoproduk = New FotoProduk;
            $fotoproduk->id_produktoko = $idproduk;
            $fotoproduk->foto = $foto->foto;
            $fotoproduk->save();
            //Hapus Foto Sementara
            $foto->delete();
        }
        
        return $produktoko;
    }

    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id produk
        $produktoko = ProdukToko::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data produktoko
        $produktoko->update($input);
        return $produktoko;
    }

    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id produk
        $produktoko = ProdukToko::findOrFail($item);
        //2. Hapus data
        $produktoko->delete();
        return $produktoko;
    }
}
