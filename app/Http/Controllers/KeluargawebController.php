<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AnggotaRequest;
use App\Http\Requests\KeluargaRequest;
use App\Http\Controllers\Controller;
use Storage;
use App\Keluarga;
use App\Warga;
use App\AnggotaKeluarga;
use Session;

class KeluargawebController extends Controller
{
    public function index()
    {
           //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
           $daftarkeluarga = Keluarga::orderBy('tanggal', 'desc')->paginate(20);;
           //2. Menghitung total keseluruhan jumlah warga
           $jumlahkeluarga = Keluarga::count();
           //3. MEMBUAT DATA API / JSON UNTUK 
           return view('keluarga.index', compact('daftarkeluarga','jumlahkeluarga'));
    }

    public function indexanggota(Keluarga $keluarga)
    {
           $data = AnggotaKeluarga::all();
           $daftaranggota = $data->where('id_keluarga',$keluarga->id);
           $jumlahanggota = $daftaranggota->count();
           return view('keluarga.anggota', compact('daftaranggota','jumlahanggota','keluarga'));
    }

    public function create()
    {
        return view('keluarga.create');
    }
    public function createanggota(Keluarga $keluarga)
    {
        return view('keluarga.createanggota', compact('keluarga'));
    }
    
    public function store(KeluargaRequest $request)
    {
        $input = $request->all();
        $keluarga = Keluarga::create($input);
        Session::flash('flash_message', 'Keluarga Berhasil Disimpan');
        return redirect('keluarga');
    }
    public function storeanggota(AnggotaRequest $request)
    {
        $input = $request->all();
        $idkeluarga = $request->input('id_keluarga');
        $anggota = AnggotaKeluarga::create($input);
        Session::flash('flash_message', 'Anggota Keluarga Berhasil Disimpan');
        return redirect('keluarga/anggota/'.$idkeluarga);
    }
    public function edit(Keluarga $keluarga)
    {
        return view('keluarga.edit', compact('keluarga'));
    }

    public function update(KeluargaRequest $request,Keluarga $keluarga)
    {
        $input = $request->all();
        $keluarga->update($input);
        
        Session::flash('flash_message', 'Keluarga berhasil diupdate');
        return redirect('keluarga');
    }

    public function destroy(Keluarga $keluarga)
    {
        $keluarga->delete();
        Session::flash('flash_message', 'Keluarga berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('keluarga');
    }

    public function destroyanggota(AnggotaKeluarga $anggota)
    {
        $anggota->delete();
        Session::flash('flash_message', 'Anggota Keluarga berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('keluarga/anggota/'.$anggota->id_keluarga);
    }

}
