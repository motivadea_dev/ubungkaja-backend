<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AntrianRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Warga;
use App\Keperluan;
use App\Antrian;
use Session;
use PDF;
use Storage;

class AntrianwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarantrian = Antrian::orderBy('id', 'desc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahantrian = Antrian::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('antrian.index', compact('daftarantrian','jumlahantrian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('antrian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AntrianRequest $request)
    {
        $input = $request->all();
        
                /*if($request->hasFile('foto')){
                    $input['foto'] = $this->uploadFoto($request);
                }
                else{
                    $input['foto'] = "noimage.png";
                }*/
                //Simpan Data Usulan
                $antrian = Antrian::create($input);
                Session::flash('flash_message', 'Antian Berhasil Disimpan');
                return redirect('antrian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Antrian $antrian)
    {
        return view('antrian.show',compact('antrian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Antrian $antrian)
    {
        return view('antrian.edit', compact('antrian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AntrianRequest $request, Antrian $antrian)
    {
        $input = $request->all();
        
                /*if($request->hasFile('foto')){
                    //Hapus foto lama
                    $this->hapusFoto($usulan);
                    //Upload foto baru
                    $input['foto'] = $this->uploadFoto($request);
                }*/
        
                $antrian->update($input);
        
                Session::flash('flash_message', 'Antrian berhasil diupdate');
                return redirect('antrian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Antrian $antrian)
    {
        $antrian->delete();
        Session::flash('flash_message', 'Antrian berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('antrian');
    }
}
