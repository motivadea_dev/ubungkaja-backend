<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\TransaksiPenjualanRequest;
use App\Http\Controllers\Controller;

use App\TransaksiPenjualan;
use App\Toko;
use Storage;
use Session;

class TransaksiPenjualanwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
       $daftartransaksipenjualan = TransaksiPenjualan::orderBy('tanggal', 'desc')->paginate(20);;
       //2. Menghitung total keseluruhan jumlah warga
       $jumlahtransaksipenjualan = TransaksiPenjualan::count();
       //3. MEMBUAT DATA API / JSON UNTUK 
       return view('transaksipenjualan.index', compact('daftartransaksipenjualan','jumlahtransaksipenjualan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaksipenjualan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $transaksipenjualan = TransaksiPenjualan::create($input);
        Session::flash('flash_message', 'Transaksi Penjualan Berhasil Disimpan');
        return redirect('transaksipenjualan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(TransaksiPenjualan $transaksipenjualan)
    {
        return view('transaksipenjualan.show',compact('transaksipenjualan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TransaksiPenjualan $transaksipenjualan)
    {
        return view('transaksipenjualan.edit', compact('transaksipenjualan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransaksiPenjualanRequest $request,TransaksiPenjualan $transaksipenjualan)
    {
        $input = $request->all();

        $transaksipenjualan->update($input);
        
                Session::flash('flash_message', 'Transaksi berhasil diupdate');
                return redirect('transaksipenjualan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransaksiPenjualan $transaksipenjualan)
    {
        $transaksipenjualan->delete();
        Session::flash('flash_message', 'Transaksi berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('transaksipenjualan');
    }
}
