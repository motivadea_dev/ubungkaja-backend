<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\WargaRequest;

use App\Warga;
use App\Dusun;
use App\Usulan;
use App\DataUser;
use App\Toko;
use App\Pendidikan;
use App\Pekerjaan;
use App\Penghasilan;
use Session;
use Storage;
use PDF;
use Excel;
use DB;
class WargawebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarwarga = Warga::orderBy('id_dusun', 'asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahwarga = Warga::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('warga.index', compact('daftarwarga','jumlahwarga'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('warga.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WargaRequest $request)
    {
        $input = $request->all();

        if($request->hasFile('foto')){
            $input['foto'] = $this->uploadFoto($request);
        }
        else{
            $input['foto'] = "noimage.png";
        }
        //Simpan Data Warga
        $warga = Warga::create($input);
        Session::flash('flash_message', 'Data Warga Berhasil Disimpan');
        return redirect('warga');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Warga $warga)
    {
        return view('warga.show',compact('warga'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Warga $warga)
    {
        return view('warga.edit', compact('warga'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WargaRequest $request,Warga $warga)
    {
        $input = $request->all();

        if($request->hasFile('foto')){
            //Hapus foto lama
            $this->hapusFoto($warga);
            //Upload foto baru
            $input['foto'] = $this->uploadFoto($request);
        }

        $warga->update($input);

        Session::flash('flash_message', 'Data Warga berhasil diupdate');
        return redirect('warga');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warga $warga)
    {
        $warga->delete();
        $this->hapusFoto($warga);
        Session::flash('flash_message', 'Data Warga berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('warga');
    }

     //Upload foto ke direktori lokal
    public function uploadFoto(WargaRequest $request){
        $foto = $request->file('foto');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('foto')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('foto')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
    }

    //Hapus foto di direktori lokal
    public function hapusFoto(Warga $warga){
        $exist = Storage::disk('foto')->exists($warga->foto);
        if(isset($warga->foto) && $exist){
           $delete = Storage::disk('foto')->delete($warga->foto);
           if($delete){
            return true;
           }
           return false;
        }
    }

    //pencarian
    public function cari(Request $request){
        $kata_kunci = $request->input('kata_kunci');    //Ambil value dari inputan pencarian
        if(!empty($kata_kunci)){                        //Jika kata kunci tidak kosong, maka... 
            //Query
            $query = Warga::where('noktp', 'LIKE', '%' . $kata_kunci . '%');

            $daftarwarga = $query->paginate(10);

            //Url Pagination
            $pagination = $daftarwarga->appends($request->except('page'));
            $jumlahwarga = $daftarwarga->total();
            return view('warga.index', compact('daftarwarga','kata_kunci','pagination','jumlahwarga'));
        }
        return redirect('warga');
    }

    //cetak pdf
    public function getPdf(Warga $warga)
    {
        $pdf = PDF::loadView('warga.print',compact('warga'))->setPaper('a4','portrait');
        //$pdf = PDF::loadView('customer.print',compact('customer','profiletoko'))->setPaper(array(0,0,332.59,650),'portrait');
        return $pdf->stream();
    }
        //Import Excel
    public function importExcel(Request $request)
    {
        if($request->hasFile('import_file')){
            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    if($value->jenis_kelamin == 'Lk'){
                        $jkel = "L";
                    }
                    else if($value->jenis_kelamin == 'Pr'){
                        $jkel = "P";
                    }
                    $insert[] = ['id'=>'' ,'noktp' => $value->noktp ,'nama' => $value->nama ,'alamat' => $value->alamat ,'tanggal_lahir' => $value->tanggal_lahir ,'jenis_kelamin' => $jkel ,'id_dusun' => $value->id_dusun ,'id_profiledesa' => '1' ,'namapendidikan' => $value->namapendidikan ,'namapekerjaan' => $value->namapekerjaan];
                }
                if(!empty($insert)){
                    DB::table('warga')->insert($insert);
                    Session::flash('flash_message', 'Import Data Warga Berhasil');
                }
            }
            else{
                Session::flash('flash_message', 'Data Kosong');
            }
        }
        else{
            Session::flash('flash_message', 'Silahkan Pilih File Excel (.xlsx / .xls / .csv) terlebih dahulu');
        }
        return back();
    }
}
