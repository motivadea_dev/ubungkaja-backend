<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warga;
use App\KategoriPost;
use App\Posting;

class PostingController extends Controller
{
    public function indexapp($item){
        //1. Menampilkan semua data posting dan dibagi per halaman 20 baris
        $daftarposting = Posting::with('kategoripost')->where('id_warga',$item)->get();
        //2. Menghitung total keseluruhan jumlah posting
        $jumlahposting = Posting::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftarposting);
        $koleksi->toJson();

        //1. Menampilkan semua data posting dan dibagi per halaman 20 baris
        $daftarkategori = KategoriPost::all();
        //2. Menghitung total keseluruhan jumlah posting
        $jumlahkategori = KategoriPost::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi2 = collect($daftarkategori);
        $koleksi2->toJson();

        return compact('koleksi','koleksi2');
    }
    public function postingbaru(){
        settype($item, "integer");
        $daftarposting = Posting::orderBy('id','desc')->first();
        $koleksi = collect($daftarposting);
        $koleksi->toJson();
        
        return $koleksi;

    }

    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data posting 
        $posting = Posting::create($input);
        return $posting;
    }

    //Upload foto ke direktori lokal
    public function uploadFoto(Request $request){
	    $foto = $request->file('file');             //Mengambil Inputan file foto
	    $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)
	
	   if($request->file('file')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
	        $foto_name = $request->fotoposting;
	        $upload_path = 'uploadposting';            //Folder foto sing diupload ke server
	        $request->file('file')->move($upload_path, $foto_name); //Copy paste ke server
	        return $foto_name;
	   }
	   return false; 
    }

    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id posting
        $posting = Posting::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data posting
        $posting->update($input);
        return $posting;
    }

    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id posting
        $posting = Posting::findOrFail($item);
        //2. Hapus data
        $posting->delete();
        return $posting;
    }
}
