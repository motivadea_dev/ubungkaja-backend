<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\TransaksiPenjualan;
use App\DetailPenjualan;
use App\Keranjang;
use App\ProdukToko;
use App\Perangkat;
use App\PerangkatToko;

class TransaksiPenjualanController extends Controller
{
    //KONSUMEN
    public function indexapp($item)
    {
        settype($item, "integer");
        $daftartransaksi = TransaksiPenjualan::where('id_warga',$item)->orderBy('id', 'desc')->get();
        $koleksi = collect($daftartransaksi);
        $koleksi->toJson();
        return $koleksi;
    }

    public function storeapp(Request $request)  
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data Transaksi 
        $transaksi = TransaksiPenjualan::create($input);
        //Ambil ID Warga
        $idwarga = $request->input('id_warga');
        settype($idwarga, "integer");
        //Ambil ID Transaksi
        $id_awal = TransaksiPenjualan::orderBy('id', 'desc')->first();
        $idtransaksi = $id_awal->id;
        settype($idtransaksi, "integer");
        //Tampilkan Keranjang
        $daftar = Keranjang::all();
        $daftarkeranjang = $daftar->where('id_warga',$idwarga);
       
        //Simpan DetailPenjualan
        foreach($daftarkeranjang as $keranjang){
            $detailpenjualan = New DetailPenjualan;
            $detailpenjualan->id_transaksipenjualan = $idtransaksi;
            $detailpenjualan->id_produktoko = $keranjang->id_produktoko;
            $detailpenjualan->jumlah = $keranjang->jumlah;
            $detailpenjualan->save();
            //Update Stok
            $produk = ProdukToko::findOrFail($keranjang->id_produktoko);
            $stokawal = $produk->stok;
            $stokakhir = $stokawal - $keranjang->jumlah;
            $produk->stok = $stokakhir;
            $produk->update();
            //Hapus Keranjang
            $keranjang->delete();
        }
        
        //Seleksi Data Toko
        $idtoko = $request->input('id_toko');
    	settype($idtoko, "integer");
    	$daftarperangkat = PerangkatToko::where('id_toko',$idtoko)->get();
    	
    	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        //Jika Usulan diterima
        $content = array("en" => 'Ada order penjualan baru,silahkan diproses.');
        
	    $fields = array(
			'app_id' => "b842e2bc-4de5-4cd7-b808-142b2917496f",
			'include_player_ids' => $device_id,
			//'include_player_ids' => array("1d4ee16b-3a59-4038-a912-dc868942e1c1"),
      		'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic NDgzY2UxOTItNDRmZi00OTFkLWE1MjAtODFhNWVjMmQxOTU4'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
        
        return $transaksi;
    }
    public function detail($item){
        settype($item, "integer"); 
        //Detail Penjualan
        $daftar = DetailPenjualan::with('produktoko')->where('id_transaksipenjualan',$item)->get();
        //$daftarpenjualan = $daftar->where('id_transaksipenjualan',$item);
        $daftar2 = TransaksiPenjualan::where('id',$item)->get();
        //$daftarpenjualan2 = $daftar2->where('id',$item);
        
        $jumlahpenjualan = $daftar2->count();

        //Seleksi
        if($jumlahpenjualan == 0){
            $data = [
            ['id_transaksipenjualan' => null, 'id_produktoko' => null, 'produktoko' => null],
            ];  
            $data2 = [
            ['id' => null, 'id_warga' => null, 'id_toko' => null, 'tanggal' => null, 'subtotal' => null, 'status'=> null],
            ]; 
            $koleksi = collect($data);
            $koleksi->toJson();
            $koleksi2 = collect($data2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
    public function updateapp(Request $request)
    {
        $item = $request->id;
        settype($item, "integer"); 
        //1.Pencarian berdasarkan Id 
        $penjualan = TransaksiPenjualan::findOrFail($item);
        $penjualan->status = 3;
        $penjualan->save();
        
        //Seleksi Data Warga
        $idtoko = $request->input('id_toko');
    	settype($idtoko, "integer");
    	$daftarperangkat = PerangkatToko::where('id_toko',$idtoko)->get();
    	
    	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        //Jika Usulan diterima
        $content = array("en" => 'Pengiriman barang sukses diterima konsumen.');
        
	    $fields = array(
			'app_id' => "b842e2bc-4de5-4cd7-b808-142b2917496f",
			'include_player_ids' => $device_id,
			//'include_player_ids' => array("1d4ee16b-3a59-4038-a912-dc868942e1c1"),
      		'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic NDgzY2UxOTItNDRmZi00OTFkLWE1MjAtODFhNWVjMmQxOTU4'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
        
        return $penjualan;
    }
    
    
    //TOKO
    public function indexappToko($item)
    {
        settype($item, "integer");
        $daftartransaksi = TransaksiPenjualan::where('id_toko',$item)->orderBy('id', 'desc')->get();
        $koleksi = collect($daftartransaksi);
        $koleksi->toJson();
        return $koleksi;
    }
    public function detailToko($item){
        settype($item, "integer"); 
        //Detail Penjualan
        $daftar = DetailPenjualan::with('produktoko')->where('id_transaksipenjualan',$item)->get();
        //$daftarpenjualan = $daftar->where('id_transaksipenjualan',$item);
        $daftar2 = TransaksiPenjualan::where('id',$item)->get();
        //$daftarpenjualan2 = $daftar2->where('id',$item);
        
        $jumlahpenjualan = $daftar2->count();

        //Seleksi
        if($jumlahpenjualan == 0){
            $data = [
            ['id_transaksipenjualan' => null, 'id_produktoko' => null, 'produktoko' => null],
            ];  
            $data2 = [
            ['id' => null, 'id_warga' => null, 'id_toko' => null, 'tanggal' => null, 'subtotal' => null, 'status'=> null],
            ]; 
            $koleksi = collect($data);
            $koleksi->toJson();
            $koleksi2 = collect($data2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
    public function updateappToko(Request $request)
    {
        $item = $request->id;
        settype($item, "integer"); 
        //1.Pencarian berdasarkan Id 
        $penjualan = TransaksiPenjualan::findOrFail($item);
        $penjualan->status = 2;
        $penjualan->save();
        
        //Seleksi Data Warga
        $idwarga = $request->input('id_warga');
    	settype($idwarga, "integer");
    	$daftarperangkat = Perangkat::where('id_warga',$idwarga)->get();
    	
    	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        //Jika Usulan diterima
        $content = array("en" => 'Order pembelian mu sedang dikirim.');
        
	    $fields = array(
			'app_id' => "b842e2bc-4de5-4cd7-b808-142b2917496f",
			'include_player_ids' => $device_id,
			//'include_player_ids' => array("f4e464a3-2d8d-41be-922d-c05d2d6b434a"),
      		'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic NDgzY2UxOTItNDRmZi00OTFkLWE1MjAtODFhNWVjMmQxOTU4'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
        
        return $penjualan;
    }
    /*
    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id keranjang
        $keranjang = Keranjang::findOrFail($item);
        //2. Hapus data
        $keranjang->delete();
        return $keranjang;
    }*/
}
