<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\KategoriprodukRequest;
use App\Http\Controllers\Controller;

use App\KategoriProduk;
use App\SubKategoriProduk;
use Storage;
use Session;

class KategoriprodukwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
         $daftarkategoriproduk = KategoriProduk::orderBy('id', 'asc')->paginate(20);
         //2. Menghitung total keseluruhan jumlah warga
         $jumlahkategoriproduk = KategoriProduk::count();
         //3. MEMBUAT DATA API / JSON UNTUK 
         return view('kategoriproduk.index', compact('daftarkategoriproduk','jumlahkategoriproduk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategoriproduk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KategoriprodukRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('foto')){
            $input['foto'] = $this->uploadFoto($request);
        }
        else{
            $input['foto'] = "noimage.png";
        }
        //Simpan Data Usulan
        $kategoriproduk = KategoriProduk::create($input);
        Session::flash('flash_message', 'Kategori Berhasil Disimpan');
        return redirect('kategoriproduk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(KategoriProduk $kategoriproduk)
    {
        $data = SubKategoriproduk::all();
        $daftarsubkategoriproduk= $data->where('id_kategoriproduk',$kategoriproduk->id);
        $jumlahsubkategoriproduk = $daftarsubkategoriproduk->count();
        return view('kategoriproduk.sub', compact('daftarsubkategoriproduk','jumlahsubkategoriproduk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(KategoriProduk $kategoriproduk)
    {
        return view('kategoriproduk.edit', compact('kategoriproduk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( KategoriprodukRequest $request,KategoriProduk $kategoriproduk)
    {
        $input = $request->all();
        if($request->hasFile('foto')){
            //Hapus foto lama
            $this->hapusFoto($kategoriproduk);
            //Upload foto baru
            $input['foto'] = $this->uploadFoto($request);
        }
        $kategoriproduk->update($input);
            Session::flash('flash_message', 'Kategori Produk berhasil diupdate');
            return redirect('kategoriproduk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(KategoriProduk $kategoriproduk)
    {
        $this->hapusFoto($kategoriproduk);
        $kategoriproduk->delete();
        Session::flash('flash_message', 'Kategori Produk Berhasil Dihapus');
        Session::flash('Penting', true);
        return redirect('kategoriproduk');
    }

    //Upload foto ke direktori lokal
    public function uploadFoto(KategoriprodukRequest $request){
        $foto = $request->file('foto');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('foto')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('foto')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
    }

    //Hapus foto di direktori lokal
    public function hapusFoto(KategoriProduk $kategoriproduk){
        $exist = Storage::disk('foto')->exists($kategoriproduk->foto);
        if(isset($kategoriproduk->foto) && $exist){
           $delete = Storage::disk('foto')->delete($kategoriproduk->foto);
           if($delete){
            return true;
           }
           return false;
        }
    }

}
