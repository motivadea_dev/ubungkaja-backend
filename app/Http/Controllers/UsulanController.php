<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warga;
use App\Kategori;
use App\Usulan;
use App\FotoTemp;
use App\FotoUsulan;
use App\ProposalTemp;
use App\ProposalUsulan;
use App\ProfileDesa;
use App\Perangkat;
use Storage;

class UsulanController extends Controller
{
    public function indexapp($item){ 
        //1. Menampilkan semua data usulan dan dibagi per halaman 20 baris
        $daftarusulan = Usulan::with('kategori','fotousulan','proposalusulan')->where('id_warga',$item)->get();
        //2. Menghitung total keseluruhan jumlah usulan
        $jumlahusulan = Usulan::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftarusulan);
        $koleksi->toJson();

        //1. Menampilkan semua data usulan dan dibagi per halaman 20 baris
        $daftarkategori = Kategori::all();
        //2. Menghitung total keseluruhan jumlah usulan
        $jumlahkategori = Kategori::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi2 = collect($daftarkategori);
        $koleksi2->toJson();

        return compact('koleksi','koleksi2');
    }
    public function indexdusun($item){     
        settype($item, "integer");
        $daftarusulan = Usulan::join('warga', function($join) use($item){
            $join->on('usulan.id_warga', '=', 'warga.id')
                 ->where('warga.id_dusun', '=', $item);
        })->get();
        //$daftarusulan = Usulan::with('warga','kategori','fotousulan','proposalusulan')->get();
        $koleksi = collect($daftarusulan);
        $koleksi->toJson();
        
        return $koleksi;

    }
    public function usulanbaru(){  
        settype($item, "integer");
        $daftarusulan = Usulan::with('warga','kategori','fotousulan','proposalusulan')->get();
        $koleksi = collect($daftarusulan);
        $koleksi->toJson();
        
        return $koleksi;

    }

    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data usulan 
        $usulan = Usulan::create($input);
        //Ambil ID Transaksi
        $id_awal = Usulan::orderBy('id', 'desc')->first();
        $idusulan = $id_awal->id;
        settype($idusulan, "integer");
        $usulan = New Usulan;
        $usulan = Usulan::findOrFail($idusulan);
        $usulan->rangking = $idusulan;
        $usulan->update();
        //Ambil ID Warga
        $idwarga = $request->input('id_warga');
        settype($idwarga, "integer");
        //Ambil Data Foto Sementara
        $daftar = FotoTemp::all();
        $daftarfoto = $daftar->where('id_warga',$idwarga);
        //Simpan Foto Usulan
        foreach($daftarfoto as $foto){
            $fotousulan = New FotoUsulan;
            $fotousulan->id_usulan = $idusulan;
            $fotousulan->foto = $foto->foto;
            $fotousulan->save();
            //Hapus Foto Sementara
            $foto->delete();
        }
        //Ambil Data Proposal Sementara
        $daftar2 = ProposalTemp::all();
        $daftarprop = $daftar2->where('id_warga',$idwarga);
        //Simpan Proposal Usulan
        foreach($daftarprop as $prop){
            $propusulan = New ProposalUsulan;
            $propusulan->id_usulan = $idusulan;
            $propusulan->file = $prop->file;
            $propusulan->save();
            //Hapus Proposal Sementara
            $prop->delete();
        }
        
        //Notifikasi ke dusun
        $daftarwarga = Warga::where('id',5)->get();
        foreach($daftarwarga as $warga){
            $iddusun = $warga->id_dusun;
            settype($iddusun, "integer");
        }
        $daftarperangkat = Perangkat::join('warga', function($join) use($iddusun){
            $join->on('perangkat.id_warga', '=', 'warga.id')
                 ->where('warga.id_dusun', '=', $iddusun);
        })->join('users', function($join){
            $join->on('users.username', '=', 'warga.noktp')
                 ->where('users.level', '=', 'dusun');
        })->get();
        	
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        
        $content = array("en" => 'Ada usulan baru dari Warga');
	    $fields = array(
		'app_id' => "0cd199c8-a64b-4b4d-be1f-913897925183",
	    'include_player_ids' => $device_id,
      	'data' => array("foo" => "bar"),
		'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic ODM1NmExN2QtNjIwYy00OWVmLTljYzctOTM2MjZiNDgzNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
        
        return $usulan;
    }
    //Upload foto ke direktori lokal
    public function uploadFoto(Request $request){
        $foto = $request->file('file');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('file')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('file')->move($upload_path, $foto_name); //Copy paste ke server
            //Simpan Link Foto
            $fototemp = New FotoTemp;
            $fototemp->id_warga = $request->id_warga;
            $fototemp->foto = $foto_name;
            $fototemp->save();
            
            return $foto_name;
        }
        return false;
    }
    public function uploadProposal(Request $request){
        $foto = $request->file('prop');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('prop')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fileupload';            //Folder foto sing diupload ke server
            $request->file('prop')->move($upload_path, $foto_name); //Copy paste ke server
            //Simpan Link Proposal
            $proptemp = New ProposalTemp;
            $proptemp->id_warga = $request->id_warga;
            $proptemp->file = $foto_name;
            $proptemp->save();
            
            return $foto_name;
        }
        return false;
    }
    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id usulan
        $usulan = Usulan::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data usulan
        $usulan->update($input);
        
        //Ambil ID Usulan
        $idusulan = $usulan->id;
        settype($idusulan, "integer");
        //Ambil ID Warga
        $idwarga = $request->input('id_warga');
        settype($idwarga, "integer");
        //Ambil Data Foto Sementara
        $daftar = FotoTemp::all();
        $daftarfoto = $daftar->where('id_warga',$idwarga);
        //Ambil Data Proposal Sementara
        $daftar1 = ProposalTemp::all();
        $daftarfoto1 = $daftar1->where('id_warga',$idwarga);
        //Simpan Foto Usulan
        foreach($daftarfoto as $foto){
            $fotousulan = New FotoUsulan;
            $fotousulan->id_usulan = $idusulan;
            $fotousulan->foto = $foto->foto;
            $fotousulan->save();
            //Hapus Foto Sementara
            $foto->delete();
        }
        //Simpan Proposal Usulan
        foreach($daftarfoto1 as $foto1){
            $propusulan = New ProposalUsulan;
            $propusulan->id_usulan = $idusulan;
            $propusulan->file = $foto1->file;
            $propusulan->save();
            //Hapus Foto Sementara
            $foto1->delete();
        }
        return $usulan;
    }
    public function updatedusun(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id usulan
        $usulan = Usulan::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data usulan
        $usulan->update($input);
        
        //Seleksi Data Warga
        $idwarga = $request->input('id_warga');
    	settype($idwarga, "integer");
    	$daftarperangkat = Perangkat::where('id_warga',$idwarga)->get();
    	
    	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        //Jika Usulan diterima
        if($request->input('status') == 2){
            $content = array("en" => 'Selamat,usulan Anda diterima');
        }
        else if($request->input('status') == 2){
            $content = array("en" => 'Maaf,usulan Anda tidak diterima');
        }
        
	    $fields = array(
			'app_id' => "0cd199c8-a64b-4b4d-be1f-913897925183",
			'include_player_ids' => $device_id,
      		'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic ODM1NmExN2QtNjIwYy00OWVmLTljYzctOTM2MjZiNDgzNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
        
        return $usulan;
    }
    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id usulan
        $usulan = Usulan::findOrFail($item);
        //2. Hapus data
        $usulan->delete();
        return $usulan;
    }
    public function destroyfoto($item)  
    {
        //1. Pencarian berdasarkan Id usulan
        $usulan = FotoUsulan::findOrFail($item);
        //2. Hapus data
        $usulan->delete();
        return $usulan;
    }
    public function destroyprop($item)      
    {
        //1. Pencarian berdasarkan Id usulan
        $usulan = ProposalUsulan::findOrFail($item);
        //2. Hapus data
        $usulan->delete();
        return $usulan;
    }
}
