<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warga;
use App\Keperluan;
use App\Antrian;
use App\FotoTemp;
use App\FotoAntrian;
use Storage;

class AntrianController extends Controller
{
    public function indexapp(){
        //1. Menampilkan semua data antrian dan dibagi per halaman 20 baris
        $daftarantrian = Antrian::with('keperluan','fotoantrian')->get();;
        //2. Menghitung total keseluruhan jumlah antrian
        $jumlahantrian = Antrian::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftarantrian);
        $koleksi->toJson();

        //1. Menampilkan semua data antrian dan dibagi per halaman 20 baris
        $daftarkategori = Keperluan::all();
        //2. Menghitung total keseluruhan jumlah antrian
        $jumlahkategori = Keperluan::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi2 = collect($daftarkategori);
        $koleksi2->toJson();

        return compact('koleksi','koleksi2');
    }
    
    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data antrian 
        $antrian = Antrian::create($input);
        //Ambil ID Transaksi
        $id_awal = Antrian::orderBy('id', 'desc')->first();
        $idantrian = $id_awal->id;
        settype($idantrian, "integer");
        //Ambil ID Warga
        $idwarga = $request->input('id_warga');
        settype($idwarga, "integer");
        //Ambil Data Foto Sementara
        $daftar = FotoTemp::all();
        $daftarfoto = $daftar->where('id_warga',$idwarga);
        //Simpan Foto antrian
        foreach($daftarfoto as $foto){
            $fotoantrian = New FotoAntrian;
            $fotoantrian->id_antrian = $idantrian;
            $fotoantrian->foto = $foto->foto;
            $fotoantrian->save();
            //Hapus Foto Sementara
            $foto->delete();
        }

        return $antrian;
    }
    //Upload foto ke direktori lokal
    public function uploadFoto(Request $request){
        $foto = $request->file('file');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('file')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('file')->move($upload_path, $foto_name); //Copy paste ke server
            //Simpan Link Foto
            $fototemp = New FotoTemp;
            $fototemp->id_warga = $request->id_warga;
            $fototemp->foto = $foto_name;
            $fototemp->save();
            
            return $foto_name;
        }
        return false;
    }

    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id antrian
        $antrian = Antrian::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data antrian
        $antrian->update($input);
        return $antrian;
    }
    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id antrian
        $antrian = Antrian::findOrFail($item);
        //2. Hapus data
        $antrian->delete();
        return $antrian;
    }
}
