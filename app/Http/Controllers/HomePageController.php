<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KomentarRequest;
use App\Http\Requests\KomentarPengaduanRequest; 
use App\Http\Requests\DaftarRequest; 
use App\Http\Requests\DaftarDesaRequest;

use App\Http\Requests;
use App\KategoriPost;
use App\Posting;
use App\Informasi;
use App\KategoriPengaduan;
use App\Pengaduan;
use App\FotoPengaduan;
use App\FotoUsulan;
use App\KomentarPost;
use App\KomentarPengaduan;
use App\BalasKomentar;
use App\BalasKomentarPengaduan;
use App\Usulan;
use App\Kategori;
use App\User;
use Session;
use Mapper;

class HomePageController extends Controller
{
    //
    public function index(){
		//1. Menampilkan semua data warga dan dibagi per halaman 20 baris
		$daftarinformasi = Informasi::all();
		$daftarkategoripost = KategoriPost::all();
		$daftarterbaru = Posting::orderBy('tanggal','desc')->get();
		$daftarpopuler = Posting::orderBy('view','desc')->get();
		return view('cms.home', compact('daftarinformasi','daftarkategoripost','daftarterbaru','daftarpopuler'));

	}
	//Informasi Posting
	public function informasi($kategori){
		$daftarkategoripost = KategoriPost::all();
		$daftarterbaru = Posting::where('id_kategoripost',$kategori)->where('status',2)
		->orderBy('tanggal','desc')->paginate(5);
		$jumlahterbaru = $daftarterbaru->count();
		$daftarpopuler = Posting::where('status',2)->orderBy('view','desc')->get();
		return view('cms.informasi', compact('daftarkategoripost','daftarterbaru','daftarpopuler','jumlahterbaru'));
	}

	public function detailinformasi(Posting $posting)
    {
		$daftarkategoripost = KategoriPost::all();
		$daftarpopuler = Posting::where('status',2)->orderBy('view','desc')->get();
		$daftar = KomentarPost::with('balaskomentar')->where('id_posting',$posting->id)->get();
        $jumlahkomentar = $daftar->count();
        $daftarkomentar = collect($daftar);
        $daftarkomentar->toJson();
		return view('cms.detailinformasi',compact('posting','daftarkategoripost','daftarpopuler','daftar','jumlahkomentar','daftarkomentar'));
	}
	public function storekomentar(KomentarRequest $request)
    {
		$input = $request->all();
        $balasan = KomentarPost::create($input);
        Session::flash('flash_message', 'Komentar Berhasil Disimpan');
		return redirect('detailinformasi/'.$request->id_posting);
	}
	public function storebalasan(Request $request)
    {
        $input = $request->all();
        $balasan = BalasKomentar::create($input);
        Session::flash('flash_message', 'Balasan Berhasil Disimpan');
		return redirect('detailinformasi/'.$request->id_posting);
	}
	//Usulan
	public function usulanwarga(){
		$daftarkategoripost = KategoriPost::all();
		$daftarkategori = Kategori::all();
		$daftarterbaru = Usulan::where('status',2)->orderBy('tanggal','desc')->paginate(5);
		$jumlahterbaru = $daftarterbaru->count();
		return view('cms.usulan', compact('daftarkategori','daftarterbaru','jumlahterbaru','daftarkategoripost'));
	}
	public function usulanwarga2($kategori){
		$daftarkategoripost = KategoriPost::all();
		$daftarkategori = Kategori::all();
		$daftarterbaru = Usulan::where('id_kategori',$kategori)->where('status',2)->orderBy('tanggal','desc')->paginate(5);
		$jumlahterbaru = $daftarterbaru->count();
		return view('cms.usulan', compact('daftarkategori','daftarterbaru','jumlahterbaru','daftarkategoripost'));
	}
	public function detailusulan(Usulan $usulan)
    {
		$idusulan = $usulan->id;
        settype($idusulan, "integer");
		$fotousulan = FotoUsulan::where('id_usulan',$idusulan)->get();
		$daftarkategori = Kategori::all();
		$daftarkategoripost = KategoriPost::all();
		$map = Mapper::map($usulan->latitude, $usulan->longitude, ['zoom' => 15, 'markers' => ['title' => 'Lokasi Usulan', 'animation' => 'DROP']]);
		return view('cms.detailusulan',compact('daftarkategori','fotousulan','map','usulan','daftarkategoripost'));
	}

	//Pengaduan
	public function pengaduanwarga(){
		$daftarkategoripost = KategoriPost::all(); //navbar poenya
		$daftarkategori = KategoriPengaduan::all(); //kategoripengaduan
		$daftarterbaru = Pengaduan::where('privasipengaduan',1)->orderBy('tanggal','desc')->paginate(5);
		$jumlahterbaru = $daftarterbaru->count();
		return view('cms.pengaduan', compact('daftarkategori','daftarterbaru','jumlahterbaru','daftarkategoripost'));
	}
	public function pengaduanwarga2($kategori){
		$daftarkategoripost = KategoriPost::all();
		$daftarkategori = KategoriPengaduan::all();
		$daftarterbaru = Pengaduan::where('id_kategoripengaduan',$kategori)->where('privasipengaduan',1)->orderBy('tanggal','desc')->paginate(5);
		$jumlahterbaru = $daftarterbaru->count();
		return view('cms.pengaduan', compact('daftarkategori','daftarterbaru','jumlahterbaru','daftarkategoripost'));
	}
	public function detailpengaduan(Pengaduan $pengaduan)
    {
		$idpengaduan = $pengaduan->id;
		settype($idpengaduan, "integer");
		$daftar = KomentarPengaduan::where('id_pengaduan',$pengaduan->id)->get();
        $jumlahkomentar = $daftar->count();
        $daftarkomentar = collect($daftar);
        $daftarkomentar->toJson();
		$fotopengaduan = FotoPengaduan::where('id_pengaduan',$idpengaduan)->get();
		$daftarkategoripost = KategoriPost::all();
		$daftarkategoripengaduan = KategoriPengaduan::all();
		$map = Mapper::map($pengaduan->latitude, $pengaduan->longitude, ['zoom' => 15, 'markers' => ['title' => 'Lokasi Pengaduan', 'animation' => 'DROP']]);
		return view('cms.detailpengaduan',compact('fotopengaduan','map','pengaduan','daftarkategoripengaduan','daftar','jumlahkomentar','daftarkomentar','daftarkategoripost'));
	}

	public function komentarpengaduan(KomentarPengaduanRequest $request)
    {
		$input = $request->all();
        $balasan = KomentarPengaduan::create($input);
        Session::flash('flash_message', 'Komentar Berhasil Disimpan');
		return redirect('detailpengaduan/'.$request->id_pengaduan);
	}
	public function balaspengaduan(Request $request)
    {
        $input = $request->all();
        $balasan = BalasKomentarPengaduan::create($input);
        Session::flash('flash_message', 'Balasan Berhasil Disimpan');
		return redirect('detailpengaduan/'.$request->id_pengaduan);
	}

	//Pendaftaran
	public function daftarperangkat(){
		return view('auth.daftarperangkat');

	}
	public function registrasi(DaftarRequest $request)
    {
		$input = $request->all();
		$input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        Session::flash('flash_message', 'Pendaftaran Sukses,silahkan cek Email anda untuk verifikasi.');
		return redirect('/register');
	}
	public function registrasidesa(DaftarDesaRequest $request)
    {
		$input = $request->all();
		//Enkripsi Password
		$input['password'] = bcrypt($input['password']);
		//Upload Foto
		if($request->hasFile('fotoktp')){
			$input['fotoktp'] = $this->uploadFotoKTP($request);
		}
		else{
			$input['fotoktp'] = "noimage.png";
		}
		if($request->hasFile('fotowajah')){
			$input['fotowajah'] = $this->uploadFotoWajah($request);
		}
		else{
			$input['fotowajah'] = "noimage.png";
		}
		//Simpan Pendaftaran
        $user = User::create($input);
        Session::flash('flash_message', 'Pendaftaran Sukses,silahkan cek Email anda untuk verifikasi.');
		return redirect('/daftarperangkat');
	}
	public function uploadFotoKTP(DaftarDesaRequest $request){
        $foto = $request->file('fotoktp');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('fotoktp')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('fotoktp')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
	}
	public function uploadFotoWajah(DaftarDesaRequest $request){
        $foto = $request->file('fotowajah');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('fotowajah')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('fotowajah')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
    }
}
