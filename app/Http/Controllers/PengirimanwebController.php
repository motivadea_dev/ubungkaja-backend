<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PengirimanRequest;
use App\Http\Controllers\Controller;

use App\TransaksiPembelian;
use App\Pengiriman;
use Storage;
use Session;

class PengirimanwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
       $daftarpengiriman = Pengiriman::orderBy('id', 'desc')->paginate(20);;
       //2. Menghitung total keseluruhan jumlah warga
       $jumlahpengiriman = Pengiriman::count();
       //3. MEMBUAT DATA API / JSON UNTUK 
       return view('pengiriman.index', compact('daftarpengiriman','jumlahpengiriman'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $input = $request->all();
        //Simpan Data Usulan
        $pengiriman = Pengiriman::create($input);
        Session::flash('flash_message', 'Pengiriman Berhasil Disimpan');
        return redirect('pengiriman');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
