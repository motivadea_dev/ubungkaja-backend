<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PenghasilanRequest;
use App\Http\Controllers\Controller;

use App\Penghasilan;
use Session;

class PenghasilanwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarpenghasilan = Penghasilan::orderBy('id','asc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahpenghasilan = Penghasilan::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('penghasilan.index', compact('daftarpenghasilan','jumlahpenghasilan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penghasilan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PenghasilanRequest $request, Penghasilan $penghasilan)
    {
        $input = $request->all();
        //Simpan Data Usulan
        $penghasilan = Penghasilan::create($input);
        Session::flash('flash_message', 'Penghasilan Berhasil Disimpan');
        return redirect('penghasilan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Penghasilan $penghasilan)
    {
        return view('penghasilan.edit', compact('penghasilan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PenghasilanRequest $request,Penghasilan $penghasilan)
    {
        $input = $request->all();
        
                $penghasilan->update($input);
        
                Session::flash('flash_message', 'Penghasilan berhasil diupdate');
                return redirect('penghasilan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penghasilan $penghasilan)
    {
        $penghasilan->delete();
        Session::flash('flash_message', 'Penghasilan berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('penghasilan');
    }
}
