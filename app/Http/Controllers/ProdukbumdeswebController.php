<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProdukBumdesRequest;
use App\Http\Controllers\Controller;


use App\ProdukBumdes;
use App\KategoriProduk;
use App\SubKategoriProduk;
use Session;
use Storage;


class ProdukbumdeswebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
                $daftarprodukbumdes = ProdukBumdes::orderBy('id', 'asc')->paginate(20);;
                //2. Menghitung total keseluruhan jumlah warga
                $jumlahprodukbumdes = ProdukBumdes::count();
                //3. MEMBUAT DATA API / JSON UNTUK 
                return view('produkbumdes.index', compact('daftarprodukbumdes','jumlahprodukbumdes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produkbumdes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdukBumdesRequest $request)
    {
        $input = $request->all();
        
                if($request->hasFile('foto')){
                    $input['foto'] = $this->uploadFoto($request);
                }
                else{
                    $input['foto'] = "noimage.png";
                }
                //Simpan Data Usulan
                $produkbumdes = ProdukBumdes::create($input);
                Session::flash('flash_message', 'Produk Bumdes Berhasil Disimpan');
                return redirect('produkbumdes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ProdukBumdes $produkbumdes)
    {
        return view('produkbumdes.show',compact('produkbumdes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProdukBumdes $produkbumdes)
    {
        return view('produkbumdes.edit', compact('produkbumdes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdukBumdesRequest $request,ProdukBumdes $produkbumdes)
    {
        $input = $request->all();
        
                if($request->hasFile('foto')){
                    //Hapus foto lama
                    $this->hapusFoto($produkbumdes);
                    //Upload foto baru
                    $input['foto'] = $this->uploadFoto($request);
                }
        
                $produkbumdes->update($input);
        
                Session::flash('flash_message', 'Produk Bumdes berhasil diupdate');
                return redirect('produkbumdes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProdukBumdes $produkbumdes)
    {
        $this->hapusFoto($produkbumdes);
        $produkbumdes->delete();
        Session::flash('flash_message', 'Produk Bumdes berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('produkbumdes');
    }

    //Upload foto ke direktori lokal
    public function uploadFoto(ProdukBumdesRequest $request){
        $foto = $request->file('foto');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('foto')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('foto')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
    }

    //Hapus foto di direktori lokal
    public function hapusFoto(ProdukBumdes $produkbumdes){
        $exist = Storage::disk('foto')->exists($produkbumdes->foto);
        if(isset($produkbumdes->foto) && $exist){
           $delete = Storage::disk('foto')->delete($produkbumdes->foto);
           if($delete){
            return true;
           }
           return false;
        }
    }
}
