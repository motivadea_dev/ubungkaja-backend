<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriPost;
use App\Http\Requests;
use App\Http\Requests\KategoripostRequest;
use App\Http\Controllers\Controller;

use Storage;
use Session;

class KategoripostwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarkategoripost = KategoriPost::orderBy('id', 'desc')->paginate(20);
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahkategoripost = KategoriPost::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('kategoripost.index', compact('daftarkategoripost','jumlahkategoripost'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategoripost.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KategoripostRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('foto')){
            $input['foto'] = $this->uploadFoto($request);
        }
        else{
            $input['foto'] = "noimage.png";
        }
        //Simpan Data Usulan
        $kategoripost = KategoriPost::create($input);
        Session::flash('flash_message', 'Kategori Berhasil Disimpan');
        return redirect('kategoripost');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(KategoriPost $kategoripost)
    {
        return view('kategoripost.edit', compact('kategoripost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KategoripostRequest $request,Kategoripost $kategoripost)
    {
        $input = $request->all();
        if($request->hasFile('foto')){
            //Hapus foto lama
            $this->hapusFoto($kategoripost);
            //Upload foto baru
            $input['foto'] = $this->uploadFoto($request);
        }
        $kategoripost->update($input);
            Session::flash('flash_message', 'Kategori Posting berhasil diupdate');
            return redirect('kategoripost');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(KategoriPost $kategoripost)
    {
        $this->hapusFoto($kategoripost);
        $kategoripost->delete();
        Session::flash('flash_message', 'Kategori Posting Berhasil Dihapus');
        Session::flash('Penting', true);
        return redirect('kategoripost');
    }

    //Upload foto ke direktori lokal
    public function uploadFoto(KategoripostRequest $request){
        $foto = $request->file('foto');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('foto')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('foto')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
    }

    //Hapus foto di direktori lokal
    public function hapusFoto(KategoriPost $kategoripost){
        $exist = Storage::disk('foto')->exists($kategoripost->foto);
        if(isset($kategoripost->foto) && $exist){
           $delete = Storage::disk('foto')->delete($kategoripost->foto);
           if($delete){
            return true;
           }
           return false;
        }
    }
}
