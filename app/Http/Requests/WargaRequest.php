<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class WargaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH'){
        $noktp_rules = 'required|numeric|max:9999999999999999|unique:warga,noktp,' . $this->get('id');
        }
        else{
        $noktp_rules = 'required|numeric|max:9999999999999999|unique:warga,noktp';
        }
        
        return [
            'noktp' => $noktp_rules,
            'nama' => 'required|string|max:50',
            'alamat' => 'required',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|in:L,P',
            'foto' => 'sometimes|image|max:1024|mimes:jpeg,jpg,bmp,png',
            //'password' => 'required|string|max:50',
            'id_dusun' => 'required',        
        ];
    }
}
