<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DistributorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH'){
        $kodedistributor_rules = 'required|string|max:50|unique:distributor,kodedistributor,' . $this->get('id');
        }
        else{
        $kodedistributor_rules = 'required|string|max:50|unique:distributor,kodedistributor';
        }
        
        return [
            'kodedistributor' => $kodedistributor_rules,
            'namadistributor' => 'required',
            'alamat' => 'required',
            'foto' => 'sometimes|image|max:1024|mimes:jpeg,jpg,bmp,png',
            'status' => 'required|in:buka,tutup',       
        ];
    }
}
