<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PengaduanRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => 'required|date',
            'judul' => 'required|string',
            'id_kategoripengaduan' => 'required',
            'id_dusun' => 'required',
            'id_warga' => 'required',
            //'longitude' => 'required',
            //'latitude' => 'required',
            'deskripsi' => 'required',
            'privasipengaduan' => 'required|in:1,2',
            'privasiidentitas' => 'required|in:1,2',
            'status' => 'required|in:1,2,3',
        ];
    }
}
