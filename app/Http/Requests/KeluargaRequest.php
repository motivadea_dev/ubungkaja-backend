<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class KeluargaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH'){
        $kodekeluarga_rules = 'required|string|max:50|unique:keluarga,nokk,' . $this->get('id');
        }
        else{
        $kodekeluarga_rules = 'required|string|max:50|unique:keluarga,nokk';
        }
        
        return [
            'id_profiledesa'=> 'required',
            'nokk' => $kodekeluarga_rules,
            'tanggal' => 'required|date',    
        ];
    }
}
