<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TransaksiPenjualanRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => 'required|date',
            'id_toko'=> 'required',
            'id_warga' => 'required',
            'totaldiskon' => 'required',
            'totalbelanja' => 'required',
            'subtotal' => 'required',
            'status' => 'required',
        ];
    }
}
