<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class KurirRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH'){
            $nokurir_rules = 'required|string|max:50|unique:kurir,nomor_kurir,' . $this->get('id');
            $platnomor_rules = 'required|string|max:50|unique:kurir,plat_nomor,' . $this->get('id');
            }
            else{
            $nokurir_rules = 'required|string|max:50|unique:kurir,nomor_kurir';
            $platnomor_rules = 'required|string|max:50|unique:kurir,plat_nomor';
            }
            
            return [
                'nomor_kurir'=> $nokurir_rules,
                'id_warga'=> 'required',
                'jenis_kendaraan'=> 'required',
                'plat_nomor' => $platnomor_rules,
                'status' => 'required|in:aktif,nonaktif',     
            ];
    }
}
