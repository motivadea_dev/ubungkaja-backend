<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TokoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH'){
        $kodetoko_rules = 'required|string|max:50|unique:toko,kodetoko,' . $this->get('id');
        }
        else{
        $kodetoko_rules = 'required|string|max:50|unique:toko,kodetoko';
        }
        
        return [
            'kodetoko' => $kodetoko_rules,
            'namatoko' => 'required',
            'alamat' => 'required',
            'foto' => 'sometimes|image|max:1024|mimes:jpeg,jpg,bmp,png',
            'status' => 'required|in:buka,tutup',       
        ];
    }
}
