<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposalTemp extends Model
{
    protected $table = 'proposaltemp';

    protected $fillable = [
        'id_warga',
        'file',  	        
    	'created_at',
        'updated_at'
    ];
}
