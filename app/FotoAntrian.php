<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoAntrian extends Model
{
    protected $table = 'fotoantrian';

    protected $fillable = [
        'id_antrian',
        'foto',       
    	'created_at',
        'updated_at'
    ];

    public function antrian(){
        return $this->belongsTo('App\Antrian', 'id_antrian');
    }
}
