<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $table = 'pekerjaan';

    protected $fillable = [
    	'namapekerjaan', 	        
    	'created_at',
        'updated_at'
    ];
}
