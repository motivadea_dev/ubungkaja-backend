<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dompet extends Model
{
    protected $table = 'dompet';
    
    protected $fillable = [
        'id_warga',
        'saldo',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many dari
    public function detaildompet(){
        return $this->hasMany('App\DetailDompet', 'id_dompet');
    }

    //Relasi One to Many ke
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
}
