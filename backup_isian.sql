INSERT INTO `profiledesa` (`id`, `nama`, `kecamatan`, `kabupaten`, `provinsi`, `nip`, `nama_kades`, `created_at`, `updated_at`) VALUES
(1, 'Desa Ubung Kaja', 'Denpasar Utara', 'Denpasar', 'Bali', '123456778', 'I Wayan Mirta', NULL, NULL);

INSERT INTO `dusun` (`id`, `id_profiledesa`, `nama`, `created_at`, `updated_at`) VALUES
(1, 1, 'Muktiharjo Kidul', NULL, NULL),
(2, 1, 'Tlogosari Kulon', NULL, NULL);

INSERT INTO `users` (`id`, `name`, `username`, `password`, `remember_token`, `level`, `status`, `fotoktp`, `fotowajah`, `nohp`, `email`, `id_dusun`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '$2y$10$qYX1WgLPv9KEN2JS0wCCS.1h25Wu9D6h81S/5X/PB6rxHoc5goXny', 'aUWFtkHQc2oRGkmX8mvYmSXMzvv9POBthcjuRbEzOTrX0EkxJTsI9IlUAqsc', 'admin', '2', '', '', '', '', 1, NULL, '2018-01-29 18:14:04'),
(29, 'Awan Aprifiantosa', '3374060104860005', '$2y$10$Pw9Fn6Bzfd3/YOzVE4FIXePKbCM.Q.6NbmJP0rj97XCXcaugAScyC', NULL, 'warga', '2', 'ktp_3374060104860005.jpg', 'wajah_3374060104860005.jpg', '081279349292', 'aoneholics@gmail.com', 1, '2018-01-29 18:04:12', '2018-01-29 18:07:44'),
(31, 'Galih', '54321', '$2y$10$xCso7bLfj6bungSP3tPcLeCJEfdjXywo1WnjIhXQ/OPoSD40j7GqG', NULL, 'warga', '2', 'ktp_54321.jpg', 'wajah_54321.jpg', '08530303030', 'Galileogalilei@gmail.com', 2, '2018-02-02 14:36:15', '2018-02-02 14:37:05'),
(32, 'Febri', '1357', '$2y$10$uxZ1JjQEt1x4kJB5cDvS8.9lbfmI749RcCYAQ7MRI.tN/8dTol8Wy', NULL, 'warga', '2', 'ktp_1357.jpg', 'wajah_1357.jpg', '083000000000', 'a@b.com', 1, '2018-02-02 14:38:30', '2018-02-02 14:38:47');

INSERT INTO `warga` (`id`, `noktp`, `nama`, `alamat`, `tanggal_lahir`, `jenis_kelamin`, `namapendidikan`, `namapekerjaan`, `namapenghasilan`, `foto`, `id_dusun`, `id_profiledesa`, `created_at`, `updated_at`) VALUES
(1, '12345678', 'Awan A', 'Jalan', '2017-11-01', 'L', '', '', '', '20171119113807.jpg', 1, 1, NULL, '2017-11-17 02:38:07'),
(2, '3374060104860005', 'Awan Aprifiantosa', 'Jalan Gringsing 2 No.24', '1986-04-01', 'L', '', '', '', 'noimage.png', 1, 1, '2018-01-27 15:27:35', '2018-01-27 15:27:35'),
(3, '54321', 'Galih', 'Jalan', '2018-02-02', 'L', '', '', '', 'noimage.png', 2, 1, '2018-02-02 14:32:37', '2018-02-02 14:32:37'),
(4, '1357', 'Febri', 'Jalan', '2018-02-02', 'L', '', '', '', 'noimage.png', 1, 1, '2018-02-02 14:33:07', '2018-02-02 14:33:07');

INSERT INTO `kategori` (`id`, `id_profiledesa`, `namakategori`, `created_at`, `updated_at`) VALUES
(1, 1, 'Penyelenggaraan Pemerintahan Desa', '2017-11-15 19:21:33', '2017-11-15 19:21:33'),
(2, 1, 'Pelaksanaan Pembangunan Desa', '2017-11-15 19:21:44', '2017-11-15 19:21:44'),
(3, 1, 'Pembinaan Kemasyarakatan', '2017-11-15 19:21:54', '2017-11-15 19:21:54'),
(4, 1, 'Pemberdayaan Masyarakat', '2017-11-15 19:22:05', '2017-11-15 19:22:05');

INSERT INTO `kategoriproduk` (`id`, `namakategori`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'Sembako', 'sembako.jpg', '2017-11-26 11:41:22', '2017-11-26 11:41:22'),
(2, 'Peralatan Rumah Tangga', 'rumahtangga.jpg', '2017-11-26 11:41:33', '2017-11-26 11:41:33');

INSERT INTO `subkategoriproduk` (`id`, `id_kategoriproduk`, `namasubkategori`, `created_at`, `updated_at`) VALUES
(2, 1, 'Minyak Goreng', '2017-11-27 23:08:44', '2017-11-27 23:08:44'),
(3, 1, 'Beras', '2017-12-05 14:04:46', '2017-12-05 14:04:46'),
(4, 2, 'Peralatan Kebersihan', '2017-12-10 23:57:25', '2017-12-10 23:57:25');

INSERT INTO `toko` (`id`, `id_warga`, `kodetoko`, `namatoko`, `alamat`, `foto`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '12343564', 'Toko Awan', 'Jalan', 'noimage.png', 'buka', '2017-11-27 23:17:08', '2017-11-27 23:17:08');

INSERT INTO `produktoko` (`id`, `id_kategoriproduk`, `id_subkategoriproduk`, `id_toko`, `kodeproduk`, `namaproduk`, `stok`, `harga`, `diskon`, `foto`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, '12345678', 'Beras Rojo Lele', 0, 15000, 13000, '20171206040636.jpg', '2017-12-05 14:06:36', '2017-12-11 23:40:09'),
(2, 1, 2, 1, '54423534', 'Minyak Sania', 8, 15000, 10000, '20171206041315.jpg', '2017-12-05 14:13:15', '2017-12-11 23:41:23'),
(3, 1, 3, 1, '56432324', 'Beras C4', 15, 10000, 9000, '20171208130247.jpg', '2017-12-08 11:02:47', '2017-12-08 11:02:47'),
(4, 2, 4, 1, '54324332', 'Kain Pel', 100, 5000, 3500, '20171210185822.jpg', '2017-12-10 23:58:22', '2017-12-10 23:58:22');

INSERT INTO `distributor` (`id`, `id_profiledesa`, `kodedistributor`, `namadistributor`, `alamat`, `foto`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '121342546', 'Cyberway Media', 'Jalan Jalan', 'noimage.png', 'buka', '2017-12-17 22:01:46', '2017-12-17 22:01:46');

INSERT INTO `produkbumdes` (`id`, `id_kategoriproduk`, `id_subkategoriproduk`, `id_distributor`, `kodeproduk`, `namaproduk`, `stok`, `harga`, `diskon`, `foto`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, '234234234', 'Beras C4', 96, 10000, 9000, '20171217170238.jpg', '2017-12-17 22:02:38', '2017-12-18 11:40:40');

