@extends('template')

@section('main')
<div id="kategori" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Kategori</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<!--@include('produk.form_pencarian')-->
	<div class="tombol-nav">
		{{ link_to('kategori/create','Tambah Kategori',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarkategori) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Kategori</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarkategori as $kategori): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $kategori-> namakategori }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('kategori/' . $kategori->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['KategoriwebController@destroy',$kategori->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Katagori.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Kategori : {{ $jumlahkategori }}</strong>
	</div>
	<div class="paging">
	{{ $daftarkategori->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop