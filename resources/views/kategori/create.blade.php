@extends('template')

@section('main')
	<div id="kategori" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Kategori</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'kategori', 'files' => true]) !!}

		@include('kategori.form', ['submitButtonText' => 'Tambah Kategori'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop