@extends('template')

@section('main')
	<div id="kategori" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Kategori</h4></b></div>
		<div class="panel-body">
		{!! Form::model($kategori, ['method' => 'PATCH', 'action' => ['KategoriwebController@update', $kategori->id],'files'=>true]) !!}

		@include('kategori.form', ['submitButtonText' => 'Update Kategori'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop