@if (isset($kategoripengaduan))
{!! Form::hidden('id', $kategoripengaduan->id) !!}
@endif

{!! Form::hidden('id_profiledesa', '1') !!}
{{-- Nama Kategori Layanan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('nama_keperluan') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('nama_keperluan','Nama Kategori Layanan',['class' => 'control-label']) !!}
	{!! Form::text('nama_keperluan', null,['class' => 'form-control']) !!}
	@if ($errors->has('nama_keperluan'))
	<span class="help-block">{{ $errors->first('nama_keperluan') }}</span>
	@endif
</div>

{{-- Syarat--}}
@if($errors->any())
<div class="form-group {{ $errors->has('syarat') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('syarat','Berkas Persyaratan',['class' => 'control-label']) !!}
	{!! Form::textarea('syarat', null,['class' => 'form-control']) !!}
	@if ($errors->has('syarat'))
	<span class="help-block">{{ $errors->first('syarat') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>