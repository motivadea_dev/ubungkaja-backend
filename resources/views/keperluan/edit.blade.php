@extends('template')

@section('main')
	<div id="keperluan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Kategori Keperluan</h4></b></div>
		<div class="panel-body">
		{!! Form::model($keperluan, ['method' => 'PATCH', 'action' => ['KeperluanwebController@update', $keperluan->id],'files'=>true]) !!}

		@include('keperluan.form', ['submitButtonText' => 'Update Kategori Layanan'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop