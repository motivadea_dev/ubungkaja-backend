@extends('template')

@section('main')
	<div id="keperluan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Kategori Layanan</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'keperluan', 'files' => true]) !!}

		@include('keperluan.form', ['submitButtonText' => 'Tambah Kategori Layanan'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop