@extends('template')

@section('main')
	<div id="produktoko" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Produk Toko</h4></b></div>
		<div class="panel-body">
		{!! Form::model($produktoko, ['method' => 'PATCH', 'action' => ['ProduktokowebController@update', $produktoko->id],'files'=>true]) !!}

		@include('produktoko.form', ['submitButtonText' => 'Update Produk Toko'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop