@extends('template')

@section('main')
	<div id="berita" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Berita</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'berita', 'files' => true]) !!}

		@include('berita.form', ['submitButtonText' => 'Tambah Berita'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop