@extends('template')

@section('main')
	<div id="berita" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Berita</h4></b></div>
		<div class="panel-body">
		{!! Form::model($berita, ['method' => 'PATCH', 'action' => ['BeritawebController@update', $berita->id],'files'=>true]) !!}

		@include('berita.form', ['submitButtonText' => 'Update Berita'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop