@if (isset($berita))
{!! Form::hidden('id', $berita->id) !!}
@endif

{!! Form::hidden('id_profiledesa', '1') !!}
{{-- Tahap --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tahap') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tahap','Tahap',['class' => 'control-label']) !!}
	{!! Form::text('tahap', null,['class' => 'form-control']) !!}
	@if ($errors->has('tahap'))
	<span class="help-block">{{ $errors->first('tahap') }}</span>
	@endif
</div>

{{-- Tanggal Mulai --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggalmulai') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggalmulai','Tanggal Mulai',['class' => 'control-label']) !!}
	{!! Form::date('tanggalmulai', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggalmulai'))
	<span class="help-block">{{ $errors->first('tanggalmulai') }}</span>
	@endif
</div>

{{-- Tanggal Selesai --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggalselesai') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggalselesai','Tanggal Selesai',['class' => 'control-label']) !!}
	{!! Form::date('tanggalselesai', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggalselesai'))
	<span class="help-block">{{ $errors->first('tanggalselesai') }}</span>
	@endif
</div>


{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>