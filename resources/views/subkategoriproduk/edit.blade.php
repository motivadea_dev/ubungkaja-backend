@extends('template')

@section('main')
	<div id="subkategoriproduk" class="panel panel-default">
		<div class="panel-heading"><b><h4>Sub Kategori Produk</h4></b></div>
		<div class="panel-body">
		{!! Form::model($subkategoriproduk, ['method' => 'PATCH', 'action' => ['SubKategoriprodukwebController@update', $subkategoriproduk->id],'files'=>true]) !!}

		@include('subkategoriproduk.form', ['submitButtonText' => 'Update Sub Kategori'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop