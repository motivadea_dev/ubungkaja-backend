@extends('template')

@section('main')
	<div id="profiledesa" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Profile Desa</h4></b></div>
		<div class="panel-body">
		{!! Form::model($profiledesa, ['method' => 'PATCH', 'action' => ['ProfiledesawebController@update', $profiledesa->id],'files'=>true]) !!}

		@include('profiledesa.form', ['submitButtonText' => 'Update Profile Desa'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop