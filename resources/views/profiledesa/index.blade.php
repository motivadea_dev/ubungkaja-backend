@extends('template')

@section('main')
<div id="profiledesa" class="panel panel-default">
	<div class="panel-heading"><b><h4>DESA</h4></b></div>
	<div class="panel-body">
	<!--@include('_partial.flash_message')
	@include('produk.form_pencarian')-->
	<div class="tombol-nav">
		{{ link_to('profiledesa/create','Tambah Profile Desa',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarprofiledesa) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Desa</th>
				<th>Kecamatan</th>
				<th>Kabupaten</th>
				<th>Proinsi</th>
				<th>Nomer Induk Penduduk</th>
				<th>Kepala Desa</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarprofiledesa as $profiledesa): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $profiledesa->nama }}</td>
				<td>{{ $profiledesa->kecamatan  }}</td>
				<td>{{ $profiledesa->kabupaten  }}</td>
				<td>{{ $profiledesa->provinsi  }}</td>
				<td>{{ $profiledesa->nip  }}</td>
				<td>{{ $profiledesa->nama_kades  }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('profiledesa/' . $profiledesa->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['ProfiledesawebController@destroy',$profiledesa->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Desa.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Desa : {{ $jumlahprofiledesa }}</strong>
	</div>
	<div class="paging">
	{{ $daftarprofiledesa->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop