@extends('template')

@section('main')
	<div id="profiledesa" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Profile Desa</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'profiledesa', 'files' => true]) !!}

		@include('profiledesa.form', ['submitButtonText' => 'Tambah Profile Desa'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop