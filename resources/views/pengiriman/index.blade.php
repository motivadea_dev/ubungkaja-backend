@extends('template')

@section('main')
<div id="pengiriman" class="panel panel-default">
	<div class="panel-heading"><b><h4>Pengiriman</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<br><br><br>
	@if (count($daftarpengiriman) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Toko</th>
				<th>Nama Kurir</th>
				<th>Status</th>
				<th>Komisi</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarpengiriman as $pengiriman): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $pengiriman->transaksipembelian->toko->namatoko }}</td>
				<td>{{ $pengiriman->kurir->warga->nama }}</td>
				<td>{{ $pengiriman->status }}</td>
				<td>{{ $pengiriman->komisi }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['PengirimanwebController@destroy',$pengiriman->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('pengiriman/print/' . $pengiriman->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak Ada Transaksi Penjualan</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Transaksi : {{ $jumlahpengiriman }}</strong>
	</div>
	<div class="paging">
	{{ $daftarpengiriman->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop