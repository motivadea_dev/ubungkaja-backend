@extends('template')

@section('main')
	<div id="pengiriman" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Pengiriman</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'pengiriman', 'files' => true]) !!}

		@include('pengiriman.form', ['submitButtonText' => 'Tambah Transaksi Pengiriman])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop