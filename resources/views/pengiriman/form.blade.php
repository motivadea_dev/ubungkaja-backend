@if (isset($pengiriman))
{!! Form::hidden('id', $pengiriman->id) !!}
@endif

{{--  Nama Toko --}}
<div class="form-group">
	{!! Form::label('id_transaksipembelian','Id Trasaksi',['class' => 'control-label']) !!}
	@if(count($daftartoko) > 0)
	{!! Form::select('id_transaksipembelian', $daftartoko, null,['class' => 'form-control', 'id'=>'id_transaksipembelian','placeholder'=>'Pilih Toko']) !!}
	@else
	<p>Tidak ada pilihan nama Toko,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_transaksipembelian'))
	<span class="help-block">{{ $errors->first('id_transaksipembelian') }}</span>
	@endif
</div>

{{-- Total Diskon --}}
@if($errors->any())
<div class="form-group {{ $errors->has('totaldiskon') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('totaldiskon','Total Diskon',['class' => 'control-label']) !!}
	{!! Form::text('totaldiskon', null,['class' => 'form-control']) !!}
	@if ($errors->has('totaldiskon'))
	<span class="help-block">{{ $errors->first('totaldiskon') }}</span>
	@endif
</div>

{{-- Total Belanja --}}
@if($errors->any())
<div class="form-group {{ $errors->has('totalbelanja') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('totalbelanja','Total Belanja',['class' => 'control-label']) !!}
	{!! Form::text('totalbelanja', null,['class' => 'form-control']) !!}
	@if ($errors->has('totalbelanja'))
	<span class="help-block">{{ $errors->first('totalbelanja') }}</span>
	@endif
</div>

{{-- Sub Total Belanja --}}
@if($errors->any())
<div class="form-group {{ $errors->has('subtotal') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('subtotal','Sub Total',['class' => 'control-label']) !!}
	{!! Form::text('subtotal', null,['class' => 'form-control']) !!}
	@if ($errors->has('subtotal'))
	<span class="help-block">{{ $errors->first('subtotal') }}</span>
	@endif
</div>

{{-- Status --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status','Status',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status','order') !!} Order
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','kirim') !!} Kirim
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','selesai') !!} Selesai
	</label>
	</div>
	@if ($errors->has('status'))
	<span class="help-block">{{ $errors->first('status') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>