@extends('template')

@section('main')
	<div id="pengiriman" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Pengiriman</h4></b></div>
		<div class="panel-body">
		{!! Form::model($pengiriman, ['method' => 'PATCH', 'action' => ['PengirimanwebController@update', $pengiriman->id],'files'=>true]) !!}

		@include('pengiriman.form', ['submitButtonText' => 'Tambah Pengiriman'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop