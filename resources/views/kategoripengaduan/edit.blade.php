@extends('template')

@section('main')
	<div id="kategoripengaduan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Kategori</h4></b></div>
		<div class="panel-body">
		{!! Form::model($kategoripengaduan, ['method' => 'PATCH', 'action' => ['KategoriPengaduanwebController@update', $kategoripengaduan->id],'files'=>true]) !!}

		@include('kategoripengaduan.form', ['submitButtonText' => 'Update Kategori'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop