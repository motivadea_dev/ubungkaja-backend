@extends('template')

@section('main')
	<div id="kategoripengaduan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Kategori</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'kategoripengaduan', 'files' => true]) !!}

		@include('kategoripengaduan.form', ['submitButtonText' => 'Tambah Kategori'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop