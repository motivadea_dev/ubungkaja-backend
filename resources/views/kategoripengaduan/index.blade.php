@extends('template')

@section('main')
<div id="kategoripengaduan" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Kategori Pengaduan</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<!--@include('produk.form_pencarian')-->
	<div class="tombol-nav">
		{{ link_to('kategoripengaduan/create','Tambah Kategori',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarkategoripengaduan) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Kategori</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarkategoripengaduan as $kategoripengaduan): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $kategoripengaduan-> namakategori }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('kategoripengaduan/' . $kategoripengaduan->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['KategoriPengaduanwebController@destroy',$kategoripengaduan->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Katagori.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Kategori : {{ $jumlahkategoripengaduan }}</strong>
	</div>
	<div class="paging">
	{{ $daftarkategoripengaduan->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop