@if (isset($kategoripengaduan))
{!! Form::hidden('id', $kategoripengaduan->id) !!}
@endif

{!! Form::hidden('id_profiledesa', '1') !!}
{{-- Nama Kategori --}}
@if($errors->any())
<div class="form-group {{ $errors->has('namakategori') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('namakategori','Nama Kategori',['class' => 'control-label']) !!}
	{!! Form::text('namakategori', null,['class' => 'form-control']) !!}
	@if ($errors->has('namakategori'))
	<span class="help-block">{{ $errors->first('namakategori') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>