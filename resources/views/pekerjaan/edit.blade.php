@extends('template')

@section('main')
	<div id="pekerjaan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Pekerjaan</h4></b></div>
		<div class="panel-body">
		{!! Form::model($pekerjaan, ['method' => 'PATCH', 'action' => ['PekerjaanwebController@update', $pekerjaan->id],'files'=>true]) !!}

		@include('pekerjaan.form', ['submitButtonText' => 'Update Pekerjaan'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop