@extends('template')

@section('main')
	<div id="pekerjaan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Pekerjaan</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'pekerjaan', 'files' => true]) !!}

		@include('pekerjaan.form', ['submitButtonText' => 'Tambah Pekerjaan'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop