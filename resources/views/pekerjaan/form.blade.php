@if (isset($pekerjaan))
{!! Form::hidden('id', $pekerjaan->id) !!}
@endif

{{-- Nama Pekerjaan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('namapekerjaan') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('namapekerjaan','Nama Pekerjaan',['class' => 'control-label']) !!}
	{!! Form::text('namapekerjaan', null,['class' => 'form-control']) !!}
	@if ($errors->has('namapekerjaan'))
	<span class="help-block">{{ $errors->first('namapekerjaan') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>