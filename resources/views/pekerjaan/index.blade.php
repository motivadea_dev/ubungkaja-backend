@extends('template')

@section('main')
<div id="pekerjaan" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Pekerjaan</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('pekerjaan/create','Tambah Pekerjaan',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarpekerjaan) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Pekerjaan</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarpekerjaan as $pekerjaan): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $pekerjaan-> namapekerjaan }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('pekerjaan/' . $pekerjaan->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['PekerjaanwebController@destroy',$pekerjaan->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Pekerjaan.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Pekerjaan : {{ $jumlahpekerjaan }}</strong>
	</div>
	<div class="paging">
	{{ $daftarpekerjaan->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop