@extends('template')

@section('main')
	<div id="keluarga" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Anggota Keluarga</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'keluarga/anggota', 'files' => true]) !!}

		@include('keluarga.formanggota', ['submitButtonText' => 'Tambah Anggota'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop