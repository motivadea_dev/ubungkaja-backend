@extends('template')
@section('main')
<div id="pengaduan" class="panel panel-default">
	<div class="panel-heading"><b><h4>Pengaduan Warga</h4></b></div>
	<div class="panel-body">
		<table class="table table-striped">
		<tr><th>Tanggal Pengaduan</th><td>{{ $pengaduan->tanggal }}
		</td></tr>
		<tr><th>Nama Warga</th><td>{{ $pengaduan->warga->nama }}
		</td></tr>
		<tr><th>Judul Pengaduan</th><td>{{ $pengaduan->judul }}</td></tr>
		<tr><th>Kategori</th><td>{{ $pengaduan->kategoripengaduan->namakategori }}
		</td></tr>
		<tr><th>Lokasi</th>
		<td>
		<div style="width: 400px; height: 400px;">
			{!! Mapper::render() !!}
		</div>
		</td></tr>
		<!--<tr><th>Foto</th><td><img width="200" height="200" src="{{ asset('fotoupload/' . $pengaduan->foto) }}">
		</td></tr>-->

		<tr><th>Deskripsi</th><td>{{ $pengaduan->deskripsi }}
		</td></tr>

		<tr><th>Privasi Pengaduan</th><td>
			@if( $pengaduan->status == 1 )
			Public
			@elseif( $pengaduan->status == 2 )
			Priivat
			@endif
		</td></tr>

		<tr><th>Privasi Identitas</th><td>
			@if( $pengaduan->status == 1 )
			Pubik
			@elseif( $pengaduan->status == 2 )
			Privat
			@endif
		</td></tr>

		<tr><th>Status</th><td>
			@if( $pengaduan->status == 1 )
			Awal
			@elseif( $pengaduan->status == 2 )
			Progres
			@elseif( $pengaduan->status == 3 )
			Selesai
			@endif
		</td></tr>
		
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop