@if (isset($keluarga))
{!! Form::hidden('id', $keluarga->id) !!}
@endif

{{-- Tanggal --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggal') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggal','Tanggal',['class' => 'control-label']) !!}
	{!! Form::date('tanggal', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggal'))
	<span class="help-block">{{ $errors->first('tanggal') }}</span>
	@endif
</div>

{{-- NO.KK --}}
@if($errors->any())
<div class="form-group {{ $errors->has('nokk') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('nokk','No.Kartu Keluarga',['class' => 'control-label']) !!}
	{!! Form::text('nokk', null,['class' => 'form-control']) !!}
	@if ($errors->has('nokk'))
	<span class="help-block">{{ $errors->first('nokk') }}</span>
	@endif
</div>
{!! Form::hidden('id_profiledesa', '1') !!}
{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>