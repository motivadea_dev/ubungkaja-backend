@extends('template')

@section('main')
<div id="keluarga" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Keluarga</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('keluarga/create','Tambah Keluarga',['class' => 'btn btn-primary']) }}
		&nbsp;
		{{ link_to('keluarga/printsemua','Cetak Semua',['class' => 'btn btn-success']) }}
	</div><br><br><br>
	@if (count($daftarkeluarga) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Tanggal</th>
				<th>No.Kartu Keluarga</th>
				<th>Desa</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarkeluarga as $keluarga): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $keluarga->tanggal }}</td>
				<td>{{ $keluarga->nokk }}</td>
				<td>{{ $keluarga->profiledesa->nama }}</td>
				<td>
					<div class="box-button"> 
					{{ link_to('keluarga/anggota/' . $keluarga->id,'Anggota Keluarga',['class' => 'btn btn-primary btn-sm']) }}</div>
					<div class="box-button">
					{{ link_to('keluarga/' . $keluarga->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['KeluargawebController@destroy',$keluarga->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('keluarga/print/' . $keluarga->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Keluarga.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Keluarga : {{ $jumlahkeluarga }}</strong>
	</div>
	<div class="paging">
	{{ $daftarkeluarga->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop