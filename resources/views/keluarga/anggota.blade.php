@extends('template')

@section('main')
<div id="keluarga" class="panel panel-default">
	<div class="panel-heading"><b><h4>Anggota Keluarga</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('keluarga/createanggota/'. $keluarga->id,'Tambah Anggota Keluarga',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftaranggota) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Warga</th>
                <th>Hubungan Keluarga</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftaranggota as $anggota): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $anggota->warga->nama }}</td>
				<td>{{ $anggota->hubungan }}</td>
				<td>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['KeluargawebController@destroyanggota',$anggota->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Anggota</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Anggota : {{ $jumlahanggota }}</strong>
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop