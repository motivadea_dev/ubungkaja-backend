{!! Form::hidden('id_keluarga', $keluarga->id) !!}
{{--  Warga --}}
<div class="form-group">
	{!! Form::label('id_warga','Nama Anggota Keluarga',['class' => 'control-label']) !!}
	@if(count($daftarwarga) > 0)
	{!! Form::select('id_warga', $daftarwarga, null,['class' => 'form-control', 'id'=>'id_warga','placeholder'=>'Pilih Nama Warga']) !!}
	@else
	<p>Tidak ada pilihan nama,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_warga'))
	<span class="help-block">{{ $errors->first('id_warga') }}</span>
	@endif
</div>
{{-- Hubungan --}}
<div class="form-group">
{!! Form::label('hubungan','Status Dalam Keluarga',['class' => 'control-label']) !!}
<select name="hubungan" class="form-control" placeholder="Pilih Status">
<option value="kepalakeluarga">Kepala Keluarga</option>
<option value="istri">Istri</option>
<option value="anak">Anak</option>
<option value="bapak">Bapak</option>
<option value="kepalakeluarga">Ibu</option>
</select>
	@if ($errors->has('hubungan'))
	<span class="help-block">{{ $errors->first('hubungan') }}</span>
	@endif
</div>
{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>