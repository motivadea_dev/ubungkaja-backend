@extends('template')

@section('main')
	<div id="keluarga" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Keluarga</h4></b></div>
		<div class="panel-body">
		{!! Form::model($keluarga, ['method' => 'PATCH', 'action' => ['KeluargawebController@update', $keluarga->id],'files'=>true]) !!}

		@include('keluarga.form', ['submitButtonText' => 'Update Keluarga'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop