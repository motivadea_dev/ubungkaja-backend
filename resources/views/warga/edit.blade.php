@extends('template')

@section('main')
	<div id="warga" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Warga</h4></b></div>
		<div class="panel-body">
		{!! Form::model($warga, ['method' => 'PATCH', 'action' => ['WargawebController@update', $warga->id],'files'=>true]) !!}

		@include('warga.form', ['submitButtonText' => 'Update Warga'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop