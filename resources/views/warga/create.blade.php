@extends('template')

@section('main')
	<div id="warga" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Warga</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'warga', 'files' => true]) !!}

		@include('warga.form', ['submitButtonText' => 'Tambah Warga'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop