@extends('template')

@section('main')
<div id="pengaduan" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Pengaduan</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('pengaduan/create','Tambah Pengaduan',['class' => 'btn btn-primary']) }}
		&nbsp;
		{{ link_to('pengaduan/printsemua','Cetak Semua',['class' => 'btn btn-success']) }}
	</div><br><br><br>
	@if (count($daftarpengaduan) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Tanggal</th>
				<th>Nama Warga</th>
				<th>Kategori Pengaduan</th>
				<th>Dusun</th>
				<th>Judul</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarpengaduan as $pengaduan): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $pengaduan->tanggal }}</td>
				<td>{{ $pengaduan->warga->nama }}</td>
				<td>{{ $pengaduan->kategoripengaduan->namakategori }}</td>
				<td>{{ $pengaduan->dusun->nama }}</td>
				<td>{{ $pengaduan->judul }}</td>
				@if($pengaduan->status == 1)
				<td>Laporan Awal</td>
				@elseif($pengaduan->status == 2)
				<td>Progres</td>
				@elseif($pengaduan->status == 3)
				<td>Selesai</td>
				@endif
				<td>
					<div class="box-button"> 
					{{ link_to('pengaduan/komentar/' . $pengaduan->id,'Komentar',['class' => 'btn btn-primary btn-sm']) }}</div>
					<div class="box-button"> 
					{{ link_to('pengaduan/tindakan/' . $pengaduan->id,'Tindak Lanjut',['class' => 'btn btn-warning btn-sm']) }}</div>
					<div class="box-button"> 
					{{ link_to('pengaduan/' . $pengaduan->id,'Detail',['class' => 'btn btn-success btn-sm']) }}</div>
					<div class="box-button">
					{{ link_to('pengaduan/' . $pengaduan->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['PengaduanwebController@destroy',$pengaduan->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('pengaduan/print/' . $pengaduan->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Pengaduan.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Warga : {{ $jumlahpengaduan }}</strong>
	</div>
	<div class="paging">
	{{ $daftarpengaduan->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop