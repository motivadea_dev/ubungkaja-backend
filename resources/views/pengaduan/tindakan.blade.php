@extends('template')

@section('main')
<div id="tindakan" class="panel panel-default">
	<div class="panel-heading"><b><h4>Tindakan</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	@if (count($daftartindakan) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
                <th>Judul</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftartindakan as $tindakan): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
                <td>{{ $tindakan->judul }}</td>
				<td>
					<!--<div class="box-button"> 
					{{ link_to('tindakan/' . $tindakan->id,'Detail',['class' => 'btn btn-success btn-sm']) }}</div>-->
					<div class="box-button"> 
					{{ link_to('tindakan/balas/' . $tindakan->id,'Balas',['class' => 'btn btn-warning btn-sm']) }}</div>
                    <div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['PengaduanwebController@destroytindakan',$tindakan->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Komentar</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Komentar : {{ $jumlahtindakan }}</strong>
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop