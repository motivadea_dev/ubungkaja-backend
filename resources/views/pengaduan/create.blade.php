@extends('template')

@section('main')
	<div id="pengaduan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Pengaduan</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'pengaduan', 'files' => true]) !!}

		@include('pengaduan.form', ['submitButtonText' => 'Tambah Pengaduan'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop