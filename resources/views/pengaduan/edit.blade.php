@extends('template')

@section('main')
	<div id="pengaduan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Pengaduan</h4></b></div>
		<div class="panel-body">
		{!! Form::model($pengaduan, ['method' => 'PATCH', 'action' => ['PengaduanwebController@update', $pengaduan->id],'files'=>true]) !!}

		@include('pengaduan.form', ['submitButtonText' => 'Update Pengaduan'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop