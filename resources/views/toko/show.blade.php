@extends('template')
@section('main')
<div id="toko" class="panel panel-default">
	<div class="panel-heading"><b><h4>Nama Toko</h4></b></div>
	<div class="panel-body">
		<table class="table table-striped">
		<tr><th>Nama Warga</th><td>{{ $toko->warga->nama }}
		</td></tr>
		<tr><th>Kode Toko</th><td>{{ $toko->kodetoko }}
		</td></tr>
		<tr><th>Nama Toko</th><td>{{ $toko->namatoko }}</td></tr>
		<tr><th>Alamat</th><td>{{ $toko->alamat }}
		</td></tr>
		<tr><th>Foto</th><td><img width="200" height="200" src="{{ asset('fotoupload/' . $toko->foto) }}">
		</td></tr>
		<tr><th>Status</th><td>
			@if( $toko->status == 'buka' )
			Buka
			@elseif( $toko->status == 'tutup' )
			Tutup
			@endif
		</td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop