@extends('template')

@section('main')
	<div id="toko" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Toko</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'toko', 'files' => true]) !!}

		@include('toko.form', ['submitButtonText' => 'Tambah Toko'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop