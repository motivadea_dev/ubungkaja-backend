@extends('template')

@section('main')
	<div id="toko" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Toko</h4></b></div>
		<div class="panel-body">
		{!! Form::model($toko, ['method' => 'PATCH', 'action' => ['TokowebController@update', $toko->id],'files'=>true]) !!}

		@include('toko.form', ['submitButtonText' => 'Update Toko'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop