@if (isset($toko))
{!! Form::hidden('id', $toko->id) !!}
@endif

{{--  Warga --}}
<div class="form-group">
	{!! Form::label('id_warga','Warga',['class' => 'control-label']) !!}
	@if(count($daftarwarga) > 0)
	{!! Form::select('id_warga', $daftarwarga, null,['class' => 'form-control', 'id'=>'id_warga','placeholder'=>'Pilih Warga']) !!}
	@else
	<p>Tidak ada pilihan Warga,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_warga'))
	<span class="help-block">{{ $errors->first('id_warga') }}</span>
	@endif
</div>

{{-- Toko --}}
@if($errors->any())
<div class="form-group {{ $errors->has('kodetoko') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('kodetoko','Kode Toko',['class' => 'control-label']) !!}
	{!! Form::text('kodetoko', null,['class' => 'form-control']) !!}
	@if ($errors->has('kodetoko'))
	<span class="help-block">{{ $errors->first('kodetoko') }}</span>
	@endif
</div>

{{-- Nama Toko --}}
@if($errors->any())
<div class="form-group {{ $errors->has('namatoko') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('namatoko','Nama Toko',['class' => 'control-label']) !!}
	{!! Form::text('namatoko', null,['class' => 'form-control']) !!}
	@if ($errors->has('namatoko'))
	<span class="help-block">{{ $errors->first('namatoko') }}</span>
	@endif
</div>

{{-- Alamat --}}
@if($errors->any())
<div class="form-group {{ $errors->has('alamat') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('alamat','Alamat Toko',['class' => 'control-label']) !!}
	{!! Form::text('alamat', null,['class' => 'form-control']) !!}
	@if ($errors->has('alamat'))
	<span class="help-block">{{ $errors->first('alamat') }}</span>
	@endif
</div>

<div class="col-md-12">
	{{-- Foto --}}
	@if($errors->any())
	<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}"></div>
	@else
	<div class="form-group">
	@endif
		{!! Form::label('foto','Foto') !!}
		{!! Form::file('foto') !!}
		@if ($errors->has('foto'))
		<span class="help-block">{{ $errors->first('foto') }}</span>
		@endif
	</div>
  </div>

{{-- Status Toko --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status','Status Toko',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status','buka') !!} Toko Buka
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','tutup') !!} Toko Tutup
	</label>
	</div>
	@if ($errors->has('status'))
	<span class="help-block">{{ $errors->first('status') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>