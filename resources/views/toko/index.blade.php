@extends('template')

@section('main')
<div id="toko" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Toko</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('toko/create','Tambah Toko',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftartoko) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Warga</th>
				<th>Kode Toko</th>
				<th>Nama Toko</th>
				<th>Alamat</th>
				<th>Status Toko</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftartoko as $toko): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $toko->warga->nama }}</td>
				<td>{{ $toko->kodetoko }}</td>
				<td>{{ $toko->namatoko }}</td>
				<td>{{ $toko->alamat }}</td>
				<td>{{ $toko->status }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('toko/' . $toko->id,'Detail',['class' => 'btn btn-success btn-sm']) }}</div> 
					<div class="box-button">
					{{ link_to('toko/' . $toko->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['TokowebController@destroy',$toko->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('toko/print/' . $toko->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Nama Toko</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Toko : {{ $jumlahtoko }}</strong>
	</div>
	<div class="paging">
	{{ $daftartoko->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop