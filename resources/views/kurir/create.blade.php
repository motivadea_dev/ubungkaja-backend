@extends('template')

@section('main')
	<div id="kurir" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Kurir</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'kurir', 'files' => true]) !!}

		@include('kurir.form', ['submitButtonText' => 'Tambah Kurir'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop