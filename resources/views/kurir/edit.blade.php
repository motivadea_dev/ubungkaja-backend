@extends('template')

@section('main')
	<div id="kurir" class="panel panel-default">
		<div class="panel-heading"><b><h4>Kurir</h4></b></div>
		<div class="panel-body">
		{!! Form::model($kurir, ['method' => 'PATCH', 'action' => ['KurirwebController@update', $kurir->id],'files'=>true]) !!}

		@include('kurir.form', ['submitButtonText' => 'Kurir'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop