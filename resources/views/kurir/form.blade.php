@if (isset($kurir))
{!! Form::hidden('id', $kurir->id) !!}
@endif

{{-- No Kurir --}}
@if($errors->any())
<div class="form-group {{ $errors->has('nomor_kurir') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('nomor_kurir','No Kurir',['class' => 'control-label']) !!}
	{!! Form::text('nomor_kurir', null,['class' => 'form-control']) !!}
	@if ($errors->has('nomor_kurir'))
	<span class="help-block">{{ $errors->first('nomor_kurir') }}</span>
	@endif
</div>

{{--  Nama Warga --}}
<div class="form-group">
	{!! Form::label('id_warga','Warga',['class' => 'control-label']) !!}
	@if(count($daftarwarga) > 0)
	{!! Form::select('id_warga', $daftarwarga, null,['class' => 'form-control', 'id'=>'id_warga','placeholder'=>'Pilih Warga']) !!}
	@else
	<p>Tidak ada pilihan Warga,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_warga'))
	<span class="help-block">{{ $errors->first('id_warga') }}</span>
	@endif
</div>

{{-- Jenis Kendaraan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('jenis_kendaraan') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('jenis_kendaraan','Jenis Kendaraan',['class' => 'control-label']) !!}
	{!! Form::text('jenis_kendaraan', null,['class' => 'form-control']) !!}
	@if ($errors->has('jenis_kendaraan'))
	<span class="help-block">{{ $errors->first('jenis_kendaraan') }}</span>
	@endif
</div>

{{-- Plat Nomor --}}
@if($errors->any())
<div class="form-group {{ $errors->has('plat_nomor') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('plat_nomor','Plat Nomor',['class' => 'control-label']) !!}
	{!! Form::text('plat_nomor', null,['class' => 'form-control']) !!}
	@if ($errors->has('plat_nomor'))
	<span class="help-block">{{ $errors->first('plat_nomor') }}</span>
	@endif
</div>

{{-- Status --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status','Status',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status','aktif') !!} Aktif
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','nonaktif') !!} Non Aktif
	</label>
	</div>
	@if ($errors->has('status'))
	<span class="help-block">{{ $errors->first('status') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>