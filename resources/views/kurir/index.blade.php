@extends('template')

@section('main')
<div id="kurir" class="panel panel-default">
	<div class="panel-heading"><b><h4>Kurir</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
	{{ link_to('kurir/create','Tambah Kurir',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarkurir) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>No Kurir</th>
				<th>Nama Kurir</th>
				<th>Jenis Kendaraan</th>
				<th>Plat Nomor</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarkurir as $kurir): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $kurir->nomor_kurir }}</td>
				<td>{{ $kurir->warga->nama }}</td>
				<td>{{ $kurir->jenis_kendaraan }}</td>
				<td>{{ $kurir->plat_nomor }}</td>
				<td>{{ $kurir->status }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('kurir/' . $kurir->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['KurirwebController@destroy',$kurir->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('kurir/print/' . $kurir->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak Ada Kurir</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Kurir : {{ $jumlahkurir }}</strong>
	</div>
	<div class="paging">
	{{ $daftarkurir->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop