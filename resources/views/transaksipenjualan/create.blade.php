@extends('template')

@section('main')
	<div id="transaksipenjualan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Transaksi Penjualan</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'transaksipenjualan', 'files' => true]) !!}

		@include('transaksipenjualan.form', ['submitButtonText' => 'Tambah Transaksi Penjualan'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop