@extends('template')

@section('main')
	<div id="transaksipenjualan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Transakasi Penjualan</h4></b></div>
		<div class="panel-body">
		{!! Form::model($transaksipenjualan, ['method' => 'PATCH', 'action' => ['TransaksiPenjualanwebController@update', $transaksipenjualan->id],'files'=>true]) !!}

		@include('transaksipenjualan.form', ['submitButtonText' => 'Transakasi Penjualan'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop