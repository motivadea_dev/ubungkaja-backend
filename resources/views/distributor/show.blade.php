@extends('template')
@section('main')
<div id="distributor" class="panel panel-default">
	<div class="panel-heading"><b><h4>Nama Distributor</h4></b></div>
	<div class="panel-body">
		<table class="table table-striped">
		<tr><th>Nama Desa</th><td>{{ $distributor->profiledesa->nama }}
		</td></tr>
		<tr><th>Kode Distributor</th><td>{{ $distributor->kodedistributor }}
		</td></tr>
		<tr><th>Nama Distributor</th><td>{{ $distributor->namadistributor }}</td></tr>
		<tr><th>Alamat</th><td>{{ $distributor->alamat }}
		</td></tr>
		<tr><th>Foto</th><td><img width="200" height="200" src="{{ asset('fotoupload/' . $distributor->foto) }}">
		</td></tr>
		<tr><th>Status</th><td>
			@if( $distributor->status == 'buka' )
			Buka
			@elseif( $distributor->status == 'tutup' )
			Tutup
			@endif
		</td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop