@extends('template')

@section('main')
<div id="distributor" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Distributor</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('distributor/create','Tambah Distributor',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftardistributor) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Desa</th>
				<th>Kode Distributor</th>
				<th>Nama Distributor</th>
				<th>Alamat</th>
				<th>Status Distributor</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftardistributor as $distributor): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $distributor->profiledesa->nama }}</td>
				<td>{{ $distributor->kodedistributor }}</td>
				<td>{{ $distributor->namadistributor }}</td>
				<td>{{ $distributor->alamat }}</td>
				<td>{{ $distributor->status }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('distributor/' . $distributor->id,'Detail',['class' => 'btn btn-success btn-sm']) }}</div> 
					<div class="box-button">
					{{ link_to('distributor/' . $distributor->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['DistributorwebController@destroy',$distributor->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('distributor/print/' . $distributor->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Nama Distributor</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Distributor : {{ $jumlahdistributor }}</strong>
	</div>
	<div class="paging">
	{{ $daftardistributor->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop