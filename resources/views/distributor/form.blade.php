@if (isset($distributor))
{!! Form::hidden('id', $distributor->id) !!}
@endif

{{--  Desa --}}
<div class="form-group">
	{!! Form::label('id_profiledesa','Desa',['class' => 'control-label']) !!}
	@if(count($daftardesa) > 0)
	{!! Form::select('id_profiledesa', $daftardesa, null,['class' => 'form-control', 'id'=>'id_profiledesa','placeholder'=>'Pilih Desa']) !!}
	@else
	<p>Tidak ada pilihan Desa,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_profiledesa'))
	<span class="help-block">{{ $errors->first('id_profiledesa') }}</span>
	@endif
</div>

{{-- Distribbutor --}}
@if($errors->any())
<div class="form-group {{ $errors->has('kodedistributor') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('kodedistributor','Kode Distributor',['class' => 'control-label']) !!}
	{!! Form::text('kodedistributor', null,['class' => 'form-control']) !!}
	@if ($errors->has('kodedistributor'))
	<span class="help-block">{{ $errors->first('kodedistributor') }}</span>
	@endif
</div>

{{-- Nama Distributor --}}
@if($errors->any())
<div class="form-group {{ $errors->has('namadistributor') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('namadistributor','Nama Distributor',['class' => 'control-label']) !!}
	{!! Form::text('namadistributor', null,['class' => 'form-control']) !!}
	@if ($errors->has('namadistributor'))
	<span class="help-block">{{ $errors->first('namadistributor') }}</span>
	@endif
</div>

{{-- Alamat --}}
@if($errors->any())
<div class="form-group {{ $errors->has('alamat') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('alamat','Alamat Toko',['class' => 'control-label']) !!}
	{!! Form::text('alamat', null,['class' => 'form-control']) !!}
	@if ($errors->has('alamat'))
	<span class="help-block">{{ $errors->first('alamat') }}</span>
	@endif
</div>

<div class="col-md-12">
	{{-- Foto --}}
	@if($errors->any())
	<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}"></div>
	@else
	<div class="form-group">
	@endif
		{!! Form::label('foto','Foto') !!}
		{!! Form::file('foto') !!}
		@if ($errors->has('foto'))
		<span class="help-block">{{ $errors->first('foto') }}</span>
		@endif
	</div>
  </div>

{{-- Status Toko --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status','Status Toko',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status','buka') !!} Distributor Buka
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','tutup') !!} Distributor Tutup
	</label>
	</div>
	@if ($errors->has('status'))
	<span class="help-block">{{ $errors->first('status') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>