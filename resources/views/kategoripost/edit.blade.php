@extends('template')

@section('main')
	<div id="kategoripost" class="panel panel-default">
		<div class="panel-heading"><b><h4>Kategori Posting</h4></b></div>
		<div class="panel-body">
		{!! Form::model($kategoripost, ['method' => 'PATCH', 'action' => ['KategoripostwebController@update', $kategoripost->id],'files'=>true]) !!}

		@include('kategoripost.form', ['submitButtonText' => 'Update Kategori Posting'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop