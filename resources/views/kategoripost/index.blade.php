@extends('template')

@section('main')
<div id="kategoripost" class="panel panel-default">
	<div class="panel-heading"><b><h4>Kategori Posting</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
	{{ link_to('kategoripost/create','Tambah Kategori Posting',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarkategoripost) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Kategori</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarkategoripost as $kategoripost): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $kategoripost->namakategori }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('kategoripost/' . $kategoripost->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['KategoripostwebController@destroy',$kategoripost->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak Ada Kategori Post</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Kategori Posting : {{ $jumlahkategoripost }}</strong>
	</div>
	<div class="paging">
	{{ $daftarkategoripost->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop