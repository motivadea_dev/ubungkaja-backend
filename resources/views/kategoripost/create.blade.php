@extends('template')

@section('main')
	<div id="kategoripost" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Kategori Posting</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'kategoripost', 'files' => true]) !!}

		@include('kategoripost.form', ['submitButtonText' => 'Tambah Kategori Posting'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop