@if (isset($kategoripost))
{!! Form::hidden('id', $kategoripost->id) !!}
@endif

{{--  Kategori Posting --}}
@if($errors->any())
<div class="form-group {{ $errors->has('namakategori') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('namakategori','Nama Kategori Posting',['class' => 'control-label']) !!}
	{!! Form::text('namakategori', null,['class' => 'form-control']) !!}
	@if ($errors->has('namakategori'))
	<span class="help-block">{{ $errors->first('namakategori') }}</span>
	@endif
</div>

{{-- Foto --}}
	@if($errors->any())
	<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}"></div>
	@else
	<div class="form-group">
	@endif
		{!! Form::label('foto','Foto') !!}
		{!! Form::file('foto') !!}
		@if ($errors->has('foto'))
		<span class="help-block">{{ $errors->first('foto') }}</span>
		@endif
	</div>
  </div>



{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>