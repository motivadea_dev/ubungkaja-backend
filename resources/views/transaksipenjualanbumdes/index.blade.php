@extends('template')

@section('main')
<div id="transaksipenjualanbumdes" class="panel panel-default">
	<div class="panel-heading"><b><h4>Transaksi Penjualan Bumdes</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
	{{ link_to('transaksipenjualanbumdes/create','Tambah Transaksi Penjualan',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftartransaksipenjualan) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Tanggal</th>
				<th>Nama Toko</th>
				<th>Total Belanja</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftartransaksipenjualan as $penjualanbumdes): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $penjualanbumdes->tanggal }}</td>
				<td>{{ $penjualanbumdes->toko->namatoko }}</td>
				<td>{{ $penjualanbumdes->totalbelanja }}</td>
				<td>{{ $penjualanbumdes->status }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('transaksipenjualanbumdes/' . $penjualanbumdes->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['TransaksiPenjualanBumdeswebController@destroy',$penjualanbumdes->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('transaksipenjualanbumdes/print/' . $penjualanbumdes->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak Ada Transaksi Penjualan</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Transaksi : {{ $jumlahtransaksipenjualan }}</strong>
	</div>
	<div class="paging">
	{{ $daftartransaksipenjualan->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop