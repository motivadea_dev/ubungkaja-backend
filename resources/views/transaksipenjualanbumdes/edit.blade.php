@extends('template')

@section('main')
	<div id="transaksipenjualanbumdes" class="panel panel-default">
		<div class="panel-heading"><b><h4>Transakasi Penjualan</h4></b></div>
		<div class="panel-body">
		{!! Form::model($transaksipenjualanbumdes, ['method' => 'PATCH', 'action' => ['TransaksiPenjualanBumdeswebController@update', $transaksipenjualanbumdes->id],'files'=>true]) !!}

		@include('transaksipenjualanbumdes.form', ['submitButtonText' => 'Transakasi Penjualan'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop