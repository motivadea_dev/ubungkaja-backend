@extends('template')

@section('main')
	<div id="transaksipenjualanbumdes" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Transaksi Penjualan</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'transaksipenjualanbumdes', 'files' => true]) !!}

		@include('transaksipenjualanbumdes.form', ['submitButtonText' => 'Tambah Transaksi Penjualan'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop