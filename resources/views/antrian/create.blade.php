@extends('template')

@section('main')
	<div id="antrian" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Antrian</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'antrian', 'files' => true]) !!}

		@include('antrian.form', ['submitButtonText' => 'Tambah Antrian'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop