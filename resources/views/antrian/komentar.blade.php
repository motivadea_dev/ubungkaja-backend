@extends('template')

@section('main')
<div id="komentar" class="panel panel-default">
	<div class="panel-heading"><b><h4>Komentar</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	@if (count($daftarkomentar) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Tanggal</th>
				<th>Nama Warga</th>
                <th>Komentar</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarkomentar as $komentar): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $komentar->tanggal }}</td>
				<td>{{ $komentar->warga->nama }}</td>
                <td>{{ $komentar->komentar }}</td>
				<td>
					<!--<div class="box-button"> 
					{{ link_to('komentar/' . $komentar->id,'Detail',['class' => 'btn btn-success btn-sm']) }}</div>-->
					<div class="box-button"> 
					{{ link_to('komentar/balas/' . $komentar->id,'Balas',['class' => 'btn btn-warning btn-sm']) }}</div>
                    <div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['PengaduanwebController@destroykomentar',$komentar->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Komentar</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Komentar : {{ $jumlahkomentar }}</strong>
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop