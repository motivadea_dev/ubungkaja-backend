<div id="pencarian">
	{!! Form::open(['url' => 'usulan/cari', 'method' => 'GET', 'id' => 'form_pencarian']) !!}
<div class="row">
	<div class="col-md-3">
		<!--<div class="input-group">
			{!! Form::date('kata_kunci',(!empty($kata_kunci)) ? $kata_kunci : null,['class'=>'form-control','placeholder'=> 'Masukkan Tanggal Usulan']) !!}
			<span class="input-group-btn">
				{!! Form::button('<i class="glyphicon glyphicon-search"></i>', ['class'=>'btn btn-default','type'=>'submit']) !!}
			</span>
		</div>-->
	{{--  Dusun --}}
	<div class="form-group">
		<div class="input-group col-md-12">
		@if(count($daftardusun) > 0)
		{!! Form::select('id_dusun', $daftardusun, null,['class' => 'form-control', 'id'=>'id_dusun','placeholder'=>'Pilih Dusun']) !!}
		@else
		<p>Tidak ada pilihan dusun,silahkan buat dulu.</p>
		@endif
		</div>
	</div>
	</div>

	<div class="col-md-3">
	{{--  Kategori --}}
	<div class="form-group">
		<div class="input-group">
		@if(count($daftarkategori) > 0)
		{!! Form::select('id_kategori', $daftarkategori, null,['class' => 'form-control', 'id'=>'id_kategori','placeholder'=>'Pilih Kategori']) !!}
		@else
		<p>Tidak ada pilihan kategori,silahkan buat dulu.</p>
		@endif
		<span class="input-group-btn">
			{!! Form::button('<i class="glyphicon glyphicon-search"></i>', ['class'=>'btn btn-default','type'=>'submit']) !!}
		</span>
		</div>
	</div>
	</div>
</div>
	{!! Form::close() !!}
</div>