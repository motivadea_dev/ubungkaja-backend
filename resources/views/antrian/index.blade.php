@extends('template')

@section('main')
<div id="antrian" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Antrian</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('antrian/create','Tambah Antrian',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarantrian) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Warga</th>
				<th>Kategori Layanan</th>
				<th>Status Persyaratan</th>
				<th>Status Jadi</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarantrian as $antrian): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $antrian->warga->nama }}</td>
				<td>{{ $antrian->keperluan->nama_keperluan }}</td>
				@if($antrian->status_syarat == 1)
				<td>Belum Lengkap</td> 
				@elseif($antrian->status_syarat == 2)
				<td>Lengkap</td>
				@endif
				@if($antrian->status_jadi == 1)
				<td>Belum</td> 
				@elseif($antrian->status_jadi == 2)
				<td>Selesai</td>
				@endif
				<td>
					{{ link_to('antrian/' . $antrian->id,'Detail',['class' => 'btn btn-success btn-sm']) }}</div>
					<div class="box-button">
					{{ link_to('antrian/' . $antrian->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['AntrianwebController@destroy',$antrian->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('antrian/print/' . $antrian->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Antrian.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Warga : {{ $jumlahantrian }}</strong>
	</div>
	<div class="paging">
	{{ $daftarantrian->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop