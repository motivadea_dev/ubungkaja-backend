@if (isset($antrian))
{!! Form::hidden('id', $antrian->id) !!}
@endif

{{-- Tanggal --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggal') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggal','Tanggal',['class' => 'control-label']) !!}
	{!! Form::date('tanggal', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggal'))
	<span class="help-block">{{ $errors->first('tanggal') }}</span>
	@endif
</div>

{{--  Warga --}}
<div class="form-group">
	{!! Form::label('id_warga','Warga',['class' => 'control-label']) !!}
	@if(count($daftarwarga) > 0)
	{!! Form::select('id_warga', $daftarwarga, null,['class' => 'form-control', 'id'=>'id_warga','placeholder'=>'Pilih Warga']) !!}
	@else
	<p>Tidak ada pilihan Warga,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_warga'))
	<span class="help-block">{{ $errors->first('id_warga') }}</span>
	@endif
</div>

{{--  Kategori Layanan --}}
<div class="form-group">
	{!! Form::label('id_keperluan','Kategori Layanan',['class' => 'control-label']) !!}
	@if(count($daftarkeperluan) > 0)
	{!! Form::select('id_keperluan', $daftarkeperluan, null,['class' => 'form-control', 'id'=>'id_keperluan','placeholder'=>'Pilih Kategori']) !!}
	@else
	<p>Tidak ada pilihan kategori,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_keperluan'))
	<span class="help-block">{{ $errors->first('id_keperluan') }}</span>
	@endif
</div>

{{-- Status Berkas Persyaratan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status_syarat') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status_syarat','Status Berkas Persyaratan',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status_syarat','1') !!} Belum Lengkap
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status_syarat','2') !!} Lengkap
	</label>
	</div>
	@if ($errors->has('status_syarat'))
	<span class="help-block">{{ $errors->first('status_syarat') }}</span>
	@endif
</div>

{{-- Status Berkas Pelayanan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status_jadi') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status_jadi','Status Berkas Pelayanan',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status_jadi','1') !!} Belum Selesai
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status_jadi','2') !!} Selesai
	</label>
	</div>
	@if ($errors->has('status_jadi'))
	<span class="help-block">{{ $errors->first('status_jadi') }}</span>
	@endif
</div>


{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>