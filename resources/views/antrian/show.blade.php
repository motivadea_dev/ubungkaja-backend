@extends('template')
@section('main')
<div id="antrian" class="panel panel-default">
	<div class="panel-heading"><b><h4>Antrian Warga</h4></b></div>
	<div class="panel-body">
		<table class="table table-striped">
		<tr><th>Nama Warga</th><td>{{ $antrian->warga->nama }}
		</td></tr>
		<tr><th>Kategori Layanan</th><td>{{ $antrian->keperluan->nama_keperluan }}</td></tr>
		<tr><th>Status Syarat</th><td>
			@if( $antrian->status_syarat == 1 )
			Belum Lengkap
			@elseif( $antrian->status_syarat == 2 )
			Lengkap
			@endif
		</td></tr>

		<tr><th>Status Jadi</th><td>
			@if( $antrian->status_jadi == 1 )
			Belum
			@elseif( $antrian->status_jadi == 2 )
			Selesai
			@endif
		</td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop