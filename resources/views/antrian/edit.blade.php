@extends('template')

@section('main')
	<div id="antrian" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Antrian</h4></b></div>
		<div class="panel-body">
		{!! Form::model($antrian, ['method' => 'PATCH', 'action' => ['AntrianwebController@update', $antrian->id],'files'=>true]) !!}

		@include('antrian.form', ['submitButtonText' => 'Update Antrian'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop