@extends('template')

@section('main')
	<div id="penghasilan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Penghasilan</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'penghasilan', 'files' => true]) !!}

		@include('penghasilan.form', ['submitButtonText' => 'Tambah Penghasilan'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop