@extends('template')

@section('main')
	<div id="penghasilan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Penghasilan</h4></b></div>
		<div class="panel-body">
		{!! Form::model($penghasilan, ['method' => 'PATCH', 'action' => ['PenghasilanwebController@update', $penghasilan->id],'files'=>true]) !!}

		@include('penghasilan.form', ['submitButtonText' => 'Update Penghasilan'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop