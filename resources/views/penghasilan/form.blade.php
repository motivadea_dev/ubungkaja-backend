@if (isset($penghasilan))
{!! Form::hidden('id', $penghasilan->id) !!}
@endif

{{-- Nama Penghasilan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('namapenghasilan') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('namapenghasilan','Nama Penghasilan',['class' => 'control-label']) !!}
	{!! Form::text('namapenghasilan', null,['class' => 'form-control']) !!}
	@if ($errors->has('namapenghasilan'))
	<span class="help-block">{{ $errors->first('namapenghasilan') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>