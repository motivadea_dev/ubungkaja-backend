@extends('template')
@section('main')
<div id="produkbumdes" class="panel panel-default">
	<div class="panel-heading"><b><h4>Nama Produk</h4></b></div>
	<div class="panel-body">
	<table class="table table-striped">
		<tr><th>Kategori Produk</th><td>{{ $produkbumdes->kategoriproduk->namakategori }}
		</td></tr>
		<tr><th>Sub Kategori Produk</th><td>{{ $produkbumdes->subkategoriproduk->namasubkategori }}
		</td></tr>
		<tr><th>Kode Produk</th><td>{{ $produkbumdes->kodeproduk }}
		</td></tr>
		<tr><th>Nama Produk</th><td>{{ $produkbumdes->namaproduk }}</td></tr>
		<tr><th>Stok Barang</th><td>{{ $produkbumdes->stok }}
		</td></tr>
		<tr><th>Harga</th><td>{{ $produkbumdes->harga }}
		</td></tr>
		<tr><th>Diskon</th><td>{{ $produkbumdes->diskon }}
		</td></tr>
		<tr><th>Foto</th><td><img width="200" height="200" src="{{ asset('fotoupload/' . $produkbumdes->foto) }}">
		</td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop