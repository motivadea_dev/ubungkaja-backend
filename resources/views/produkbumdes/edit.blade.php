@extends('template')

@section('main')
	<div id="produkbumdes" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Produk</h4></b></div>
		<div class="panel-body">
		{!! Form::model($produkbumdes, ['method' => 'PATCH', 'action' => ['ProdukbumdeswebController@update', $produkbumdes->id],'files'=>true]) !!}

		@include('produkbumdes.form', ['submitButtonText' => 'Update Produk'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop