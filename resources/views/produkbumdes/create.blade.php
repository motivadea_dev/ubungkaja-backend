@extends('template')

@section('main')
	<div id="produkbumdes" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Produk</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'produkbumdes', 'files' => true]) !!}

		@include('produkbumdes.form', ['submitButtonText' => 'Tambah Produk'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop