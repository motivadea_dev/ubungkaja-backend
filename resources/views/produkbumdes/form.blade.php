@if (isset($produkbumdes))
{!! Form::hidden('id', $produkbumdes->id) !!}
@endif


{{--  Kategori Produk --}}
<div class="form-group">
	{!! Form::label('id_kategoriproduk','Kategori Produk',['class' => 'control-label']) !!}
	@if(count($daftarkategoriproduk) > 0)
	{!! Form::select('id_kategoriproduk', $daftarkategoriproduk, null,['class' => 'form-control', 'id'=>'id_kategoriproduk','placeholder'=>'Pilih Kategori Produk']) !!}
	@else
	<p>Tidak ada pilihan Kategori Produk,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_kategoriproduk'))
	<span class="help-block">{{ $errors->first('id_kategoriproduk') }}</span>
	@endif
</div>

{{--  Sub Kategori Produk --}}
<div class="form-group">
	{!! Form::label('id_subkategoriproduk','Sub Kategori Produk',['class' => 'control-label']) !!}
	@if(count($daftarsubkategoriproduk) > 0)
	{!! Form::select('id_subkategoriproduk', $daftarsubkategoriproduk, null,['class' => 'form-control', 'id'=>'id_subkategoriproduk','placeholder'=>'Pilih Sub Kategori Produk']) !!}
	@else
	<p>Tidak ada pilihan Sub Kategori Produk,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_subkategoriproduk'))
	<span class="help-block">{{ $errors->first('id_subkategoriproduk') }}</span>
	@endif
</div>
{!! Form::hidden('id_distributor', '1') !!}
{{-- Kode Produk --}}
@if($errors->any())
<div class="form-group {{ $errors->has('kodeproduk') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('kodeproduk','Kode Produk',['class' => 'control-label']) !!}
	{!! Form::text('kodeproduk', null,['class' => 'form-control']) !!}
	@if ($errors->has('kodeproduk'))
	<span class="help-block">{{ $errors->first('kodeproduk') }}</span>
	@endif
</div>

{{-- Nama Produk --}}
@if($errors->any())
<div class="form-group {{ $errors->has('namaproduk') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('namaproduk','Nama Produk',['class' => 'control-label']) !!}
	{!! Form::text('namaproduk', null,['class' => 'form-control']) !!}
	@if ($errors->has('namaproduk'))
	<span class="help-block">{{ $errors->first('namaproduk') }}</span>
	@endif
</div>

{{-- Stok Barang --}}
@if($errors->any())
<div class="form-group {{ $errors->has('stok') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('stok','Stok Barang',['class' => 'control-label']) !!}
	{!! Form::text('stok', null,['class' => 'form-control']) !!}
	@if ($errors->has('stok'))
	<span class="help-block">{{ $errors->first('stok') }}</span>
	@endif
</div>

{{-- Harga Barang --}}
@if($errors->any())
<div class="form-group {{ $errors->has('harga') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('harga','Harga Barang',['class' => 'control-label']) !!}
	{!! Form::text('harga', null,['class' => 'form-control']) !!}
	@if ($errors->has('harga'))
	<span class="help-block">{{ $errors->first('harga') }}</span>
	@endif
</div>

{{-- Diskon --}}
@if($errors->any())
<div class="form-group {{ $errors->has('diskon') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('diskon','Harga Diskon',['class' => 'control-label']) !!}
	{!! Form::text('diskon', null,['class' => 'form-control']) !!}
	@if ($errors->has('diskon'))
	<span class="help-block">{{ $errors->first('diskon') }}</span>
	@endif
</div>


<div class="col-md-12">
	{{-- Foto --}}
	@if($errors->any())
	<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}"></div>
	@else
	<div class="form-group">
	@endif
		{!! Form::label('foto','Foto') !!}
		{!! Form::file('foto') !!}
		@if ($errors->has('foto'))
		<span class="help-block">{{ $errors->first('foto') }}</span>
		@endif
	</div>
  </div>


{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>