@extends('template')

@section('main')
	<div id="usulan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Usulan</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'usulan', 'files' => true]) !!}

		@include('usulan.form', ['submitButtonText' => 'Tambah Usulan'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop