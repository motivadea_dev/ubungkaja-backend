@extends('template')

@section('main')
	<div id="usulan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Usulan</h4></b></div>
		<div class="panel-body">
		{!! Form::model($usulan, ['method' => 'PATCH', 'action' => ['UsulanwebController@update', $usulan->id],'files'=>true]) !!}

		@include('usulan.form', ['submitButtonText' => 'Update Usulan'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop