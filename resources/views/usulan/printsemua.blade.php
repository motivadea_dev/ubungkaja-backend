<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Usulan Warga</title>
        <style type="text/css">
        @page { margin: 20px; margin-top:5px;}
        </style>
    </head>
        <body>  
            <h2 align="center">DAFTAR SEMUA USULAN WARGA</h2>
            <p align="center">
            @foreach($seleksidaftarusulan as $usulan)
            <table width="100%">
            <tr><th width="100">Tanggal Usulan</th><td>: {{ $usulan->tanggal }}</td></tr>
            <tr><th width="100">Nama Warga</th><td>: {{ $usulan->warga->nama }}</td></tr>
            <tr><th width="100">Judul Usulan</th><td>: {{ $usulan->judul }}</td></tr>
            <tr><th width="100">Kategori Usulan</th><td>: {{ $usulan->kategori->namakategori }}</td></tr>
            <tr><th width="100">Volume</th><td>: {{ $usulan->volume }}</td></tr>
            <tr><th width="100">Satuan</th><td>: {{ $usulan->satuan }}</td></tr>
            <tr><th width="100">Prioritas</th><td>: 
            @if($usulan->prioritas == 1)
            Tidak
            @elseif($usulan->prioritas == 2)
            Ya
            @endif
            </td></tr>
            <tr><th width="100">Deskripsi Usulan</th><td>: {{ $usulan->deskripsi }}</td></tr>
            </table>
            <hr>
             @endforeach
        <script type="text/javascript">
          print();
        </script>
        </body>
</html>