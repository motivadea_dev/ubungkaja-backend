@extends('template')

@section('main')
<div id="hasil" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Hasil Musyawarah Per Dusun</h4></b></div>
	<div class="panel-body">
	
	@if (count($daftardusun) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Dusun</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftardusun as $dusun): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $dusun->nama }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('hasil/' . $dusun->id , 'Detail Hasil', ['class' => 'btn btn-success btn-sm']) }}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Nama Dusun.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Dusun : {{ $jumlahdusun }}</strong>
	</div>
	<div class="paging">
	{{ $daftardusun->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop