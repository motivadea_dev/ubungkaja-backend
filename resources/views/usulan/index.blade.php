@extends('template')

@section('main')
<div id="usulan" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Usulan</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	{{--@include('usulan.form_pencarian')--}}
	<div class="tombol-nav">
		<!--{{ link_to('usulan/create','Tambah Usulan',['class' => 'btn btn-primary']) }}
		&nbsp;-->
		<a href="{{ url('usulan/printsemua') }}" class="btn btn-success" target="_blank"><i class="glyphicon glyphicon-print"></i> Cetak Rekapitulasi Usulan</a>
	</div><br><br><br>
	@if (count($daftarusulan) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Tanggal</th>
				<th>Nama Warga</th>
				<th>Kategori</th>
				<th>Judul</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarusulan as $usulan): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $usulan->tanggal }}</td>
				<td>{{ $usulan->warga->nama }}</td>
				<td>{{ $usulan->kategori->namakategori }}</td>
				<td>{{ $usulan->judul }}</td>
				@if($usulan->status == 1)
				<td><span class="glyphicon glyphicon-search" style="color:orange;"></span> Usulan Baru</td>
				@elseif($usulan->status == 2)
				<td><span class="glyphicon glyphicon-ok" style="color:green;"></span> Diterima</td>
				@elseif($usulan->status == 3)
				<td><span class="glyphicon glyphicon-remove" style="color:red;"></span> Ditolak</td>
				@endif
				<td>
					<div class="box-button"> 
					@if($usulan->status == 1)
					{{ link_to('usulan/' . $usulan->id,'Verifikasi',['class' => 'btn btn-primary btn-sm']) }}
					@else
					{{ link_to('usulan/' . $usulan->id,'&nbsp;&nbsp;Detail&nbsp;&nbsp;&nbsp;',['class' => 'btn btn-success btn-sm']) }}
					@endif
					</div>
					<div class="box-button">
					{{ link_to('usulan/' . $usulan->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['UsulanwebController@destroy',$usulan->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<!--<div class="box-button"> 
					{{ link_to('usulan/print/' . $usulan->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>-->
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada usulan.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Warga : {{ $jumlahusulan }}</strong>
	</div>
	<div class="paging">
	{{ $daftarusulan->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop