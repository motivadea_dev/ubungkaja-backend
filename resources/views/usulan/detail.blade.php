@extends('template')

@section('main')
<div id="hasil" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Hasil Musyawarah Per Dusun</h4></b></div>
	<div class="panel-body">
	<div class="tombol-nav">
	<a href="{{ url('hasil/printsemua/'.$dusun->id) }}" class="btn btn-success btn-md" target="_blank"><i class="glyphicon glyphicon-print"></i> Cetak Daftar Usulan</a>
	</div><br><br><br>
	@if (count($daftarusulan) > 0)
	<table class="table">
		<thead>
			<tr>
            <th>Nama Warga</th>
            <th>Kategori</th>
            <th>Judul</th>
			<th>Lokasi</th>
			<th>Volume</th>
			<th>Satuan</th>
			<th>Prioritas</th>
			<td align="center"><b>Urutan/Rangking</b></td>
			</tr>
		</thead>
		<tbody>
			
			<?php 
			$i=0; 
			$len = count($daftarusulan);
			?>	
                @foreach($daftarusulan as $usulan)
                	@if($usulan->warga->id_dusun == $dusun->id)
				<tr>
					<td>{{ $usulan->warga->nama }}</td>
					<td>{{ $usulan->kategori->namakategori }}</td>
					<td>{{ $usulan->judul }}</td>
					<td>
					<div class="box-button"> 
					<a href="{{ url('usulan/'.$usulan->id) }}" class="btn btn-success btn-md"><i class="glyphicon glyphicon-map-marker"></i> View</a>
					</div>
					</td>
					<td>{{ $usulan->volume }}</td>
					<td>{{ $usulan->satuan }}</td>
					@if($usulan->prioritas == 1)
					<td>Tidak</td>
					@else
					<td>Ya</td>
					@endif

					<!-- INISIALISASI VALUE RANGKING NEXT & PREV -->
					<td align="center">
					@if($i == 0)
						<div class="box-button">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
						</div>
						<div class="box-button"> 
						<a href="{{ url('usulanturun/'.$usulan->id) }}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down"></i></a>
						</div>
					@elseif($i == $len - 1)
						<div class="box-button"> 
						<a href="{{ url('usulannaik/'.$usulan->id) }}" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-arrow-up"></i></a>
						</div>
						<div class="box-button">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						</div>
					@else
						<div class="box-button"> 
						<a href="{{ url('usulannaik/'.$usulan->id) }}" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-arrow-up"></i></a>
						</div>
						<div class="box-button"> 
						<a href="{{ url('usulanturun/'.$usulan->id) }}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down"></i></a>
						</div>
					@endif
					</td>
				</tr>
					<?php $i++; ?>
                	@endif
            	@endforeach
		</tbody>
	</table>
	@else
	<p>Tidak ada Usulan.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Usulan : {{ $i }}</strong>
	</div>
	<div class="paging">
	{{ $daftarusulan->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop