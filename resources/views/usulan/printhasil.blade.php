<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Usulan Warga</title>
        <style type="text/css">
        @page { margin: 20px; margin-top:5px;}
        </style>
    </head>
        <body>  
            <h3 align="center">DAFTAR GAGASAN DUSUN/KELOMPOK {{ $dusun->nama }}</h3>
            <table>
            <tr><td><b>DESA</td><td>: {{ $desa->nama }}</td></tr>
            <tr><td><b>KECAMATAN</td><td>: {{ $desa->kecamatan }}</td></tr>
            <tr><td><b>KOTA/KABUPATEN</td><td>: {{ $desa->kabupaten }}</td></tr>
            <tr><td><b>PROVINSI</td><td>: {{ $desa->provinsi }}</td></tr>
            </table><br>
            <table width="100%" border="1">
            <tr><td align="center" rowspan="2"><b>No</td><td align="center" rowspan="2"><b>Gagasan Kegiatan</td><td align="center" rowspan="2"><b>Lokasi</td><td align="center" rowspan="2"><b>Volume</td><td align="center" rowspan="2"><b>Satuan</td><td align="center" colspan="3"><b>Penerima Manfaat</td></tr>
            <tr><td align="center"><b>LK</td><td align="center"><b>PR</td><td align="center"><b>A-RTM</td></tr>
            <?php $i=1; ?>
            @foreach($daftarusulan as $usulan)
                @if($usulan->warga->id_dusun == $dusun->id)
                <tr>
                <td align="center">{{ $i }}</td><td>{{ $usulan->judul }}</td><td></td><td align="right">{{ $usulan->volume }}</td>
                <td>{{ $usulan->satuan }}</td><td align="right">{{ $usulan->pria }}</td><td align="right">{{ $usulan->wanita }}</td><td align="right">{{ $usulan->rtm }}</td></tr>
                <?php $i++; ?>
                @endif
            @endforeach
            </table><br>
            <table width="100%">
            <tr><td colspan="2" align="right">Desa {{ $desa->nama }},  
            <?php 
            $bulan = date("m");
            if($bulan == 01){
                $bulanindo = "Januari";
            }
            else if($bulan == 02){
                $bulanindo = "Februari";
            }
            else if($bulan == 03){
                $bulanindo = "Maret";
            }
            else if($bulan == 04){
                $bulanindo = "April";
            }
            else if($bulan == 05){
                $bulanindo = "Mei";
            }
            else if($bulan == 06){
                $bulanindo = "Juni";
            }
            else if($bulan == 07){
                $bulanindo = "Juli";
            }
            else if($bulan == 08){
                $bulanindo = "Agustus";
            }
            else if($bulan == 09){
                $bulanindo = "September";
            }
            else if($bulan == 10){
                $bulanindo = "Oktober";
            }
            else if($bulan == 11){
                $bulanindo = "November";
            }
            else if($bulan == 12){
                $bulanindo = "Desember";
            }
            
            echo date("d") . " " . $bulanindo . " " . date("Y");
            ?>
            </td></tr>
            <tr>
            <td align="center"><br>Mengetahui<br>Kepala Desa<br><br><br><br><br>( {{ $desa->nama_kades }} )</td>
            <td align="center"><br><br>Ketua Tim Penyusun RPJM Desa<br><br><br><br><br>( ........................... )</td>
            </tr>
            </table>
        <script type="text/javascript">
          print();
        </script>
        </body>
</html>