@extends('template')

@section('main')
<div id="kategoriproduk" class="panel panel-default">
	<div class="panel-heading"><b><h4>Sub Kategori Produk</h4></b></div>
	<div class="panel-body">
	<div class="tombol-nav">
	{{ link_to('subkategoriproduk/create','Tambah Sub Kategori Produk',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarsubkategoriproduk) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Sub Kategori</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarsubkategoriproduk as $subkategoriproduk): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $subkategoriproduk->namasubkategori }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('subkategoriproduk/' . $subkategoriproduk->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['SubKategoriprodukwebController@destroy',$subkategoriproduk->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('subkategoriproduk/print/' . $subkategoriproduk->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak Ada Spesifkasi Produk</p>
	@endif

</div>
@stop

@section('footer')
	@include('footer')
@stop