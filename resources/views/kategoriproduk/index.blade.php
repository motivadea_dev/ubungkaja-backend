@extends('template')

@section('main')
<div id="kategoriproduk" class="panel panel-default">
	<div class="panel-heading"><b><h4>Kategori Produk</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
	{{ link_to('kategoriproduk/create','Tambah Kategori Produk',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarkategoriproduk) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Kategori</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarkategoriproduk as $kategoriproduk): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $kategoriproduk->namakategori }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('kategoriproduk/' . $kategoriproduk->id,'Sub Kategori',['class' => 'btn btn-success btn-sm']) }}</div> 
					<div class="box-button">
					{{ link_to('kategoriproduk/' . $kategoriproduk->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['KategoriprodukwebController@destroy',$kategoriproduk->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('kategoriproduk/print/' . $kategoriproduk->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak Ada Spesifkasi Produk</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Kategori : {{ $jumlahkategoriproduk }}</strong>
	</div>
	<div class="paging">
	{{ $daftarkategoriproduk->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop