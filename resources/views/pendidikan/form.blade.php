@if (isset($pendidikan))
{!! Form::hidden('id', $pendidikan->id) !!}
@endif


{{-- Nama Pendidikan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('namapendidikan') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('namapendidikan','Nama Pendidikan',['class' => 'control-label']) !!}
	{!! Form::text('namapendidikan', null,['class' => 'form-control']) !!}
	@if ($errors->has('namapendidikan'))
	<span class="help-block">{{ $errors->first('namapendidikan') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>