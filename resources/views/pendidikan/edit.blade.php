@extends('template')

@section('main')
	<div id="pendidikan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Pendidikan</h4></b></div>
		<div class="panel-body">
		{!! Form::model($pendidikan, ['method' => 'PATCH', 'action' => ['PendidikanwebController@update', $pendidikan->id],'files'=>true]) !!}

		@include('pendidikan.form', ['submitButtonText' => 'Update Pendidikan'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop