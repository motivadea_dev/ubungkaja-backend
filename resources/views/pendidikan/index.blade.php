@extends('template')

@section('main')
<div id="pendidikan" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Pendidikan</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('pendidikan/create','Tambah Pendidikan',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarpendidikan) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Pendidikan</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarpendidikan as $pendidikan): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $pendidikan-> namapendidikan }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('pendidikan/' . $pendidikan->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['PendidikanwebController@destroy',$pendidikan->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Pendidikan.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Pekerjaan : {{ $jumlahpendidikan }}</strong>
	</div>
	<div class="paging">
	{{ $daftarpendidikan->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop