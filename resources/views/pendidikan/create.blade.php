@extends('template')

@section('main')
	<div id="pendidikan" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Pendidikan</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'pendidikan', 'files' => true]) !!}

		@include('pendidikan.form', ['submitButtonText' => 'Tambah Pendidikan'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop