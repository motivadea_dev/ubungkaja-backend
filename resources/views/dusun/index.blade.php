@extends('template')

@section('main')
<div id="dusun" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Dusun</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<!--@include('produk.form_pencarian')-->
	<div class="tombol-nav">
		{{ link_to('dusun/create','Tambah Dusun',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftardusun) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Nama Dusun</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftardusun as $dusun): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $dusun->nama }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('dusun/' . $dusun->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['DusunwebController@destroy',$dusun->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Nama Dusun.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Dusun : {{ $jumlahdusun }}</strong>
	</div>
	<div class="paging">
	{{ $daftardusun->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop