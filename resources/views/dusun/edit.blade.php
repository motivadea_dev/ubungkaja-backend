@extends('template')

@section('main')
	<div id="dusun" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Dusun</h4></b></div>
		<div class="panel-body">
		{!! Form::model($dusun, ['method' => 'PATCH', 'action' => ['DusunwebController@update', $dusun->id],'files'=>true]) !!}

		@include('dusun.form', ['submitButtonText' => 'Update Dusun'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop