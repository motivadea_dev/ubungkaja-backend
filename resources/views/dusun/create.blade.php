@extends('template')

@section('main')
	<div id="dusun" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Dusun</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'dusun', 'files' => true]) !!}

		@include('dusun.form', ['submitButtonText' => 'Tambah Dusun'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop