-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 07, 2018 at 05:09 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `perencanaandesa`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggotakeluarga`
--

CREATE TABLE IF NOT EXISTS `anggotakeluarga` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_keluarga` int(10) unsigned NOT NULL,
  `id_warga` int(10) unsigned NOT NULL,
  `hubungan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `anggotakeluarga_id_keluarga_foreign` (`id_keluarga`),
  KEY `anggotakeluarga_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `antrian`
--

CREATE TABLE IF NOT EXISTS `antrian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `id_warga` int(10) unsigned NOT NULL,
  `id_keperluan` int(10) unsigned NOT NULL,
  `status_syarat` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `status_jadi` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `antrian_id_keperluan_foreign` (`id_keperluan`),
  KEY `antrian_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `balaskomentar`
--

CREATE TABLE IF NOT EXISTS `balaskomentar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_komentarpost` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `komentar` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `balaskomentar_id_komentarpost_foreign` (`id_komentarpost`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profiledesa` int(10) unsigned NOT NULL,
  `tahap` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggalmulai` date NOT NULL,
  `tanggalselesai` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `berita_id_profiledesa_foreign` (`id_profiledesa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `datauser`
--

CREATE TABLE IF NOT EXISTS `datauser` (
  `id_warga` int(10) unsigned NOT NULL,
  `nomor_telepon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_warga`),
  UNIQUE KEY `datauser_nomor_telepon_unique` (`nomor_telepon`),
  UNIQUE KEY `datauser_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detaildompet`
--

CREATE TABLE IF NOT EXISTS `detaildompet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_dompet` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `nominal` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detaildompet_id_dompet_foreign` (`id_dompet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `detailpembelian`
--

CREATE TABLE IF NOT EXISTS `detailpembelian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_transaksipembelian` int(10) unsigned NOT NULL,
  `id_produkbumdes` int(10) unsigned NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detailpembelian_id_produkbumdes_foreign` (`id_produkbumdes`),
  KEY `detailpembelian_id_transaksipembelian_foreign` (`id_transaksipembelian`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `detailpenjualan`
--

CREATE TABLE IF NOT EXISTS `detailpenjualan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_transaksipenjualan` int(10) unsigned NOT NULL,
  `id_produktoko` int(10) unsigned NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detailpenjualan_id_transaksipenjualan_foreign` (`id_transaksipenjualan`),
  KEY `detailpenjualan_id_produktoko_foreign` (`id_produktoko`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `distributor`
--

CREATE TABLE IF NOT EXISTS `distributor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profiledesa` int(10) unsigned NOT NULL,
  `kodedistributor` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `namadistributor` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('buka','tutup') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `distributor_kodedistributor_unique` (`kodedistributor`),
  KEY `distributor_id_profiledesa_foreign` (`id_profiledesa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `distributor`
--

INSERT INTO `distributor` (`id`, `id_profiledesa`, `kodedistributor`, `namadistributor`, `alamat`, `foto`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '121342546', 'Cyberway Media', 'Jalan Jalan', 'noimage.png', 'buka', '2017-12-17 15:01:46', '2017-12-17 15:01:46');

-- --------------------------------------------------------

--
-- Table structure for table `dompet`
--

CREATE TABLE IF NOT EXISTS `dompet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_warga` int(10) unsigned NOT NULL,
  `saldo` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dompet_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dusun`
--

CREATE TABLE IF NOT EXISTS `dusun` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profiledesa` int(10) unsigned NOT NULL,
  `nama` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dusun_id_profiledesa_foreign` (`id_profiledesa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `dusun`
--

INSERT INTO `dusun` (`id`, `id_profiledesa`, `nama`, `created_at`, `updated_at`) VALUES
(1, 1, 'Muktiharjo Kidul', NULL, NULL),
(2, 1, 'Tlogosari Kulon', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `favorit`
--

CREATE TABLE IF NOT EXISTS `favorit` (
  `id_warga` int(10) unsigned NOT NULL,
  `id_produktoko` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_warga`,`id_produktoko`),
  KEY `favorit_id_warga_index` (`id_warga`),
  KEY `favorit_id_produktoko_index` (`id_produktoko`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fotoantrian`
--

CREATE TABLE IF NOT EXISTS `fotoantrian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_antrian` int(10) unsigned NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fotoantrian_id_antrian_foreign` (`id_antrian`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fotopengaduan`
--

CREATE TABLE IF NOT EXISTS `fotopengaduan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pengaduan` int(10) unsigned NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fotopengaduan_id_pengaduan_foreign` (`id_pengaduan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fotoproduk`
--

CREATE TABLE IF NOT EXISTS `fotoproduk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_produktoko` int(10) unsigned NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fotoproduk_id_produktoko_foreign` (`id_produktoko`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fototemp`
--

CREATE TABLE IF NOT EXISTS `fototemp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_warga` int(11) NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fototempproduk`
--

CREATE TABLE IF NOT EXISTS `fototempproduk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_toko` int(11) NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fotousulan`
--

CREATE TABLE IF NOT EXISTS `fotousulan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usulan` int(10) unsigned NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fotousulan_id_usulan_foreign` (`id_usulan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `informasi`
--

CREATE TABLE IF NOT EXISTS `informasi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profiledesa` int(10) unsigned NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `informasi_id_profiledesa_foreign` (`id_profiledesa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profiledesa` int(10) unsigned NOT NULL,
  `namakategori` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kategori_id_profiledesa_foreign` (`id_profiledesa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `id_profiledesa`, `namakategori`, `created_at`, `updated_at`) VALUES
(1, 1, 'Penyelenggaraan Pemerintahan Desa', '2017-11-15 12:21:33', '2017-11-15 12:21:33'),
(2, 1, 'Pelaksanaan Pembangunan Desa', '2017-11-15 12:21:44', '2017-11-15 12:21:44'),
(3, 1, 'Pembinaan Kemasyarakatan', '2017-11-15 12:21:54', '2017-11-15 12:21:54'),
(4, 1, 'Pemberdayaan Masyarakat', '2017-11-15 12:22:05', '2017-11-15 12:22:05');

-- --------------------------------------------------------

--
-- Table structure for table `kategoripengaduan`
--

CREATE TABLE IF NOT EXISTS `kategoripengaduan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profiledesa` int(10) unsigned NOT NULL,
  `namakategori` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kategoripost`
--

CREATE TABLE IF NOT EXISTS `kategoripost` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namakategori` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kategoripost`
--

INSERT INTO `kategoripost` (`id`, `namakategori`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'Wisata', 'noimage.png', '2018-02-07 09:00:59', '2018-02-07 09:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `kategoriproduk`
--

CREATE TABLE IF NOT EXISTS `kategoriproduk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namakategori` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kategoriproduk`
--

INSERT INTO `kategoriproduk` (`id`, `namakategori`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'Sembako', 'sembako.jpg', '2017-11-26 04:41:22', '2017-11-26 04:41:22'),
(2, 'Peralatan Rumah Tangga', 'rumahtangga.jpg', '2017-11-26 04:41:33', '2017-11-26 04:41:33');

-- --------------------------------------------------------

--
-- Table structure for table `keluarga`
--

CREATE TABLE IF NOT EXISTS `keluarga` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nokk` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `id_profiledesa` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keluarga_nokk_unique` (`nokk`),
  KEY `keluarga_id_profiledesa_foreign` (`id_profiledesa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `keperluan`
--

CREATE TABLE IF NOT EXISTS `keperluan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profiledesa` int(10) unsigned NOT NULL,
  `nama_keperluan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `syarat` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `keperluan_id_profiledesa_foreign` (`id_profiledesa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE IF NOT EXISTS `keranjang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_warga` int(11) NOT NULL,
  `id_produktoko` int(10) unsigned NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `keranjang_id_produktoko_foreign` (`id_produktoko`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `keranjangtoko`
--

CREATE TABLE IF NOT EXISTS `keranjangtoko` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_toko` int(11) NOT NULL,
  `id_produkbumdes` int(10) unsigned NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `keranjangtoko_id_produkbumdes_foreign` (`id_produkbumdes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `komentarpengaduan`
--

CREATE TABLE IF NOT EXISTS `komentarpengaduan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pengaduan` int(10) unsigned NOT NULL,
  `id_warga` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `komentar` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `komentarpengaduan_id_pengaduan_foreign` (`id_pengaduan`),
  KEY `komentarpengaduan_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `komentarpost`
--

CREATE TABLE IF NOT EXISTS `komentarpost` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_posting` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `komentar` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `komentarpost_id_posting_foreign` (`id_posting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kurir`
--

CREATE TABLE IF NOT EXISTS `kurir` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_warga` int(10) unsigned NOT NULL,
  `nomor_kurir` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `plat_nomor` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_kendaraan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('aktif','nonaktif') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kurir_nomor_kurir_unique` (`nomor_kurir`),
  KEY `kurir_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_10_12_100001_create_table_berita', 1),
('2014_10_12_100002_create_table_detailpenjualan', 1),
('2014_10_12_100003_create_table_transaksipenjualan', 1),
('2014_10_12_100004_create_table_detailpembelian', 1),
('2014_10_12_100005_create_table_keranjang', 1),
('2014_10_12_100006_create_table_keranjangtoko', 1),
('2014_10_12_100007_create_table_produkbumdes', 1),
('2014_10_12_100008_create_table_pengiriman', 1),
('2014_10_12_100009_create_table_pesan', 1),
('2014_10_12_100010_create_table_notifikasiwarga', 1),
('2014_10_12_100011_create_table_notifikasitoko', 1),
('2014_10_12_100012_create_table_detaildompet', 1),
('2014_10_12_100013_create_table_fototemp', 1),
('2014_10_12_100013_create_table_proposaltemp', 1),
('2014_10_12_100014_create_table_fototempproduk', 1),
('2014_10_12_100015_create_table_fotousulan', 1),
('2014_10_12_100015_create_table_proposalusulan', 1),
('2014_10_12_100016_create_table_balaskomentar', 1),
('2014_10_12_100016_create_table_fotopengaduan', 1),
('2014_10_12_100017_create_table_komentarpengaduan', 1),
('2014_10_12_100017_create_table_komentarpost', 1),
('2014_10_12_100017_create_table_polling', 1),
('2014_10_12_100018_create_table_tindakanpengaduan', 1),
('2014_10_12_100019_create_table_fotoproduk', 1),
('2014_10_12_100020_create_table_fotoantrian', 1),
('2014_10_12_100021_create_table_pendidikan', 1),
('2014_10_12_100022_create_table_pekerjaan', 1),
('2014_10_12_100023_create_table_penghasilan', 1),
('2014_10_12_100024_create_table_sertifikat', 1),
('2014_10_12_100025_create_table_posting', 1),
('2014_10_12_100025_create_table_produktoko', 1),
('2014_10_12_100026_create_table_usulan', 1),
('2014_10_12_100027_create_table_kategori', 1),
('2014_10_12_100028_create_table_subkategoriproduk', 1),
('2014_10_12_100029_create_table_transaksipembelian', 1),
('2014_10_12_100030_create_table_pengaduan', 1),
('2014_10_12_100031_create_table_kategoripengaduan', 1),
('2014_10_12_100032_create_table_antrian', 1),
('2014_10_12_100033_create_table_keperluan', 1),
('2014_10_12_100034_create_table_anggotakeluarga', 1),
('2014_10_12_100035_create_table_keluarga', 1),
('2014_10_12_100036_create_table_informasi', 1),
('2014_10_12_100037_create_table_distributor', 1),
('2014_10_12_100038_create_table_toko', 1),
('2014_10_12_100039_create_table_dompet', 1),
('2014_10_12_100040_create_table_kurir', 1),
('2014_10_12_100041_create_table_kategoripost', 1),
('2014_10_12_100041_create_table_kategoriproduk', 1),
('2014_10_12_100042_create_table_perangkat', 1),
('2014_10_12_100043_create_table_warga', 1),
('2014_10_12_100044_create_table_dusun', 1),
('2014_10_12_100045_create_table_favorit', 1),
('2014_10_12_100046_create_table_datauser', 1),
('2014_10_12_100047_create_table_profiledesa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifikasitoko`
--

CREATE TABLE IF NOT EXISTS `notifikasitoko` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_toko` int(10) unsigned NOT NULL,
  `isi_notifikasi` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifikasitoko_id_toko_foreign` (`id_toko`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifikasiwarga`
--

CREATE TABLE IF NOT EXISTS `notifikasiwarga` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_warga` int(10) unsigned NOT NULL,
  `isi_notifikasi` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifikasiwarga_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE IF NOT EXISTS `pekerjaan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namapekerjaan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE IF NOT EXISTS `pendidikan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namapendidikan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pengaduan`
--

CREATE TABLE IF NOT EXISTS `pengaduan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_kategoripengaduan` int(10) unsigned NOT NULL,
  `id_dusun` int(10) unsigned NOT NULL,
  `id_warga` int(10) unsigned NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `privasipengaduan` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `privasiidentitas` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pengaduan_id_kategoripengaduan_foreign` (`id_kategoripengaduan`),
  KEY `pengaduan_id_warga_foreign` (`id_warga`),
  KEY `pengaduan_id_dusun_foreign` (`id_dusun`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `penghasilan`
--

CREATE TABLE IF NOT EXISTS `penghasilan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namapenghasilan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman`
--

CREATE TABLE IF NOT EXISTS `pengiriman` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_transaksipembelian` int(10) unsigned NOT NULL,
  `id_kurir` int(10) unsigned NOT NULL,
  `status` enum('proses','selesai') COLLATE utf8_unicode_ci NOT NULL,
  `komisi` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pengiriman_id_transaksipembelian_foreign` (`id_transaksipembelian`),
  KEY `pengiriman_id_kurir_foreign` (`id_kurir`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `perangkat`
--

CREATE TABLE IF NOT EXISTS `perangkat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_warga` int(10) unsigned NOT NULL,
  `app_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `perangkat_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE IF NOT EXISTS `pesan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_warga` int(10) unsigned NOT NULL,
  `id_toko` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `isi_pesan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pesan_id_toko_foreign` (`id_toko`),
  KEY `pesan_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `polling`
--

CREATE TABLE IF NOT EXISTS `polling` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_posting` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `polling_id_posting_foreign` (`id_posting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `posting`
--

CREATE TABLE IF NOT EXISTS `posting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kategoripost` int(10) unsigned NOT NULL,
  `id_warga` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isipost` text COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `headline` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posting_id_kategoripost_foreign` (`id_kategoripost`),
  KEY `posting_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `posting`
--

INSERT INTO `posting` (`id`, `id_kategoripost`, `id_warga`, `tanggal`, `judul`, `isipost`, `foto`, `tag`, `status`, `headline`, `view`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2018-02-07', 'TEST', 'TEST', '20180207160123.jpg', 'TEST', '2', '2', 0, '2018-02-07 09:01:23', '2018-02-07 09:01:23');

-- --------------------------------------------------------

--
-- Table structure for table `produkbumdes`
--

CREATE TABLE IF NOT EXISTS `produkbumdes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kategoriproduk` int(10) unsigned NOT NULL,
  `id_subkategoriproduk` int(10) unsigned NOT NULL,
  `id_distributor` int(10) unsigned NOT NULL,
  `kodeproduk` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `namaproduk` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` double NOT NULL,
  `diskon` double NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `produkbumdes_kodeproduk_unique` (`kodeproduk`),
  KEY `produkbumdes_id_subkategoriproduk_foreign` (`id_subkategoriproduk`),
  KEY `produkbumdes_id_distributor_foreign` (`id_distributor`),
  KEY `produkbumdes_id_kategoriproduk_foreign` (`id_kategoriproduk`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `produkbumdes`
--

INSERT INTO `produkbumdes` (`id`, `id_kategoriproduk`, `id_subkategoriproduk`, `id_distributor`, `kodeproduk`, `namaproduk`, `stok`, `harga`, `diskon`, `foto`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, '234234234', 'Beras C4', 96, 10000, 9000, '20171217170238.jpg', '2017-12-17 15:02:38', '2017-12-18 04:40:40');

-- --------------------------------------------------------

--
-- Table structure for table `produktoko`
--

CREATE TABLE IF NOT EXISTS `produktoko` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kategoriproduk` int(10) unsigned NOT NULL,
  `id_subkategoriproduk` int(10) unsigned NOT NULL,
  `id_toko` int(10) unsigned NOT NULL,
  `kodeproduk` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `namaproduk` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` double NOT NULL,
  `diskon` double NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `produktoko_kodeproduk_unique` (`kodeproduk`),
  KEY `produktoko_id_subkategoriproduk_foreign` (`id_subkategoriproduk`),
  KEY `produktoko_id_toko_foreign` (`id_toko`),
  KEY `produktoko_id_kategoriproduk_foreign` (`id_kategoriproduk`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `produktoko`
--

INSERT INTO `produktoko` (`id`, `id_kategoriproduk`, `id_subkategoriproduk`, `id_toko`, `kodeproduk`, `namaproduk`, `stok`, `harga`, `diskon`, `foto`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, '12345678', 'Beras Rojo Lele', 0, 15000, 13000, '20171206040636.jpg', '2017-12-05 07:06:36', '2017-12-11 16:40:09'),
(2, 1, 2, 1, '54423534', 'Minyak Sania', 8, 15000, 10000, '20171206041315.jpg', '2017-12-05 07:13:15', '2017-12-11 16:41:23'),
(3, 1, 3, 1, '56432324', 'Beras C4', 15, 10000, 9000, '20171208130247.jpg', '2017-12-08 04:02:47', '2017-12-08 04:02:47'),
(4, 2, 4, 1, '54324332', 'Kain Pel', 100, 5000, 3500, '20171210185822.jpg', '2017-12-10 16:58:22', '2017-12-10 16:58:22');

-- --------------------------------------------------------

--
-- Table structure for table `profiledesa`
--

CREATE TABLE IF NOT EXISTS `profiledesa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kecamatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kabupaten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_kades` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `profiledesa`
--

INSERT INTO `profiledesa` (`id`, `nama`, `kecamatan`, `kabupaten`, `provinsi`, `nip`, `nama_kades`, `created_at`, `updated_at`) VALUES
(1, 'Desa Ubung Kaja', 'Denpasar Utara', 'Denpasar', 'Bali', '123456778', 'I Wayan Mirta', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `proposaltemp`
--

CREATE TABLE IF NOT EXISTS `proposaltemp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_warga` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `proposalusulan`
--

CREATE TABLE IF NOT EXISTS `proposalusulan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usulan` int(10) unsigned NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proposalusulan_id_usulan_foreign` (`id_usulan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sertifikat`
--

CREATE TABLE IF NOT EXISTS `sertifikat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_keluarga` int(10) unsigned NOT NULL,
  `lokasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `latitude` decimal(11,8) NOT NULL,
  `luaswilayah` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sertifikat_id_keluarga_foreign` (`id_keluarga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subkategoriproduk`
--

CREATE TABLE IF NOT EXISTS `subkategoriproduk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kategoriproduk` int(10) unsigned NOT NULL,
  `namasubkategori` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subkategoriproduk_id_kategoriproduk_foreign` (`id_kategoriproduk`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `subkategoriproduk`
--

INSERT INTO `subkategoriproduk` (`id`, `id_kategoriproduk`, `namasubkategori`, `created_at`, `updated_at`) VALUES
(2, 1, 'Minyak Goreng', '2017-11-27 16:08:44', '2017-11-27 16:08:44'),
(3, 1, 'Beras', '2017-12-05 07:04:46', '2017-12-05 07:04:46'),
(4, 2, 'Peralatan Kebersihan', '2017-12-10 16:57:25', '2017-12-10 16:57:25');

-- --------------------------------------------------------

--
-- Table structure for table `tindakanpengaduan`
--

CREATE TABLE IF NOT EXISTS `tindakanpengaduan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pengaduan` int(10) unsigned NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tindakanpengaduan_id_pengaduan_foreign` (`id_pengaduan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `toko`
--

CREATE TABLE IF NOT EXISTS `toko` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_warga` int(10) unsigned NOT NULL,
  `kodetoko` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `namatoko` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('buka','tutup') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `toko_kodetoko_unique` (`kodetoko`),
  KEY `toko_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `toko`
--

INSERT INTO `toko` (`id`, `id_warga`, `kodetoko`, `namatoko`, `alamat`, `foto`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '12343564', 'Toko Awan', 'Jalan', 'noimage.png', 'buka', '2017-11-27 16:17:08', '2017-11-27 16:17:08');

-- --------------------------------------------------------

--
-- Table structure for table `transaksipembelian`
--

CREATE TABLE IF NOT EXISTS `transaksipembelian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_toko` int(10) unsigned NOT NULL,
  `id_distributor` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `totaldiskon` double NOT NULL,
  `totalbelanja` double NOT NULL,
  `subtotal` double NOT NULL,
  `status` enum('order','kirim','selesai') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaksipembelian_id_distributor_foreign` (`id_distributor`),
  KEY `transaksipembelian_id_toko_foreign` (`id_toko`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaksipenjualan`
--

CREATE TABLE IF NOT EXISTS `transaksipenjualan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_toko` int(10) unsigned NOT NULL,
  `id_warga` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `totaldiskon` double NOT NULL,
  `totalbelanja` double NOT NULL,
  `subtotal` double NOT NULL,
  `status` enum('order','kirim','selesai') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaksipenjualan_id_toko_foreign` (`id_toko`),
  KEY `transaksipenjualan_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` enum('admin','dusun','desa','warga','pelaksana') COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `fotoktp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fotowajah` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nohp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_dusun` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_id_dusun_foreign` (`id_dusun`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `remember_token`, `level`, `status`, `fotoktp`, `fotowajah`, `nohp`, `email`, `id_dusun`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '$2y$10$qYX1WgLPv9KEN2JS0wCCS.1h25Wu9D6h81S/5X/PB6rxHoc5goXny', 'pNQ4hymGQWGNfguGOUM0JCAdKay9FxcxhpTubbHcCR9ZlPBf75QZs0AWi5Ie', 'admin', '2', '', '', '', '', 1, NULL, '2018-02-07 08:58:26'),
(29, 'Awan Aprifiantosa', '3374060104860005', '$2y$10$Pw9Fn6Bzfd3/YOzVE4FIXePKbCM.Q.6NbmJP0rj97XCXcaugAScyC', NULL, 'warga', '2', 'ktp_3374060104860005.jpg', 'wajah_3374060104860005.jpg', '081279349292', 'aoneholics@gmail.com', 1, '2018-01-29 11:04:12', '2018-01-29 11:07:44'),
(31, 'Galih', '54321', '$2y$10$xCso7bLfj6bungSP3tPcLeCJEfdjXywo1WnjIhXQ/OPoSD40j7GqG', NULL, 'warga', '2', 'ktp_54321.jpg', 'wajah_54321.jpg', '08530303030', 'Galileogalilei@gmail.com', 2, '2018-02-02 07:36:15', '2018-02-02 07:37:05'),
(32, 'Febri', '1357', '$2y$10$uxZ1JjQEt1x4kJB5cDvS8.9lbfmI749RcCYAQ7MRI.tN/8dTol8Wy', NULL, 'warga', '2', 'ktp_1357.jpg', 'wajah_1357.jpg', '083000000000', 'a@b.com', 1, '2018-02-02 07:38:30', '2018-02-02 07:38:47');

-- --------------------------------------------------------

--
-- Table structure for table `usulan`
--

CREATE TABLE IF NOT EXISTS `usulan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_kategori` int(10) unsigned NOT NULL,
  `id_warga` int(10) unsigned NOT NULL,
  `lokasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `latitude` decimal(11,8) NOT NULL,
  `volume` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wanita` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rtm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL,
  `prioritas` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `rangking` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usulan_id_kategori_foreign` (`id_kategori`),
  KEY `usulan_id_warga_foreign` (`id_warga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `warga`
--

CREATE TABLE IF NOT EXISTS `warga` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noktp` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('L','P') COLLATE utf8_unicode_ci NOT NULL,
  `namapendidikan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `namapekerjaan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `namapenghasilan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_dusun` int(10) unsigned NOT NULL,
  `id_profiledesa` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `warga_noktp_unique` (`noktp`),
  KEY `warga_id_dusun_foreign` (`id_dusun`),
  KEY `warga_id_profiledesa_foreign` (`id_profiledesa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `warga`
--

INSERT INTO `warga` (`id`, `noktp`, `nama`, `alamat`, `tanggal_lahir`, `jenis_kelamin`, `namapendidikan`, `namapekerjaan`, `namapenghasilan`, `foto`, `id_dusun`, `id_profiledesa`, `created_at`, `updated_at`) VALUES
(1, '12345678', 'Awan A', 'Jalan', '2017-11-01', 'L', '', '', '', '20171119113807.jpg', 1, 1, NULL, '2017-11-16 19:38:07'),
(2, '3374060104860005', 'Awan Aprifiantosa', 'Jalan Gringsing 2 No.24', '1986-04-01', 'L', '', '', '', 'noimage.png', 1, 1, '2018-01-27 08:27:35', '2018-01-27 08:27:35'),
(3, '54321', 'Galih', 'Jalan', '2018-02-02', 'L', '', '', '', 'noimage.png', 2, 1, '2018-02-02 07:32:37', '2018-02-02 07:32:37'),
(4, '1357', 'Febri', 'Jalan', '2018-02-02', 'L', '', '', '', 'noimage.png', 1, 1, '2018-02-02 07:33:07', '2018-02-02 07:33:07');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anggotakeluarga`
--
ALTER TABLE `anggotakeluarga`
  ADD CONSTRAINT `anggotakeluarga_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `anggotakeluarga_id_keluarga_foreign` FOREIGN KEY (`id_keluarga`) REFERENCES `keluarga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `antrian`
--
ALTER TABLE `antrian`
  ADD CONSTRAINT `antrian_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `antrian_id_keperluan_foreign` FOREIGN KEY (`id_keperluan`) REFERENCES `keperluan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `balaskomentar`
--
ALTER TABLE `balaskomentar`
  ADD CONSTRAINT `balaskomentar_id_komentarpost_foreign` FOREIGN KEY (`id_komentarpost`) REFERENCES `komentarpost` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `berita`
--
ALTER TABLE `berita`
  ADD CONSTRAINT `berita_id_profiledesa_foreign` FOREIGN KEY (`id_profiledesa`) REFERENCES `profiledesa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `datauser`
--
ALTER TABLE `datauser`
  ADD CONSTRAINT `datauser_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detaildompet`
--
ALTER TABLE `detaildompet`
  ADD CONSTRAINT `detaildompet_id_dompet_foreign` FOREIGN KEY (`id_dompet`) REFERENCES `dompet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detailpembelian`
--
ALTER TABLE `detailpembelian`
  ADD CONSTRAINT `detailpembelian_id_transaksipembelian_foreign` FOREIGN KEY (`id_transaksipembelian`) REFERENCES `transaksipembelian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detailpembelian_id_produkbumdes_foreign` FOREIGN KEY (`id_produkbumdes`) REFERENCES `produkbumdes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detailpenjualan`
--
ALTER TABLE `detailpenjualan`
  ADD CONSTRAINT `detailpenjualan_id_produktoko_foreign` FOREIGN KEY (`id_produktoko`) REFERENCES `produktoko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detailpenjualan_id_transaksipenjualan_foreign` FOREIGN KEY (`id_transaksipenjualan`) REFERENCES `transaksipenjualan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `distributor`
--
ALTER TABLE `distributor`
  ADD CONSTRAINT `distributor_id_profiledesa_foreign` FOREIGN KEY (`id_profiledesa`) REFERENCES `profiledesa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dompet`
--
ALTER TABLE `dompet`
  ADD CONSTRAINT `dompet_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dusun`
--
ALTER TABLE `dusun`
  ADD CONSTRAINT `dusun_id_profiledesa_foreign` FOREIGN KEY (`id_profiledesa`) REFERENCES `profiledesa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favorit`
--
ALTER TABLE `favorit`
  ADD CONSTRAINT `favorit_id_produktoko_foreign` FOREIGN KEY (`id_produktoko`) REFERENCES `produktoko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorit_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fotoantrian`
--
ALTER TABLE `fotoantrian`
  ADD CONSTRAINT `fotoantrian_id_antrian_foreign` FOREIGN KEY (`id_antrian`) REFERENCES `antrian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fotopengaduan`
--
ALTER TABLE `fotopengaduan`
  ADD CONSTRAINT `fotopengaduan_id_pengaduan_foreign` FOREIGN KEY (`id_pengaduan`) REFERENCES `pengaduan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fotoproduk`
--
ALTER TABLE `fotoproduk`
  ADD CONSTRAINT `fotoproduk_id_produktoko_foreign` FOREIGN KEY (`id_produktoko`) REFERENCES `produktoko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fotousulan`
--
ALTER TABLE `fotousulan`
  ADD CONSTRAINT `fotousulan_id_usulan_foreign` FOREIGN KEY (`id_usulan`) REFERENCES `usulan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `informasi`
--
ALTER TABLE `informasi`
  ADD CONSTRAINT `informasi_id_profiledesa_foreign` FOREIGN KEY (`id_profiledesa`) REFERENCES `profiledesa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kategori`
--
ALTER TABLE `kategori`
  ADD CONSTRAINT `kategori_id_profiledesa_foreign` FOREIGN KEY (`id_profiledesa`) REFERENCES `profiledesa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keluarga`
--
ALTER TABLE `keluarga`
  ADD CONSTRAINT `keluarga_id_profiledesa_foreign` FOREIGN KEY (`id_profiledesa`) REFERENCES `profiledesa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keperluan`
--
ALTER TABLE `keperluan`
  ADD CONSTRAINT `keperluan_id_profiledesa_foreign` FOREIGN KEY (`id_profiledesa`) REFERENCES `profiledesa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_id_produktoko_foreign` FOREIGN KEY (`id_produktoko`) REFERENCES `produktoko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keranjangtoko`
--
ALTER TABLE `keranjangtoko`
  ADD CONSTRAINT `keranjangtoko_id_produkbumdes_foreign` FOREIGN KEY (`id_produkbumdes`) REFERENCES `produkbumdes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `komentarpengaduan`
--
ALTER TABLE `komentarpengaduan`
  ADD CONSTRAINT `komentarpengaduan_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `komentarpengaduan_id_pengaduan_foreign` FOREIGN KEY (`id_pengaduan`) REFERENCES `pengaduan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `komentarpost`
--
ALTER TABLE `komentarpost`
  ADD CONSTRAINT `komentarpost_id_posting_foreign` FOREIGN KEY (`id_posting`) REFERENCES `posting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kurir`
--
ALTER TABLE `kurir`
  ADD CONSTRAINT `kurir_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notifikasitoko`
--
ALTER TABLE `notifikasitoko`
  ADD CONSTRAINT `notifikasitoko_id_toko_foreign` FOREIGN KEY (`id_toko`) REFERENCES `toko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notifikasiwarga`
--
ALTER TABLE `notifikasiwarga`
  ADD CONSTRAINT `notifikasiwarga_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD CONSTRAINT `pengaduan_id_dusun_foreign` FOREIGN KEY (`id_dusun`) REFERENCES `dusun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengaduan_id_kategoripengaduan_foreign` FOREIGN KEY (`id_kategoripengaduan`) REFERENCES `kategoripengaduan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengaduan_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD CONSTRAINT `pengiriman_id_kurir_foreign` FOREIGN KEY (`id_kurir`) REFERENCES `kurir` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengiriman_id_transaksipembelian_foreign` FOREIGN KEY (`id_transaksipembelian`) REFERENCES `transaksipembelian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `perangkat`
--
ALTER TABLE `perangkat`
  ADD CONSTRAINT `perangkat_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pesan`
--
ALTER TABLE `pesan`
  ADD CONSTRAINT `pesan_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pesan_id_toko_foreign` FOREIGN KEY (`id_toko`) REFERENCES `toko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `polling`
--
ALTER TABLE `polling`
  ADD CONSTRAINT `polling_id_posting_foreign` FOREIGN KEY (`id_posting`) REFERENCES `posting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posting`
--
ALTER TABLE `posting`
  ADD CONSTRAINT `posting_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posting_id_kategoripost_foreign` FOREIGN KEY (`id_kategoripost`) REFERENCES `kategoripost` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produkbumdes`
--
ALTER TABLE `produkbumdes`
  ADD CONSTRAINT `produkbumdes_id_kategoriproduk_foreign` FOREIGN KEY (`id_kategoriproduk`) REFERENCES `kategoriproduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `produkbumdes_id_distributor_foreign` FOREIGN KEY (`id_distributor`) REFERENCES `distributor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `produkbumdes_id_subkategoriproduk_foreign` FOREIGN KEY (`id_subkategoriproduk`) REFERENCES `subkategoriproduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produktoko`
--
ALTER TABLE `produktoko`
  ADD CONSTRAINT `produktoko_id_kategoriproduk_foreign` FOREIGN KEY (`id_kategoriproduk`) REFERENCES `kategoriproduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `produktoko_id_subkategoriproduk_foreign` FOREIGN KEY (`id_subkategoriproduk`) REFERENCES `subkategoriproduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `produktoko_id_toko_foreign` FOREIGN KEY (`id_toko`) REFERENCES `toko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `proposalusulan`
--
ALTER TABLE `proposalusulan`
  ADD CONSTRAINT `proposalusulan_id_usulan_foreign` FOREIGN KEY (`id_usulan`) REFERENCES `usulan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sertifikat`
--
ALTER TABLE `sertifikat`
  ADD CONSTRAINT `sertifikat_id_keluarga_foreign` FOREIGN KEY (`id_keluarga`) REFERENCES `keluarga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subkategoriproduk`
--
ALTER TABLE `subkategoriproduk`
  ADD CONSTRAINT `subkategoriproduk_id_kategoriproduk_foreign` FOREIGN KEY (`id_kategoriproduk`) REFERENCES `kategoriproduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tindakanpengaduan`
--
ALTER TABLE `tindakanpengaduan`
  ADD CONSTRAINT `tindakanpengaduan_id_pengaduan_foreign` FOREIGN KEY (`id_pengaduan`) REFERENCES `pengaduan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `toko`
--
ALTER TABLE `toko`
  ADD CONSTRAINT `toko_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksipembelian`
--
ALTER TABLE `transaksipembelian`
  ADD CONSTRAINT `transaksipembelian_id_toko_foreign` FOREIGN KEY (`id_toko`) REFERENCES `toko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksipembelian_id_distributor_foreign` FOREIGN KEY (`id_distributor`) REFERENCES `distributor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksipenjualan`
--
ALTER TABLE `transaksipenjualan`
  ADD CONSTRAINT `transaksipenjualan_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksipenjualan_id_toko_foreign` FOREIGN KEY (`id_toko`) REFERENCES `toko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_dusun_foreign` FOREIGN KEY (`id_dusun`) REFERENCES `dusun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usulan`
--
ALTER TABLE `usulan`
  ADD CONSTRAINT `usulan_id_warga_foreign` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usulan_id_kategori_foreign` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `warga`
--
ALTER TABLE `warga`
  ADD CONSTRAINT `warga_id_profiledesa_foreign` FOREIGN KEY (`id_profiledesa`) REFERENCES `profiledesa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `warga_id_dusun_foreign` FOREIGN KEY (`id_dusun`) REFERENCES `dusun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
