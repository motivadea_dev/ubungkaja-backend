<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKomentarpost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentarpost', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_posting')->unsigned();
            $table->date('tanggal');
            $table->string('nama');
            $table->string('email');
            $table->text('komentar');
            $table->timestamps();
        });
        Schema::table('balaskomentar', function(Blueprint $table) {
            $table->foreign('id_komentarpost')
                ->references('id')
                ->on('komentarpost')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balaskomentar', function(Blueprint $table) {
            $table->dropForeign('balaskomentar_id_komentarpost_foreign');
        });
        Schema::drop('komentarpost');
    }
}
