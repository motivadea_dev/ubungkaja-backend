<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKomentarpengaduan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentarpengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengaduan')->unsigned();
            $table->integer('id_warga')->unsigned();
            $table->date('tanggal');
            $table->text('komentar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('komentarpengaduan');
    }
}
