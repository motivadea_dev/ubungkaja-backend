<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKategoripengaduan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategoripengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profiledesa')->unsigned();
            $table->string('namakategori');
            $table->timestamps();
        });
        Schema::table('pengaduan', function(Blueprint $table) {
            $table->foreign('id_kategoripengaduan')
                ->references('id')
                ->on('kategoripengaduan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengaduan', function(Blueprint $table) {
            $table->dropForeign('pengaduan_id_kategoripengaduan_foreign');
        });
        Schema::drop('kategoripengaduan');
    }
}
