<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKeperluan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keperluan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profiledesa')->unsigned();
            $table->string('nama_keperluan');
            $table->text('syarat');
            $table->timestamps();
        });
        Schema::table('antrian', function(Blueprint $table) {
            $table->foreign('id_keperluan')
                ->references('id')
                ->on('keperluan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('antrian', function(Blueprint $table) {
            $table->dropForeign('antrian_id_keperluan_foreign');
        });
        Schema::drop('keperluan');
    }
}
