<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBalaskomentar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balaskomentar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_komentarpost')->unsigned();
            $table->date('tanggal');
            $table->string('nama');
            $table->string('email');
            $table->text('komentar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('balaskomentar');
    }
}
