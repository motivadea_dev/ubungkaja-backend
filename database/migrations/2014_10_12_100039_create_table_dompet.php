<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDompet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dompet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_warga')->unsigned();
            $table->double('saldo');
            $table->timestamps();
        });
        Schema::table('detaildompet', function(Blueprint $table) {
            $table->foreign('id_dompet')
                ->references('id')
                ->on('dompet')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detaildompet', function(Blueprint $table) {
            $table->dropForeign('detaildompet_id_dompet_foreign');
        });
        Schema::drop('dompet');
    }
}
