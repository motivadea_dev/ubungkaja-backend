<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAntrian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antrian', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->integer('id_warga')->unsigned();
            $table->integer('id_keperluan')->unsigned();
            $table->enum('status_syarat',['1','2']);
            $table->enum('status_jadi',['1','2']);
            $table->timestamps();
        });
        Schema::table('fotoantrian', function(Blueprint $table) {
            $table->foreign('id_antrian')
                ->references('id')
                ->on('antrian')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fotoantrian', function(Blueprint $table) {
            $table->dropForeign('fotoantrian_id_antrian_foreign');
        });
        Schema::drop('antrian');
    }
}
