<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKategoriproduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategoriproduk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('namakategori', 50);
            $table->string('foto')->nullable;
            $table->timestamps();
        });

        Schema::table('subkategoriproduk', function(Blueprint $table) {
            $table->foreign('id_kategoriproduk')
                ->references('id')
                ->on('kategoriproduk')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('produktoko', function(Blueprint $table) {
            $table->foreign('id_kategoriproduk')
                ->references('id')
                ->on('kategoriproduk')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('produkbumdes', function(Blueprint $table) {
            $table->foreign('id_kategoriproduk')
                ->references('id')
                ->on('kategoriproduk')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subkategoriproduk', function(Blueprint $table) {
            $table->dropForeign('subkategoriproduk_id_kategoriproduk_foreign');
        });
        Schema::table('produktoko', function(Blueprint $table) {
            $table->dropForeign('produktoko_id_kategoriproduk_foreign');
        });
        Schema::table('produkbumdes', function(Blueprint $table) {
            $table->dropForeign('produkbumdes_id_kategoriproduk_foreign');
        });
        Schema::drop('kategoriproduk');
    }
}
