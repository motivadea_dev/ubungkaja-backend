<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->enum('level', ['admin','dusun','desa','warga','pelaksana']);
            $table->enum('status', ['1','2']);
            $table->string('fotoktp')->nullable;
            $table->string('fotowajah')->nullable;
            $table->string('nohp');
            $table->string('email');
            $table->integer('id_dusun')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
