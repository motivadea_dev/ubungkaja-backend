<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDusun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dusun', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profiledesa')->unsigned();
            $table->string('nama', 20);
            $table->timestamps();
        });
        Schema::table('warga', function(Blueprint $table) {
            $table->foreign('id_dusun')
                ->references('id')
                ->on('dusun')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('users', function(Blueprint $table) {
            $table->foreign('id_dusun')
                ->references('id')
                ->on('dusun')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('pengaduan', function(Blueprint $table) {
            $table->foreign('id_dusun')
                ->references('id')
                ->on('dusun')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warga', function(Blueprint $table) {
            $table->dropForeign('warga_id_dusun_foreign');
        });
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_id_dusun_foreign');
        });
        Schema::drop('dusun');
    }
}
