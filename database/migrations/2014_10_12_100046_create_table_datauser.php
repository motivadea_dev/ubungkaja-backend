<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDatauser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datauser', function (Blueprint $table) {
            $table->integer('id_warga')->unsigned()->primary('id_warga');
            $table->string('nomor_telepon')->unique();
            $table->string('email')->unique();
            $table->timestamps();

            $table->foreign('id_warga')
                ->references('id')->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('datauser');
    }
}
