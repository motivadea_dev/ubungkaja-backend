<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDistributor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profiledesa')->unsigned();
            $table->string('kodedistributor', 50)->unique();
            $table->string('namadistributor', 50);
            $table->text('alamat');
            $table->string('foto')->nullable;
            $table->enum('status',['buka','tutup']);
            $table->timestamps();
        });

        Schema::table('produkbumdes', function(Blueprint $table) {
            $table->foreign('id_distributor')
                ->references('id')
                ->on('distributor')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('transaksipembelian', function(Blueprint $table) {
            $table->foreign('id_distributor')
                ->references('id')
                ->on('distributor')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produkbumdes', function(Blueprint $table) {
            $table->dropForeign('produkbumdes_id_distributor_foreign');
        });
        Schema::table('transaksipembelian', function(Blueprint $table) {
            $table->dropForeign('transaksipembelian_id_distributor_foreign');
        });
        Schema::drop('distributor');
    }
}
