<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransaksipembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksipembelian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_toko')->unsigned();
            $table->integer('id_distributor')->unsigned();
            $table->date('tanggal');
            $table->double('totaldiskon');
            $table->double('totalbelanja');
            $table->double('subtotal');
            $table->enum('status',['order','kirim','selesai']);
            $table->timestamps();
        });
        Schema::table('pengiriman', function(Blueprint $table) {
            $table->foreign('id_transaksipembelian')
                ->references('id')
                ->on('transaksipembelian')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('detailpembelian', function(Blueprint $table) {
            $table->foreign('id_transaksipembelian')
                ->references('id')
                ->on('transaksipembelian')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengiriman', function(Blueprint $table) {
            $table->dropForeign('pengiriman_id_transaksipembelian_foreign');
        });
        Schema::table('detailpembelian', function(Blueprint $table) {
            $table->dropForeign('detailpembelian_id_transaksipembelian_foreign');
        });
        Schema::drop('transaksipembelian');
    }
}
