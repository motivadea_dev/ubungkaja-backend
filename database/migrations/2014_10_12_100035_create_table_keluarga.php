<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKeluarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keluarga', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nokk', 50)->unique();
            $table->date('tanggal');
            $table->integer('id_profiledesa')->unsigned();
            $table->timestamps();
        });
        Schema::table('anggotakeluarga', function(Blueprint $table) {
            $table->foreign('id_keluarga')
                ->references('id')
                ->on('keluarga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('sertifikat', function(Blueprint $table) {
            $table->foreign('id_keluarga')
                ->references('id')
                ->on('keluarga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('anggotakeluarga', function(Blueprint $table) {
            $table->dropForeign('anggotakeluarga_id_keluarga_foreign');
        });
        Schema::table('serfitikat', function(Blueprint $table) {
            $table->dropForeign('sertifikat_id_keluarga_foreign');
        });
        Schema::drop('keluarga');
    }
}
