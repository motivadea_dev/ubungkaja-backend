<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profiledesa')->unsigned();
            $table->string('namakategori');
            $table->timestamps();
        });
        Schema::table('usulan', function(Blueprint $table) {
            $table->foreign('id_kategori')
                ->references('id')
                ->on('kategori')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usulan', function(Blueprint $table) {
            $table->dropForeign('usulan_id_kategori_foreign');
        });
        Schema::drop('kategori');
    }
}
