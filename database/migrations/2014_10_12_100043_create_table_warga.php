<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warga', function (Blueprint $table) {
            $table->increments('id');
            $table->string('noktp', 50)->unique();
            $table->string('nama', 50);
            $table->text('alamat');
            $table->date('tanggal_lahir');
            $table->enum('jenis_kelamin',['L','P']);
            $table->string('namapendidikan');
            $table->string('namapekerjaan');
            $table->string('namapenghasilan');
            $table->string('foto')->nullable;
            $table->integer('id_dusun')->unsigned();
            $table->integer('id_profiledesa')->unsigned();
            $table->timestamps();
        });
        Schema::table('usulan', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('toko', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('transaksipenjualan', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('pesan', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('notifikasiwarga', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('dompet', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('kurir', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('pengaduan', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('komentarpengaduan', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('antrian', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('anggotakeluarga', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('perangkat', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('posting', function(Blueprint $table) {
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usulan', function(Blueprint $table) {
            $table->dropForeign('usulan_id_warga_foreign');
        });
        Schema::table('toko', function(Blueprint $table) {
            $table->dropForeign('toko_id_warga_foreign');
        });
        Schema::table('transaksipenjualan', function(Blueprint $table) {
            $table->dropForeign('transaksipenjualan_id_warga_foreign');
        });
        Schema::table('pesan', function(Blueprint $table) {
            $table->dropForeign('pesan_id_warga_foreign');
        });
        Schema::table('notifikasiwarga', function(Blueprint $table) {
            $table->dropForeign('notifikasiwarga_id_warga_foreign');
        });
        Schema::table('dompet', function(Blueprint $table) {
            $table->dropForeign('dompet_id_warga_foreign');
        });
        Schema::table('kurir', function(Blueprint $table) {
            $table->dropForeign('kurir_id_warga_foreign');
        });
        Schema::table('pengaduan', function(Blueprint $table) {
            $table->dropForeign('pengaduan_id_warga_foreign');
        });
        Schema::table('komentarpengaduan', function(Blueprint $table) {
            $table->dropForeign('komentarpengaduan_id_warga_foreign');
        });
        Schema::table('antrian', function(Blueprint $table) {
            $table->dropForeign('antrian_id_warga_foreign');
        });
        Schema::table('anggotakeluarga', function(Blueprint $table) {
            $table->dropForeign('anggotakeluarga_id_warga_foreign');
        });
        Schema::table('perangkat', function(Blueprint $table) {
            $table->dropForeign('perangkat_id_warga_foreign');
        });
        Schema::table('posting', function(Blueprint $table) {
            $table->dropForeign('posting_id_warga_foreign');
        });
        Schema::drop('warga');
    }
}
