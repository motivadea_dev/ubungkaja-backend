<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetailPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailpembelian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_transaksipembelian')->unsigned();
            $table->integer('id_produkbumdes')->unsigned();
            $table->integer('jumlah');
            $table->timestamps();

            //Set Primary Key
            /*$table->primary(['id_transaksipembelian', 'id_produkbumdes']);

            //Set Foreign Key ke Transaksi
            $table->foreign('id_transaksipembelian')
                ->references('id')
                ->on('transaksipembelian')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            //Set Foreign Key ke Produk
            $table->foreign('id_produkbumdes')
                ->references('id')
                ->on('produkbumdes')
                ->onDelete('cascade')
                ->onUpdate('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_transaksipembelian');
    }
}
