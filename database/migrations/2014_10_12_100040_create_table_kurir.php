<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKurir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurir', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_warga')->unsigned();
            $table->string('nomor_kurir',25)->unique();
            $table->string('plat_nomor', 50);
            $table->string('jenis_kendaraan', 50);
            $table->enum('status',['aktif','nonaktif']);
            $table->timestamps();
        });
        Schema::table('pengiriman', function(Blueprint $table) {
            $table->foreign('id_kurir')
                ->references('id')
                ->on('kurir')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengiriman', function(Blueprint $table) {
            $table->dropForeign('pengiriman_id_kurir_foreign');
        });
        Schema::drop('kurir');
    }
}
