<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubkategoriproduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subkategoriproduk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kategoriproduk')->unsigned();
            $table->string('namasubkategori', 50);
            $table->timestamps();
        });
        Schema::table('produktoko', function(Blueprint $table) {
            $table->foreign('id_subkategoriproduk')
                ->references('id')
                ->on('subkategoriproduk')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('produkbumdes', function(Blueprint $table) {
            $table->foreign('id_subkategoriproduk')
                ->references('id')
                ->on('subkategoriproduk')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produktoko', function(Blueprint $table) {
            $table->dropForeign('produktoko_id_subkategoriproduk_foreign');
        });
        Schema::table('produkbumdes', function(Blueprint $table) {
            $table->dropForeign('produkbumdes_id_subkategoriproduk_foreign');
        });
        Schema::drop('subkategoriproduk');
    }
}
