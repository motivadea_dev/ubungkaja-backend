<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKategoripost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategoripost', function (Blueprint $table) {
            $table->increments('id');
            $table->string('namakategori', 50);
            $table->string('foto')->nullable;
            $table->timestamps();
        });

        Schema::table('posting', function(Blueprint $table) {
            $table->foreign('id_kategoripost')
                ->references('id')
                ->on('kategoripost')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posting', function(Blueprint $table) {
            $table->dropForeign('posting_id_kategoripost_foreign');
        });

        Schema::drop('kategoripost');
    }
}
