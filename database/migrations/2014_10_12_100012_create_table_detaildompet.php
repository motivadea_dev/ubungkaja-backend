<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetaildompet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detaildompet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dompet')->unsigned();
            $table->date('tanggal');
            $table->text('keterangan');
            $table->double('nominal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detaildompet');
    }
}
