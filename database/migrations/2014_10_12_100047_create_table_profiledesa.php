<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProfiledesa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiledesa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('kecamatan');
            $table->string('kabupaten');
            $table->string('provinsi');
            $table->string('nip');
            $table->string('nama_kades');
            $table->timestamps();
        });
        Schema::table('dusun', function(Blueprint $table) {
            $table->foreign('id_profiledesa')
                ->references('id')
                ->on('profiledesa')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('berita', function(Blueprint $table) {
            $table->foreign('id_profiledesa')
                ->references('id')
                ->on('profiledesa')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('kategori', function(Blueprint $table) {
            $table->foreign('id_profiledesa')
                ->references('id')
                ->on('profiledesa')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('warga', function(Blueprint $table) {
            $table->foreign('id_profiledesa')
                ->references('id')
                ->on('profiledesa')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('distributor', function(Blueprint $table) {
            $table->foreign('id_profiledesa')
                ->references('id')
                ->on('profiledesa')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('keperluan', function(Blueprint $table) {
            $table->foreign('id_profiledesa')
                ->references('id')
                ->on('profiledesa')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('keluarga', function(Blueprint $table) {
            $table->foreign('id_profiledesa')
                ->references('id')
                ->on('profiledesa')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('informasi', function(Blueprint $table) {
            $table->foreign('id_profiledesa')
                ->references('id')
                ->on('profiledesa')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produkbumdes', function(Blueprint $table) {
            $table->dropForeign('produkbumdes_id_profiledesa_foreign');
        });
        Schema::table('dusun', function(Blueprint $table) {
            $table->dropForeign('dusun_id_profiledesa_foreign');
        });
        Schema::table('berita', function(Blueprint $table) {
            $table->dropForeign('berita_id_profiledesa_foreign');
        });
        Schema::table('kategori', function(Blueprint $table) {
            $table->dropForeign('kategori_id_profiledesa_foreign');
        });
        Schema::table('warga', function(Blueprint $table) {
            $table->dropForeign('warga_id_profiledesa_foreign');
        });
        Schema::table('distributor', function(Blueprint $table) {
            $table->dropForeign('distributor_id_profiledesa_foreign');
        });
        Schema::table('keperluan', function(Blueprint $table) {
            $table->dropForeign('keperluan_id_profiledesa_foreign');
        });
        Schema::table('keluarga', function(Blueprint $table) {
            $table->dropForeign('keluarga_id_profiledesa_foreign');
        });
        Schema::table('informasi', function(Blueprint $table) {
            $table->dropForeign('informasi_id_profiledesa_foreign');
        });
        Schema::drop('profiledesa');
    }
}
