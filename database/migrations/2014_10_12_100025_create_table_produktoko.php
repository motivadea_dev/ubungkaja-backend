<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduktoko extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produktoko', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kategoriproduk')->unsigned();
            $table->integer('id_subkategoriproduk')->unsigned();
            $table->integer('id_toko')->unsigned();
            $table->string('kodeproduk', 50)->unique();
            $table->string('namaproduk', 50);
            $table->integer('stok');
            $table->double('harga');
            $table->double('diskon');
            $table->string('foto')->nullable;
            $table->timestamps();
        });
        Schema::table('keranjang', function(Blueprint $table) {
            $table->foreign('id_produktoko')
                ->references('id')
                ->on('produktoko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('detailpenjualan', function(Blueprint $table) {
            $table->foreign('id_produktoko')
                ->references('id')
                ->on('produktoko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('fotoproduk', function(Blueprint $table) {
            $table->foreign('id_produktoko')
                ->references('id')
                ->on('produktoko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keranjang', function(Blueprint $table) {
            $table->dropForeign('keranjang_id_produktoko_foreign');
        });
        Schema::table('detailpenjualan', function(Blueprint $table) {
            $table->dropForeign('detailpenjualan_id_produktoko_foreign');
        });
        Schema::table('fotoproduk', function(Blueprint $table) {
            $table->dropForeign('fotoproduk_id_produktoko_foreign');
        });
        Schema::drop('produktoko');
    }
}
