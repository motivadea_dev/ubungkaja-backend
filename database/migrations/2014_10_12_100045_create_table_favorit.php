<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFavorit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorit', function (Blueprint $table) {
            $table->integer('id_warga')->unsigned()->index();
            $table->integer('id_produktoko')->unsigned()->index();
            $table->timestamps();

            //Set Primary Key
            $table->primary(['id_warga', 'id_produktoko']);

            //Set Foreign Key ke Transaksi
            $table->foreign('id_warga')
                ->references('id')
                ->on('warga')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            //Set Foreign Key ke Produk
            $table->foreign('id_produktoko')
                ->references('id')
                ->on('produktoko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('favorit');
    }
}
